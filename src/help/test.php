<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
error_reporting(E_ALL);
ini_set('display_errors', FALSE);
ini_set('display_startup_errors', FALSE);
ini_set('error_reporting', E_ALL & ~E_WARNING);
//$report_warn=error_reporting(E_ERROR | E_PARSE);

date_default_timezone_set('UTC');

    //dla jezykow
      session_start();
       $_SESSION['chart_generator_szawl_visitor']['agent']=$_SERVER["HTTP_USER_AGENT"];
      //require_once('include/ext.php');
      $browser = substr(strrchr($_SESSION['chart_generator_szawl_visitor']['agent'], ")"), 1);
      $tok = explode(" ",$_SESSION['chart_generator_szawl_visitor']['agent']);
      $_SESSION['chart_generator_szawl_visitor']['page'] = 'test';
      //require_once('include/ext.php');

      if(file_exists('./../admin/lang.php')) {
          $dir='../';
          include_once('./../admin/lang.php');

      }
      if(file_exists('./../admin/funkc.php')) {
          include_once('./../admin/funkc.php');

      }

?>
<!doctype html>

<html lang="<?php if(isset($_SESSION['chart_generator_szawl_visitor']['lang'])){ echo $_SESSION['chart_generator_szawl_visitor']['lang'];} ?>">
<head>
  <meta charset="utf-8">
  <?php if(isset($lang['title'])){echo "<title>".$lang['title']."</title>\n";}else{echo "<title>Knitting chart generator szawl.eu hrlp</title> \n";}?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name = "author" content = "Wlodzimierz Poplavskij">

        <?php
            if ( isset($lang['description']) ) {
                echo '<meta name = "description" content = "'.$lang['description'].'">';
            } else {
                echo '<meta name = "description" content = "Knitting chart generator http://szawl.eu/chart/">';
            }
            if ( isset($lang['keywords']) ) {
                echo '<meta name = "keywords" content = "'.$lang['keywords'].'">';
            } else {
                echo '<meta name = "keywords" content = "knitting,chart">';
            }
        ?>
	<link rel="apple-touch-icon" href="apple-touch-icon.png">
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
        <link rel="stylesheet" href="../css/bootstrap/bootstrap-responsive.css" type="text/css" media="screen" title="master" charset="utf-8">
        <link rel="stylesheet" href="../css/bootstrap/bootstrap-theme.css" type="text/css" media="screen" title="master" charset="utf-8">
        <link rel="stylesheet" href="../css/bootstrap/bootstrap.css" type="text/css" media="screen" title="master" charset="utf-8">
        <link rel="stylesheet" href="../css/normalize.css">
        <link rel="stylesheet" href="../css/main.css">
        <link rel="stylesheet" type="text/css" href="../highslide/highslide.css" />
        <link rel="stylesheet" href="../css/hint/hint.min.css">
        <link rel="stylesheet" href="css/tabsTheme.css">
        <script src="../js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body>
    <?php if (0 === strcmp('index',$_SESSION['chart_generator_szawl_visitor']['page'])) { ?>
    <!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-MXMC4P"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MXMC4P');</script>
<!-- End Google Tag Manager -->
    <?php } ?>
<div class="container">
    <div class="row topline">
        <div class="col-sm-12"><h1 title="http://szawl.eu/chart/ &beta;"><?php if(isset($lang['title'])){ echo $lang['title'];} ?> </h1></div>
    </div>
    <div class="row menu_panel">
        <div class="col-sm-12">
            <div id="langbar">
            <?php
                if(isset($lang['langbar'])){

                    echo ShowLanguages($lang['langbar']).' ';
                }
                if(isset($lang['tools'])){
                    echo '<div class="tools"> '.$lang['tools'].' <a href="http://szawl.eu" class="b">http://szawl.eu</a>';

                    echo '</div> <div class="clearfix"></div>';}
            ?>
            </div>
        </div>
    </div>
    <div class="row plain">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#a" data-toggle="tab"><?php if(isset($lang['step'])){ echo $lang['step'];} ?> 1</a></li>
            <li><a href="#b" data-toggle="tab"><?php if(isset($lang['step'])){ echo $lang['step'];} ?> 2</a></li>
            <li><a href="#c" data-toggle="tab"><?php if(isset($lang['step'])){ echo $lang['step'];} ?> 3</a></li>
            <li><a href="#d" data-toggle="tab"><?php if(isset($lang['step'])){ echo $lang['step'];} ?> 4</a></li>
            <li><a href="#e" data-toggle="tab"><?php if(isset($lang['step'])){ echo $lang['step'];} ?> 5</a></li>
            <li><a href="#f" data-toggle="tab"><?php if(isset($lang['step'])){ echo $lang['step'];} ?> 6</a></li>
            <li><a href="#g" data-toggle="tab"><?php if(isset($lang['step'])){ echo $lang['step'];} ?> 7</a></li>
        </ul>
        <div id="myTabContent" class="tab-content">
          <div class="tab-pane fade active in" id="a" data-image-src="./images/<?php if(isset($_SESSION['chart_generator_szawl_visitor']['lang'])){ echo $_SESSION['chart_generator_szawl_visitor']['lang'];} ?>/r1.png" data-text="<?php if(isset($lang['step1'])){ echo $lang['step1'];} ?>" data-pos-x="680" data-pos-y="360" data-pos-width="350" data-pos-height="200">

          </div>
          <div class="tab-pane fade" id="b" data-image-src="images/<?php if(isset($_SESSION['chart_generator_szawl_visitor']['lang'])){ echo $_SESSION['chart_generator_szawl_visitor']['lang'];} ?>/r1.png" data-text="<?php if(isset($lang['step2'])){ echo $lang['step2'];} ?>" data-pos-x="600" data-pos-y="250" data-pos-width="350" data-pos-height="100">

          </div>
          <div class="tab-pane fade" id="c" data-image-src="images/<?php if(isset($_SESSION['chart_generator_szawl_visitor']['lang'])){ echo $_SESSION['chart_generator_szawl_visitor']['lang'];} ?>/r1.png" data-text="<?php if(isset($lang['step3'])){ echo $lang['step3'];} ?>" data-pos-x="400" data-pos-y="700" data-pos-width="300" data-pos-height="200">

          </div>
          <div class="tab-pane fade" id="d" data-image-src="images/<?php if(isset($_SESSION['chart_generator_szawl_visitor']['lang'])){ echo $_SESSION['chart_generator_szawl_visitor']['lang'];} ?>/r2.png" data-text="<?php if(isset($lang['step4'])){ echo $lang['step4'];} ?>" data-pos-x="800" data-pos-y="250" data-pos-width="300" data-pos-height="200">

          </div>
          <div class="tab-pane fade" id="e" data-image-src="images/<?php if(isset($_SESSION['chart_generator_szawl_visitor']['lang'])){ echo $_SESSION['chart_generator_szawl_visitor']['lang'];} ?>/r3.png" data-text="<?php if(isset($lang['step5'])){ echo $lang['step5'];} ?>" data-pos-x="600" data-pos-y="150" data-pos-width="300" data-pos-height="200">

          </div>
          <div class="tab-pane fade" id="f" data-image-src="images/<?php if(isset($_SESSION['chart_generator_szawl_visitor']['lang'])){ echo $_SESSION['chart_generator_szawl_visitor']['lang'];} ?>/r5.png" data-text="<?php if(isset($lang['step6'])){ echo $lang['step6'];} ?>" data-pos-x="1200" data-pos-y="230" data-pos-width="300" data-pos-height="200">

          </div>
          <div class="tab-pane fade" id="g" data-image-src="images/<?php if(isset($_SESSION['chart_generator_szawl_visitor']['lang'])){ echo $_SESSION['chart_generator_szawl_visitor']['lang'];} ?>/r6.png" data-text="<?php if(isset($lang['step7'])){ echo $lang['step7'];} ?>" data-pos-x="1200" data-pos-y="230" data-pos-width="300" data-pos-height="200">

          </div>
        </div>
    </div>
    <div class="row footer">

        <div class="col-sm-12"><div class="copyright"> Created by  <a href="http://poplauki.eu" target="_blank">Poplauki</a> <?php if(isset($lang['copyrights'])){ echo $lang['copyrights'];} ?></div> </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.3/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="../js/vendor/jquery-1.12.3.min.js"><\/script>')</script>
<script src="../js/vendor/bootstrap.js" type="text/javascript"></script>
<script src="js/vendor/konva/konva.min.js" type="text/javascript"></script>
<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<script src="js/plugins.js"></script>
<script src="js/main.js"></script>
</body>
</html>
