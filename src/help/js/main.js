
$(document).ready(function () {
    hs.graphicsDir = './../highslide/graphics/';
    hs.outlineType = 'rounded-white';
    hs.creditsPosition = 'bottom left';
    hs.anchor = 'top'
    hs.allowSizeReduction = false;
    hs.enableKeyListener = false;
    hs.captionEval = 'this.a.title';

    hs.lang = GetHSLang($('html').attr('lang'));


    var $stage = null;
    var tab = null;
    var tab_name = null;
    var image_src = null;
    var step_text = null;
    var step_pos = null;

    var tab_a = $("#a");
    if ('undefined' !== typeof tab_a && true === tab_a.hasClass('active')) {
        tab = tab_a;
        tab_name = 'a';
        image_src = tab_a.attr('data-image-src');
        step_text = tab_a.attr('data-text');
        step_pos = {pos_x:tab_a.attr('data-pos-x'),pos_y:tab_a.attr('data-pos-y'),width:tab_a.attr('data-pos-width'),height:tab_a.attr('data-pos-height')};
    }
    var tab_b = $("#b");
    if ('undefined' !== typeof tab_b && true === tab_b.hasClass('active')) {
        tab = tab_b;
        tab_name = 'b';
        image_src = tab_b.attr('data-image-src');
        step_text = tab_b.attr('data-text');
        step_pos = {pos_x:tab_b.attr('data-pos-x'),pos_y:tab_b.attr('data-pos-y'),width:tab_b.attr('data-pos-width'),height:tab_b.attr('data-pos-height')};
    }
    var tab_c = $("#c");
    if ('undefined' !== typeof tab_c && true ===tab_c.hasClass('active')) {
        tab = tab_c;
        tab_name = 'c';
        image_src = tab_c.attr('data-image-src');
        step_text = tab_c.attr('data-text');
        step_pos = {pos_x:tab_c.attr('data-pos-x'),pos_y:tab_c.attr('data-pos-y'),width:tab_c.attr('data-pos-width'),height:tab_c.attr('data-pos-height')};
    }
    var tab_d = $("#d");
    if ('undefined' !== typeof tab_d && true ===tab_d.hasClass('active')) {
        tab = tab_d;
        tab_name = 'd';
        image_src = tab_d.attr('data-image-src');
        step_text = tab_d.attr('data-text');
        step_pos = {pos_x:tab_d.attr('data-pos-x'),pos_y:tab_d.attr('data-pos-y'),width:tab_d.attr('data-pos-width'),height:tab_d.attr('data-pos-height')};
    }
    var tab_e = $("#e");
    if ('undefined' !== typeof tab_e && true ===tab_e.hasClass('active')) {
        tab = tab_e;
        tab_name = 'e';
        image_src = tab_e.attr('data-image-src');
        step_text = tab_e.attr('data-text');
        step_pos = {pos_x:tab_e.attr('data-pos-x'),pos_y:tab_e.attr('data-pos-y'),width:tab_e.attr('data-pos-width'),height:tab_e.attr('data-pos-height')};
    }
    var tab_f = $("#f");
    if ('undefined' !== typeof tab_f && true ===tab_f.hasClass('active')) {
        tab = tab_f;
        tab_name = 'f';
        image_src = tab_f.attr('data-image-src');
        step_text = tab_f.attr('data-text');
        step_pos = {pos_x:tab_f.attr('data-pos-x'),pos_y:tab_f.attr('data-pos-y'),width:tab_f.attr('data-pos-width'),height:tab_f.attr('data-pos-height')};
    }
    var tab_g = $("#g");
    if ('undefined' !== typeof tab_g && true ===tab_g.hasClass('active')) {
        tab = tab_g;
        tab_name = 'g';
        image_src = tab_g.attr('data-image-src');
        step_text = tab_g.attr('data-text');
        step_pos = {pos_x:tab_g.attr('data-pos-x'),pos_y:tab_g.attr('data-pos-y'),width:tab_g.attr('data-pos-width'),height:tab_g.attr('data-pos-height')};
    }

    var draw_screen = function(params) {
        var result = {};
        var tab = $("#myTabContent");
        var stage = params.stage;
        if (null !== stage) {
            stage.destroy();
            stage = null;
        }
        var tab_params = {id:tab.attr('id'),
            width:tab.width(),
            height:tab.height()};

        var ratio_block = parseFloat(tab_params.width / tab_params.height);
        stage = new Konva.Stage({
            container: params.tab_name,
            width: tab_params.width,
            height: tab_params.height
        });

        var layer = new Konva.Layer();

        var rect = new Konva.Rect({
            x: 0,
            y: 0,
            stroke: '#555',
            strokeWidth: 2,
            fill: '#fff',
            width: tab_params.width,
            height: tab_params.height
        });

        layer.add(rect);

        stage.add(layer);



        var imageObj = new Image();
        imageObj.onload = function() {
          //rect.fillPatternImage(imageObj);
            var new_width = this.width;
            var new_height = this.height;
            var step = 20;
            var margin_top = 0;
            var margin_left = 0;

            var tooltip_pos = {pos_x:600,pos_y:200,width:600,height:200};
            if ('undefined' !== typeof params.pos ) {
                tooltip_pos = params.pos;
            }

            var badge_group = new Konva.Group({
                x: (-1)*parseInt(tooltip_pos.pos_x)+(parseInt(tooltip_pos.width)/2),
                y: (-1)*parseInt(tooltip_pos.pos_y)+(parseInt(tooltip_pos.height)/2),
                width: tab_params.width,
                height: tab_params.height,
                id: 'canvas_image_group',
                draggable:true
            });

            badge_group.size({
                width: tab_params.width,
                height: tab_params.height
            });

            badge_group.on('mouseover', function () {
                document.body.style.cursor = 'move';
            });

            badge_group.on('move', function () {
                document.body.style.cursor = 'move';
            });

            badge_group.on('mouseout', function () {
                document.body.style.cursor = 'default';
            });

            badge_group.on('click', function () {
                document.body.style.cursor = 'pointer';

            });

            var tooltip_rect = new Konva.Rect({
                x: parseInt(tooltip_pos.pos_x),
                y: parseInt(tooltip_pos.pos_y),
                stroke: '#000',
                strokeWidth: 1,
                fill: '#fff',
                cornerRadius: 20,
                width: parseInt(tooltip_pos.width),
                height: parseInt(tooltip_pos.height),
            });

            tooltip_rect.on('mouseover', function () {
                document.body.style.cursor = 'pointer';
            });

            tooltip_rect.on('mouseenter', function () {
                document.body.style.cursor = 'pointer';
            });

            tooltip_rect.on('mouseup', function () {
                document.body.style.cursor = 'pointer';
            });


            var tooltip_text = new Konva.Text({
                x: parseInt(tooltip_pos.pos_x),
                y: parseInt(tooltip_pos.pos_y),
                text: ''+params.step,
                fontSize: 25,
                fontFamily: '"Trebuchet MS", "Lucida Grande", "Lucida Sans Unicode", "Lucida Sans", Tahoma, sans-serif',
                fill: '#000',
                width: parseInt(tooltip_pos.width),
                height: parseInt(tooltip_pos.height),
                padding: 10,
                align: 'center'
            });

            tooltip_text.on('mouseover', function () {
                document.body.style.cursor = 'pointer';
            });

            tooltip_text.on('mouseenter', function () {
                document.body.style.cursor = 'pointer';
            });

            tooltip_text.on('mouseup', function () {
                document.body.style.cursor = 'pointer';
            });

            var image = new Konva.Image({
                x: margin_left,
                y: margin_top,
                fill: '#c0c0c0',
                image: imageObj,
                width: new_width,
                height: new_height,
                shadowColor: 'black',
                shadowBlur: 10,
                shadowOffset: [10, 10],
                shadowOpacity: 0.1,
                opacity: 0.5,
                cornerRadius: 10
            });

            badge_group.add(image).add(tooltip_rect).add(tooltip_text);
            layer.add(badge_group);
            layer.draw();
        };
        imageObj.src = params.image;

        return result;
    };
    var draw_params = {tab:tab,
        tab_name:tab_name,
        stage:$stage,
        image:image_src,
        step:step_text,
        pos:step_pos};
    var draw_tab = draw_screen(draw_params);

    $('.nav-tabs li a').on('click',function(){
        var self = $(this);
        var href = self.attr('href');
        tab = $(''+href);
        tab_name = tab.attr('id');
        image_src = tab.attr('data-image-src');
        step_text = tab.attr('data-text');
        step_pos = {pos_x:tab.attr('data-pos-x'),pos_y:tab.attr('data-pos-y'),width:tab.attr('data-pos-width'),height:tab.attr('data-pos-height')};
        var draw_params = {tab:tab,
            tab_name:tab_name,
            stage:$stage,
            image:image_src,
            step:step_text,
            pos:step_pos};

        var draw_tab = draw_screen(draw_params);
    });

    $(window).on('resize',function(){


        var href = '';
        $('ul.nav-tabs').find('li').each(function(){
            var self = $(this);

            if (true === self.hasClass('active')) {
                self.find('a').each(function(){

                    href = $(this).attr('href');
                });

            }
        });

        tab = $(''+href);
        tab_name = tab.attr('id');
        image_src = tab.attr('data-image-src');
        step_text = tab.attr('data-text');
        step_pos = {pos_x:tab.attr('data-pos-x'),pos_y:tab.attr('data-pos-y'),width:tab.attr('data-pos-width'),height:tab.attr('data-pos-height')};
        var draw_params = {tab:tab,
            tab_name:tab_name,
            stage:$stage,
            image:image_src,
            step:step_text};

        var draw_tab = draw_screen(draw_params);
    });

    var area = $("#area");
    if ('undefined' !== typeof area) {
        var area_html = area.html();


        if ('' === area_html) {
            //alert('empty');
            area.html('' + fillSquares(11, 11));
            $('#area_x').val(10);
            $('#area_y').val(10);
        }
    }
    $('.append').on('click',function () {
        var thi = $(this);
        var title = thi.attr('id');

        if (title) {
            var selected_block = $('#selectedblock');

            if (selected_block.val() === 'null') {

            } else {
                var mas = title.split("y");
                var block = $('#' + selected_block.val());
                var selected = $('#' + selected_block.val() + '_value');
                if (mas[1] !== -1 && block) {
                    selected.val(parseInt(mas[1]));

                    block.css({'background-image': 'url(\'images/keys_build_' + mas[1] + '.png\')', 'background-position': 'center center', 'background-repeat': 'none', 'background-color': '#fff', 'border': 'thin solid black'});
                    $('#printimage').show();
                    $('#resetimage').show();
                    $('#downloadsource').show();
                    $('#downloadimage').show();
                    $('#createpdf').show();
                }
                selected_block.val(null);
            }

        }
    });
});
$(document).on("change", "input[name=numbering]", function (e) {
    var thi = $(this);
    var value = thi.val();
});
$(document).on("change", "#right_columns_value", function (e) {
    var thi = $(this);
    var value = parseInt(thi.val());
    var area_x = $('#area_x');
    var area_y = $('#area_y');
    if (value <= 30 && value > 0) {
        if (parseInt(area_y.val()) !== value) {
            var area = $("#area");
            var diff = 0;
            if (parseInt(area_y.val()) > value) {
                diff = Math.abs(parseInt(area_y.val()) - value);
                var you_sure = confirm("" + $('#sure_text_column').html());
                if (you_sure === true) {
                    var source_area = area.html();
                    var get_columns = $('#right_columns_value');
                    var elem = $('#block0_0');
                    var i = 1, k = 1, j = parseInt(area_y.val()) - 1;
                    for (i = 1; i <= parseInt(area_x.val()); i++) {
                        for (k = value + 1; k <= j + 1; k++) {
                            elem = $('#block' + i + '_' + k + '');
                            if (elem) {
                                elem.remove();
                            }
                        }
                    }
                    if (parseInt(area_y.val()) >= 18) {
                        var width = parseInt(area.css('width'));
                        var width_old = width;
                        var width_diff = 24 * Math.abs(parseInt(area_y.val()) - 18);
                        width -= width_diff;
                        var str_width = width + 'px';
                        area.css({'width': str_width});
                    }
                    area_y.val(value);
                    get_columns.val(value);
                }
            } else {
                diff = Math.abs(value - parseInt(area_y.val()));
                var get_columns = $('#right_columns_value');
                var source_area = area.html();
                var source_area_columns = source_area.split('<div class="clear"></div>');
                var source_area_new = '', source_area_new_sub = '';
                var i = 1, k = 1, j = parseInt(area_y.val()) + 1;

                for (i = 1; i < source_area_columns.length; i++) {
                    for (k = j; k <= value; k++) {
                        source_area_new_sub = source_area_new_sub + '<div id="block' + i + '_' + k + '" class="area_block" title="chart.szawl.eu/v2"><input type="hidden" id="block' + i + '_' + k + '_x" value="' + i + '"><input type="hidden" id="block' + i + '_' + k + '_y" value="' + k + '"><input type="hidden" id="block' + i + '_' + k + '_value" name="block' + i + '_' + k + '_value" value="null"><select id="block' + i + '_' + k + '_select" name="block' + i + '_' + k + '_select" class="block_select" title="block' + i + '_' + k + '"></select></div>';
                    }
                    source_area_new = source_area_new + '' + source_area_columns[i - 1] + '' + source_area_new_sub + '<div class="clear"></div>';
                    source_area_new_sub = '';
                }

                if (value >= 18) {
                    var width = parseInt(area.css('width'));
                    var width_old = width;
                    var width_diff = 24 * diff;
                    width += width_diff;
                    var str_width = width + 'px';
                    area.css({'width': str_width});


                }
                area.html(source_area_new);
                area_y.val(value);
                get_columns.val(value);
            }
        }
    } else {
        thi.val(area_y.val());
    }
});
$(document).on("change", "#bottom_rows_value", function (e) {
    var thi = $(this);
    var value = parseInt(thi.val());
    var area_x = $('#area_x');
    var area_y = $('#area_y');

    if (value <= 30 && value > 0) {
        if (parseInt(area_x.val()) !== value) {
            var area = $("#area");
            var diff = 0;
            if (area_x.val() > value) {
                diff = Math.abs(area_x.val() - value);
                var you_sure = confirm("" + $('#sure_text_row').html());
                if (you_sure === true) {
                    var source_area = area.html();
                    var get_rows = $('#bottom_rows_value');
                    var source_area_columns = source_area.split('<div class="clear"></div>');
                    var source_area_old_rows = '';
                    var j = 1, i = parseInt(area_x.val()) - 1;
                    for (j = 1; j <= value; j++) {
                        source_area_old_rows = source_area_old_rows + '' + source_area_columns[j - 1] + '<div class="clear"></div>';
                    }
                    if (parseInt(area_x.val()) >= 18) {

                        var height = parseInt(area.css('height'));
                        var height_old = height;
                        var height_diff = 24 * Math.abs(parseInt(area_x.val()) - 18);
                        height -= height_diff;
                        var str_height = height + 'px';
                        area.css({'height': str_height});


                    }
                    area.html(source_area_old_rows);
                    area_x.val(value);
                    get_rows.val(value);
                }
            } else {
                diff = Math.abs(value - area_x.val());
                var get_rows = $('#bottom_rows_value');
                var source_area = area.html();
                var source_area_new_row = '';
                var j = 1, k = 1, i = parseInt(area_x.val()) + 1;
                for (k = i; k <= value; k++) {
                    for (j = 1; j <= parseInt(area_y.val()); j++) {

                        source_area_new_row = source_area_new_row + '<div id="block' + k + '_' + j + '" class="area_block" title="chart.szawl.eu/v2"><input type="hidden" id="block' + k + '_' + j + '_x" value="' + k + '"><input type="hidden" id="block' + k + '_' + j + '_y" value="' + j + '"><input type="hidden" id="block' + k + '_' + j + '_value" name="block' + k + '_' + j + '_value" value="null"><select id="block' + k + '_' + j + '_select" name="block' + k + '_' + j + '_select" class="block_select" title="block' + k + '_' + j + '"></select></div>';
                    }
                    source_area_new_row = source_area_new_row + '<div class="clear"></div>';
                }
                if (value >= 18) {

                    var height = parseInt(area.css('height'));
                    var height_old = height;
                    var height_diff = 24 * diff;
                    height += height_diff;
                    var str_height = height + 'px';
                    area.css({'height': str_height});


                }

                area.html(source_area + '' + source_area_new_row);
                area_x.val(value);
                get_rows.val(value);
            }
        } else {

        }
    } else {
        thi.val(area_x.val());
    }
});
$(document).on("click", "#emptyblock", function (e) {
    var selected_block = $('#selectedblock');
    if (selected_block) {
        var present = $('#' + selected_block.val());
        present.css({'background-color': 'transparent', 'background-image': '', 'border-top': 'none', 'border-left': 'none', 'border-right': 'thin solid black', 'border-bottom': 'thin solid black'});
    }
    $('#emptyblock').hide();
});
$(document).on("click", "#downloadimage", function (e) {
    var area = $("#area");
    if (area) {
        $('#loader_block').show();
        var area_html = area.html();
        var area_x = $('#area_x');
        var area_y = $('#area_y');
        var elem = $('#block0_0');
        var $radios = $('input:radio[name=numbering]');
        var mas = new Array();
        var j = 1, i = 1, k = 1;
        var point = {};
        for (i = 1; i <= parseInt(area_y.val()); i++) {
            for (j = 1; j <= parseInt(area_x.val()); j++) {
                elem = $('#block' + j + '_' + i + '_value');

                if (elem) {
                    point = {
                        point_title: '#block' + j + '_' + i,
                        x_coord: i - 1,
                        y_coord: j - 1,
                        value: elem.val()
                    };
                    mas.push(point);

                }
                k++;
            }
        }

        var pattern = {
            title: $('input[name=named]').val(),
            x_point: area_x.val(),
            y_point: area_y.val(),
            numbering: $('input:radio[name=numbering]:checked').val(),
            points: mas
        };
        $.post("include/save.php", {'values': JSON.stringify(pattern, ["title", "x_point", "y_point", "numbering", "points", "point_title", "x_coord", "y_coord", "value"])}, function (file) {
            window.location.href = "../../v1/include/picture2.php";
            //window.open(["include/export.php"]);
        });
        $('#loader_block').hide();
    }
});
$(document).on("click", "#downloadsource", function (e) {
    var area = $("#area");
    if (area) {
        $('#loader_block').show();
        var area_html = area.html();
        var area_x = $('#area_x');
        var area_y = $('#area_y');
        var elem = $('#block0_0');
        var $radios = $('input:radio[name=numbering]');
        var mas = new Array();
        var j = 1, i = 1, k = 1;
        var point = {};
        for (i = 1; i <= parseInt(area_y.val()); i++) {
            for (j = 1; j <= parseInt(area_x.val()); j++) {
                elem = $('#block' + j + '_' + i + '_value');

                if (elem) {
                    point = {
                        point_title: '#block' + j + '_' + i,
                        x_coord: i - 1,
                        y_coord: j - 1,
                        value: elem.val()
                    };
                    mas.push(point);

                }
                k++;
            }
        }

        var pattern = {
            title: $('input[name=named]').val(),
            x_point: area_x.val(),
            y_point: area_y.val(),
            numbering: $('input:radio[name=numbering]:checked').val(),
            points: mas
        };

        $.post("include/save.php", {'values': JSON.stringify(pattern, ["title", "x_point", "y_point", "numbering", "points", "point_title", "x_coord", "y_coord", "value"])}, function (file) {
            window.location.href = "../../v1/include/export.php";
            //window.open(["include/export.php"]);
        });
        $('#loader_block').hide();
    }
});
$(document).on("click", "#createpdf", function (e) {
    var area = $("#area");
    if (area) {
        $('#loader_block').show();
        var area_html = area.html();
        var area_x = $('#area_x');
        var area_y = $('#area_y');
        var elem = $('#block0_0');
        var $radios = $('input:radio[name=numbering]');
        var mas = new Array();
        var j = 1, i = 1, k = 1;
        var point = {};
        for (i = 1; i <= parseInt(area_y.val()); i++) {
            for (j = 1; j <= parseInt(area_x.val()); j++) {
                elem = $('#block' + j + '_' + i + '_value');

                if (elem) {
                    point = {
                        point_title: '#block' + j + '_' + i,
                        x_coord: i - 1,
                        y_coord: j - 1,
                        value: elem.val()
                    };
                    mas.push(point);

                }
                k++;
            }
        }

        var pattern = {
            title: $('input[name=named]').val(),
            x_point: area_x.val(),
            y_point: area_y.val(),
            numbering: $('input:radio[name=numbering]:checked').val(),
            points: mas
        };

        $.post("include/save.php", {'values': JSON.stringify(pattern, ["title", "x_point", "y_point", "numbering", "points", "point_title", "x_coord", "y_coord", "value"])}, function (file) {
            window.location.href = "../../v1/include/export_pdf.php";
            //window.open(["include/export.php"]);
        });
        $('#loader_block').hide();
    }
});
$(document).on("click", "#resetimage", function (e) {
    var you_sure = confirm("" + $('#sure_text_clear').html());
    if (you_sure === true) {
        var area = $("#area");
        if (area) {
            var area_html = area.html();


            if (area_html.trim() !== '') {
                //alert('empty');
                area.html('' + fillSquares(11, 11));
                $('#area_x').val(10);
                $('#area_y').val(10);
                $('#bottom_rows_value').val(10);
                $('#right_columns_value').val(10);
                $('#selectedblock').empty();
                $('#inputkommand').empty();
                $('#controller').val(null);
                $('input[name=named]').val('Chart_1');
                var $radios = $('input:radio[name=numbering]');
                $radios.filter('[value=all]').prop('checked', true);
                $('#resetimage').hide();
                $('#printimage').hide();
                $('#downloadsource').hide();
                $('#downloadimage').hide();
                $('#createpdf').hide();
            }

        }
    }
});
$(document).on("click", ".area_block", function (e) {

    var thi = $(this);
    var title = thi.attr('id');
    if (title) {
        //alert('title - '+title);
        //var select_sample=$('#select_sample');
        var controller = $('#controller');
        var selected_block = $('#selectedblock');

        var x = $('#' + title + '_x');
        var y = $('#' + title + '_y');
        if (selected_block.val() !== title) {
            if (selected_block.val() !== null) {
                var present = $('#' + selected_block.val());
                present.css({'background-color': 'transparent', 'border-top': 'none', 'border-left': 'none', 'border-right': 'thin solid black', 'border-bottom': 'thin solid black'});
            }
        } else {
            $('#emptyblock').show();
        }
        selected_block.val(title);
        thi.css({'background-color': '#f00', 'border': 'thin solid yellow'});
    }
});


$(document).on('change', ".block_select", function () {

    var thi = $(this);
    var title = thi.attr('title');

    if (title) {
        var option = $("select#" + title + "_select");
        var controller = $('#controller');

        var x, y, block, selected;
        x = $('#' + title + '_x');
        y = $('#' + title + '_y');
        block = $('#block' + x.val() + '_' + y.val());
        selected = $('#block' + x.val() + '_' + y.val() + '_value');
        if (option.val() !== -1) {


            selected.val(option.val());
            block.css({'background-image': 'url(\'images/keys_build_' + option.val() + '.png\')', 'background-repeat': 'none'});

        } else {
            //controller.val(null);
            selected.val('');
            block.css({'background-image': 'none'});
        }
        option.html('');
        option.hide();
    }
});
$(document).on('click', ".right_columns_plus", function () {
    var area = $("#area");
    var area_x = $('#area_x');
    var area_y = $('#area_y');
    var get_columns = $('#right_columns_value');
    if (get_columns && get_columns.val() < 30) {
        var source_area = area.html();
        var source_area_columns = source_area.split('<div class="clear"></div>');
        var source_area_new = '';
        var i = 1, j = parseInt(area_y.val()) + 1;
        for (i = 1; i < source_area_columns.length; i++) {
            source_area_new = source_area_new + '' + source_area_columns[i - 1] + '<div id="block' + i + '_' + j + '" class="area_block" title="chart.szawl.eu/v2"><input type="hidden" id="block' + i + '_' + j + '_x" value="' + i + '"><input type="hidden" id="block' + i + '_' + j + '_y" value="' + j + '"><input type="hidden" id="block' + i + '_' + j + '_value" name="block' + i + '_' + j + '_value" value="null"><select id="block' + i + '_' + j + '_select" name="block' + i + '_' + j + '_select" class="block_select" title="block' + i + '_' + j + '"></select></div><div class="clear"></div>';
        }
        if (j >= 18) {

            var width = parseInt(area.css('width'));
            width += 24;
            var str_width = width + 'px';
            area.css({'width': str_width});

        }
        area.html(source_area_new);
        area_y.val(j);
        get_columns.val(j);
    }
});

$(document).on('click', ".right_columns_minus", function () {
    var area = $("#area");
    var area_x = $('#area_x');
    var area_y = $('#area_y');
    var get_columns = $('#right_columns_value');

    if (get_columns && get_columns.val() < 30) {
        var you_sure = confirm("" + $('#sure_text_column').html());
        if (you_sure === true) {
            var source_area = area.html();
            var elem = $('#block0_0');
            var i = 1, j = parseInt(area_y.val()) - 1;
            for (i = 1; i <= parseInt(area_x.val()); i++) {
                elem = $('#block' + i + '_' + parseInt(area_y.val()) + '');
                if (elem) {
                    elem.remove();
                }
            }
            if (parseInt(area_y.val()) >= 18) {
                var width = parseInt(area.css('width'));
                width -= 24;
                var str_width = width + 'px';
                area.css({'width': str_width});
            }
            //alert('columns '+get_columns.val()+'');
            area_y.val(j);
            get_columns.val(j);
        }

    }
});

$(document).on('click', ".bottom_rows_plus", function () {
    var area = $("#area");
    var area_x = $('#area_x');
    var area_y = $('#area_y');
    var get_rows = $('#bottom_rows_value');
    if (get_rows && get_rows.val() < 30) {
        var source_area = area.html();
        var source_area_new_row = '';
        var j = 1, i = parseInt(area_x.val()) + 1;
        for (j = 1; j <= parseInt(area_y.val()); j++) {

            source_area_new_row = source_area_new_row + '<div id="block' + i + '_' + j + '" class="area_block" title="chart.szawl.eu/v2"><input type="hidden" id="block' + i + '_' + j + '_x" value="' + i + '"><input type="hidden" id="block' + i + '_' + j + '_y" value="' + j + '"><input type="hidden" id="block' + i + '_' + j + '_value" name="block' + i + '_' + j + '_value" value="null"><select id="block' + i + '_' + j + '_select" name="block' + i + '_' + j + '_select" class="block_select" title="block' + i + '_' + j + '"></select></div>';
        }
        source_area_new_row = source_area_new_row + '<div class="clear"></div>';
        if (i >= 18) {

            var height = parseInt(area.css('height'));
            height += 24;
            var str_height = height + 'px';
            area.css({'height': str_height});

        }

        area.html(source_area + '' + source_area_new_row);
        area_x.val(i);
        get_rows.val(i);
    }

});

$(document).on('click', ".bottom_rows_minus", function () {
    var area = $("#area");
    var area_x = $('#area_x');
    var area_y = $('#area_y');
    var get_rows = $('#bottom_rows_value');
    if (get_rows && get_rows.val() < 30) {

        var you_sure = confirm("" + $('#sure_text_row').html());
        if (you_sure === true) {

            var source_area = area.html();
            var source_area_columns = source_area.split('<div class="clear"></div>');
            var source_area_old_rows = '';
            var j = 1, i = parseInt(area_x.val()) - 1;
            for (j = 1; j < source_area_columns.length - 1; j++) {
                source_area_old_rows = source_area_old_rows + '' + source_area_columns[j - 1] + '<div class="clear"></div>';
            }
            if (i >= 18) {

                var height = parseInt(area.css('height'));
                height -= 24;
                var str_height = height + 'px';
                area.css({'height': str_height});

            }
            area.html(source_area_old_rows);
            area_x.val(i);
            get_rows.val(i);
        }
    }

});

$(document).on('click', ".language", function () {
    var self = $(this);
    var lang = self.attr('data-language-key');
    select_lang(lang);
});

function fillSquares(x, y) {
    var str = '', src_tmp = '';
    if (x > 0 && y > 0) {
        var i = 1, j = 1;
        src_tmp = '';
        for (i = 1; i < x; i++) {

            for (j = 1; j < y; j++) {
                src_tmp = src_tmp + '<div id="block' + i + '_' + j + '" class="area_block" title="chart.szawl.eu/v2"><input type="hidden" id="block' + i + '_' + j + '_x" value="' + i + '"><input type="hidden" id="block' + i + '_' + j + '_y" value="' + j + '"><input type="hidden" id="block' + i + '_' + j + '_value" name="block' + i + '_' + j + '_value" value="null"><select id="block' + i + '_' + j + '_select" name="block' + i + '' + j + '_select" class="block_select" title="block' + i + '_' + j + '"></select></div>';
            }
            src_tmp = src_tmp + '<div class="clear"></div>';
        }
        str = str + src_tmp + '';
    }
    return str;
}
function select_lang(let) {
    //alert(" "+let+" ");
    var form = document.createElement("form"), tmp;
    form.action = self.location;
    form.method = "post";
    form.id = "__id__tmp_form_for_post_submit";
    //for (var param in let) {
    tmp = document.createElement("input");
    tmp.type = "hidden";
    tmp.name = "lang_sel";
    tmp.value = let;
    form.appendChild(tmp);
    //}
    document.body.appendChild(form);
    form.submit();
}
function unhide(divID) {
    var item = document.getElementById(divID);
    if (item) {
        item.className = (item.className === 'hidden') ? 'unhidden' : 'hidden';
    }
}
function cleanArea() {
    var formarea = document.getElementById("inputkommand");
    unhide("subButton");
    unhide("chart_picture");
    unhide("exportLink");
    unhide("saveLink");
    //alert(" "+formarea.value+" ");
    if (formarea.value !== "") {
        formarea.value = "";
    }
    unhide("rezet");
}
function ClickOn(va) {
    var item = document.getElementById("helper");
    var item2 = document.getElementById("showhelp");
    if (va) {
        item.className = 'unhidden';
        item2.className = 'hidden';
    } else {
        item.className = 'hidden';
        item2.className = 'unhidden';
    }
}
function checkArea() {
    var formarea = document.getElementById("inputkommand");
    var alls = new Array(200);
    var sArray = new Array();
    n = 0;
    var sk = '', b = 0, e;
    sArray = formarea.value.split("\n");
    //alert(""+sArray.length+"");
    //alert(""+sArray[0]);
    //alert(""+sArray[1]);
    //alert(""+sArray[2]);
    var i = 0;
    for (i = 0; i < sArray.length; i++) {
        var mArray = new Array();
        mArray = sArray[i].split(" ");
        //alert(""+mArray.length+"");
        while (b < mArray.length) {
            while (mArray.charAt(b) === sk)
                b++;
            e = b;
            while ((mArray.charAt(e) !== sk) && (e < mArray.length))
                e++;
            alls[n] = parseInt(mArray.substring(b, e));
            n++;
            b = e;
        }
    }
    alert("n " + n);
    //unhide("inputkommand");
    //unhide("subButton");
    //var i=0;
    //for(i=0;i<n;i++)alert(""+alls[i]+"");
}

function CR(s, c) {
    s.style.backgroundColor = c;
}

function supports_html5_storage() {
  try {
    return 'localStorage' in window && window['localStorage'] !== null;
} catch (e) {
    return false;
  }
}

function getSzawlState() {
    if (!supports_html5_storage()) { return null; }
    var answer = localStorage.getItem("settings");
    return answer;
}


function saveSzawlValue(key,value) {
    if (!supports_html5_storage()) { return false; }
    var answer = localStorage.getItem("settings");
    answer[key]=value;
    localStorage.setItem("settings", answer);
    return true;
}

function GetHSLang(value) {
    if ('en' === value) {
        return {};
    } else if ('ru' === value){
        return {
	cssDirection: 'ltr',
	loadingText: 'Загружается...',
	loadingTitle: 'Нажмите для отмены',
	focusTitle: 'Нажмите чтобы поместить на передний план',
	fullExpandTitle: 'Развернуть до оригинального размера',
	creditsText: 'Использует <i>Highslide JS</i>',
	creditsTitle: 'Перейти на домашнюю страницу Highslide JS',
	previousText: 'Предыдущее',
	nextText: 'Следующее',
	moveText: 'Переместить',
	closeText: 'Закрыть',
	closeTitle: 'Закрыть (esc)',
	resizeTitle: 'Изменить размер',
	playText: 'Слайдшоу',
	playTitle: 'Начать слайдшоу (пробел)',
	pauseText: 'Пауза',
	pauseTitle: 'Приостановить слайдшоу (пробел)',
	previousTitle: 'Предыдущее (стрелка влево)',
	nextTitle: 'Следующее (стрелка вправо)',
	moveTitle: 'Переместить',
	fullExpandText: 'Оригинальный размер',
	number: 'Изображение %1 из %2',
	restoreTitle: 'Нажмите чтобы закрыть изображение, нажмите и перетащите для изменения местоположения. Для просмотра изображений используйте стрелки.'
};
    } else if ('pl' === value){
        return {
	cssDirection: 'ltr',
	loadingText: 'Ładowanie...',
	loadingTitle: 'Kliknij, aby anulować',
	focusTitle: 'Kliknij, aby przenieść na wierzch',
	fullExpandTitle: 'Rozszerz do pełnego rozmiaru',
	creditsText: 'Korzysta z <i>Highslide JS</i>',
	creditsTitle: 'Przejdź do strony domowej Highslide JS',
	previousText: 'Wstecz',
	nextText: 'Dalej',
	moveText: 'Przesuń',
	closeText: 'Zamknij',
	closeTitle: 'Zamknij (esc)',
	resizeTitle: 'Zmień rozmiar',
	playText: 'Uruchom',
	playTitle: 'Uruchom pokaz slajdów (spacja)',
	pauseText: 'Pauza',
	pauseTitle: 'Wstrzymaj pokaz slajdów (spacja)',
	previousTitle: 'Wstecz (lewa strzałka)',
	nextTitle: 'Dalej (prawa strzałka)',
	moveTitle: 'Przesuń',
	fullExpandText: 'Pełny rozmiar',
	number: 'Image %1 of %2',
	restoreTitle: 'Kliknij, aby zamknąć obrazek; kliknij i przeciąg, aby przesunąć. Użyj klawiszy strzałek, aby przejść dalej lub wstecz.'
};
    } else if ('lt' === value){
        return {
	cssDirection: 'ltr',
	loadingText: 'Kraunama...',
	loadingTitle: 'Atšaukti krovimą',
	focusTitle: 'Rodyti viršuje',
	fullExpandTitle: 'Išplėsti iki pilno dydžio',
	creditsText: 'Pagaminta <i>Highslide JS</i>',
	creditsTitle: 'Eiti į Highslide JS namų puslapį',
	previousText: 'Buvusi',
	nextText: 'Sekanti',
	moveText: 'Patraukti',
	closeText: 'Uždaryti',
	closeTitle: 'Uždaryti (esc klavišas)',
	resizeTitle: 'Pakeisti dydį',
	playText: 'Rodyti',
	playTitle: 'Rodyti slideshow (spacebar klavišas)',
	pauseText: 'Pristabdyti',
	pauseTitle: 'Pristabdyti slideshow (spacebar klavišas)',
	previousTitle: 'Buvusi (klavišas kairėn)',
	nextTitle: 'Sekanti (klavišas dešinėn)',
	moveTitle: 'Patraukti',
	fullExpandText: 'Pilnas dydis',
	number: 'Paveikslėlis %1 iš %2',
	restoreTitle: 'Sputelkite ir uždarysite paveikslėlį. Spustelkite neatleisdami ir galėsite keisti paveikslėlio pozicija svetainėje. Naudokite rodykles, norėdami pažiūrėti buvusią arba sekančią.'
} ;
    } else if ('de' === value){
        return {
	cssDirection: 'ltr',
	loadingText: 'Lade...',
	loadingTitle: 'Klick zum Abbrechen',
	focusTitle: 'Klick um nach vorn zu bringen',
	fullExpandTitle: 'Zur Originalgröße erweitern',
	creditsText: 'Powered by <i>Highslide JS</i>',
	creditsTitle: 'Gehe zur Highslide JS Homepage',
	previousText: 'Voriges',
	nextText: 'Nächstes',
	moveText: 'Verschieben',
	closeText: 'Schließen',
	closeTitle: 'Schließen (Esc)',
	resizeTitle: 'Größe wiederherstellen',
	playText: 'Abspielen',
	playTitle: 'Slideshow abspielen (Leertaste)',
	pauseText: 'Pause',
	pauseTitle: 'Pausiere Slideshow (Leertaste)',
	previousTitle: 'Voriges (Pfeiltaste links)',
	nextTitle: 'Nächstes (Pfeiltaste rechts)',
	moveTitle: 'Verschieben',
	fullExpandText: 'Vollbild',
	number: 'Bild %1 von %2',
	restoreTitle: 'Klick um das Bild zu schließen, klick und ziehe um zu verschieben. Benutze Pfeiltasten für vor und zurück.'
};
    } else if ('fr' === value){
        return {
	cssDirection: 'ltr',
	loadingText: 'Chargement...',
	loadingTitle: 'Cliquer pour annuler',
	focusTitle: 'Cliquer pour amener au premier plan',
	fullExpandTitle: 'Afficher à la taille réelle',
	creditsText: 'Propulsé par <i>Highslide JS</i>',
	creditsTitle: 'Site Web de Highslide JS',
	previousText: 'Précédente',
	nextText: 'Suivante',
	moveText: 'Déplacer',
	closeText: 'Fermer',
	closeTitle: 'Fermer (esc ou Échappement)',
	resizeTitle: 'Redimensionner',
	playText: 'Lancer',
	playTitle: 'Lancer le diaporama (barre d\'espace)',
	pauseText: 'Pause',
	pauseTitle: 'Suspendre le diaporama (barre d\'espace)',
	previousTitle: 'Précédente (flèche gauche)',
	nextTitle: 'Suivante (flèche droite)',
	moveTitle: 'Déplacer',
	fullExpandText: 'Taille réelle',
	number: 'Image %1 sur %2',
	restoreTitle: 'Cliquer pour fermer l\'image, cliquer et faire glisser pour déplacer, utiliser les touches flèches droite et gauche pour suivant et précédent.'
};
    } else if ('es' === value){
        return {
	cssDirection: 'ltr',
	loadingText: 'Cargando...',
	loadingTitle: 'Click para cancelar',
	focusTitle: 'Click para traer al frente',
	fullExpandTitle: 'Expandir al tamaño actual',
	creditsText: 'Potenciado por <i>Highslide JS</i>',
	creditsTitle: 'Ir al home de Highslide JS',
	previousText: 'Anterior',
	nextText: 'Siguiente',
	moveText: 'Mover',
	closeText: 'Cerrar',
	closeTitle: 'Cerrar (esc)',
	resizeTitle: 'Redimensionar',
	playText: 'Iniciar',
	playTitle: 'Iniciar slideshow (barra espacio)',
	pauseText: 'Pausar',
	pauseTitle: 'Pausar slideshow (barra espacio)',
	previousTitle: 'Anterior (flecha izquierda)',
	nextTitle: 'Siguiente (flecha derecha)',
	moveTitle: 'Mover',
	fullExpandText: 'Tamaño real',
	number: 'Imagen %1 de %2',
	restoreTitle: 'Click para cerrar la imagen, click y arrastrar para mover. Usa las flechas del teclado para avanzar o retroceder.'
};
    } else if ('it' === value){
        return {
	cssDirection: 'ltr',
	loadingText: 'Caricamento in corso',
	loadingTitle: 'Fare clic per annullare',
	focusTitle: 'Fare clic per portare in avanti',
	fullExpandTitle: 'Visualizza dimensioni originali',
	creditsText: 'Powered by <i>Highslide JS</i>',
	creditsTitle: 'Vai al sito Web di Highslide JS',
	previousText: 'Precedente',
	nextText: 'Successiva',
	moveText: 'Sposta',
	closeText: 'Chiudi',
	closeTitle: 'Chiudi (Esc)',
	resizeTitle: 'Ridimensiona',
	playText: 'Avvia',
	playTitle: 'Avvia slideshow (barra spaziatrice)',
	pauseText: 'Pausa',
	pauseTitle: 'Pausa slideshow (barra spaziatrice)',
	previousTitle: 'Precedente (freccia sinistra)',
	nextTitle: 'Successiva (freccia destra)',
	moveTitle: 'Sposta',
	fullExpandText: 'Dimensione massima',
	number: 'Immagine %1 di %2',
	restoreTitle: 'Fare clic per chiudere l\'immagine, trascina per spostare. Frecce andare avanti e indietro.'
};
    } else if ('fi' === value){
        return {
	cssDirection: 'ltr',
	loadingText: 'Lataa...',
	loadingTitle: 'Keskeytä',
	focusTitle: 'Tuo esille',
	fullExpandTitle: 'Suurenna täysikokoiseksi',
	creditsText: 'Moottorina <i>Highslide JS</i>',
	creditsTitle: 'Avaa Highslide JS kotisivu',
	previousText: 'Edellinen',
	nextText: 'Seuraava',
	moveText: 'Siirrä',
	closeText: 'Sulje',
	closeTitle: 'Sulje (esc)',
	resizeTitle: 'Muuta kokoa',
	playText: 'Aloita',
	playTitle: 'Aloita esitys (välilyönti)',
	pauseText: 'Tauko',
	pauseTitle: 'Tauko esityksessä (välilyönti)',
	previousTitle: 'Edellinen (nuoli vasemmalle)',
	nextTitle: 'Seuraava (nuoli oikealle)',
	moveTitle: 'Siirrä',
	fullExpandText: 'Täysikokoinen',
	number: 'Kuva %1 / %2',
	restoreTitle: 'Kilauta sulkeaksesi, paina ja raahaa siirtääksesi. Valitse edellinen tai seuraava nuolilla.'
};
    } else if ('uk' === value){
        return {
	cssDirection: 'ltr',
	loadingText: 'Завантаження...',
	loadingTitle: 'Натисніть для скасування',
	focusTitle: 'Натисніть щоб перемістити на передній план',
	fullExpandTitle: 'Розкрити до оригінального розміру',
	creditsText: 'Використовує <i>Highslide JS</i>',
	creditsTitle: 'Відвідати домашню сторінку Highslide JS',
	previousText: 'Попереднє',
	nextText: 'Наступне',
	moveText: 'Перемістити',
	closeText: 'Закрити',
	closeTitle: 'Закрити (esc)',
	resizeTitle: 'Змінити розмір',
	playText: 'Слайдшоу',
	playTitle: 'Почати слайдшоу (пробіл)',
	pauseText: 'Пауза',
	pauseTitle: 'Призупинити слайдшоу (пробіл)',
	previousTitle: 'Попереднє (стрілка ліворуч)',
	nextTitle: 'Наступне (стрілка праворуч)',
	moveTitle: 'Перемістити',
	fullExpandText: 'Оригінальний розмір',
	number: 'Зображення %1 з %2',
	restoreTitle: 'Настисніть кнопку мишеняти щоб закрити зображення, натисніть та перетягніть для зміни розташування. Для просмотру зображень користуйтеся стрілками.'
};
    } else {
        return {};
    }
}
