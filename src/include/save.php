<?php
session_start();

$filename = "informationlist.txt";
$MyFile = fopen($filename, "w+");

fwrite($MyFile, implode(", ", $_POST));
fclose($MyFile);
$errmsg_arr[] = ' data has been save to '.$filename.' file';
$errflag = true;

if(isset($_POST) && !empty($_POST)){
    // var_dump($_POST['values']);
    $_SESSION['duomenys'] = $_POST;
    if($errflag) {
        $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
        session_write_close();
    }
    print implode(", ", $_SESSION['duomenys']);
} else {
    exit(1);
}
