<?php
session_start();
//error_reporting(E_ALL);
//ini_set('display_errors', TRUE);
//ini_set('display_startup_errors', TRUE);

use Mpdf\Mpdf;

require_once __DIR__ . './../vendor/autoload.php';

function code2utf($num, $lo = true)
{
    //Returns the utf string corresponding to the unicode value
    if ($num < 128) {
        if ($lo) return chr($num);
        else return '&#' . $num . ';';
    }
    if ($num < 2048) return chr(($num >> 6) + 192) . chr(($num & 63) + 128);
    if ($num < 65536) return chr(($num >> 12) + 224) . chr((($num >> 6) & 63) + 128) . chr(($num & 63) + 128);
    if ($num < 2097152) return chr(($num >> 18) + 240) . chr((($num >> 12) & 63) + 128) . chr((($num >> 6) & 63) + 128) . chr(($num & 63) + 128);
    return '?';
}

if (!isset($_SESSION['duomenys'])) {
    die("Не удается создать новую картинку!");
}
if (isset($_SESSION['duomenys'])) {
    $values = $_SESSION['duomenys'];
    //unset($_SESSION['duomenys']);
    $title = trim($values['title']);
    if (isset($title)) {
        $filename = $title;
    } else {
        $filename = "chart_" . date('Y') . "_" . date('n') . "_" . date('j') . "_" . date('G') . "_" . date('i') . "_" . date('s');
    }
    $duom = " x_point " . $values['x_point'] . "  y_point" . $values['y_point'] . "\n";
    $h = intval($values['x_point']);
    $w = intval($values['y_point']);
    $numbering = trim($values['numbering']);
    $block_size = 28;
    $imagewidth = 65 + $w * $block_size;
    $imageheight = 28 + $h * $block_size;
    $mas = array();
    $points = json_decode(trim($values['points']), true);
    for ($i = 0; $i < count($points); $i++) {
        if (strcmp($points[$i]['value'], 'null') !== 0) {
            $val = intval($points[$i]['value']);
        } else {
            $val = 1;
        }
        $mas[intval($points[$i]['y_coord'])][intval($points[$i]['x_coord'])] = $val;
    }
    if ($val === -1) {
        die();
    }
    $html = '<div style="width:' . $imagewidth . 'px;height:' . $imageheight . 'px;margin:0;padding:0;">';
    $k = $h * 2;
    $p = $h * 2 - 1;
    for ($i = 0; $i < count($mas); $i++) {
        $html .= '';
        if (isset($numbering) && $numbering === 'even') {

            $html .= '<div style="width:' . $block_size . 'px;height:' . $block_size . 'px;border:0;font-size:10px;text-align:right;vertical-align:middle;float:left;"><div style="padding:7px 0  0 5px;">' . $k . '-</div></div>';
            $k -= 2;
        } else if (isset($numbering) && $numbering === 'all') {
            if (($h - $i) % 2 === 0) {
                $html .= '<div style="width:' . $block_size . 'px;height:' . $block_size . 'px;border:0;font-size:10px;text-align:right;vertical-align:middle;float:left;"><div style="padding:7px 0  0 5px;">' . ($h - $i) . '-</div></div>';
            } else {
                $html .= '<div style="width:' . $block_size . 'px;height:' . $block_size . 'px;border:0;font-size:10px;text-align:right;vertical-align:top;float:left;"></div>';
            }
        } else if (isset($numbering) && $numbering === 'odd') {
            $html .= '<div style="width:' . $block_size . 'px;height:' . $block_size . 'px;border:0;font-size:10px;text-align:center;vertical-align:middle;float:left;"></div>';
        }
        for ($j = 0; $j < count($mas[$i]); $j++) {
            $html .= '<div style="width:' . $block_size . 'px;height:' . $block_size . 'px;border:0;margin:0;padding:0;text-align:center;vertical-align:top;float:left;"><img src="img2/keys_build_' . $mas[$i][$j] . '.png" width="' . $block_size . '" height="' . $block_size . '" style="width:' . $block_size . 'px;height:' . $block_size . 'px;border:0;"/></div>';
        }
        if (isset($numbering) && $numbering == 'odd') {
            $html .= '<div style="width:' . $block_size . 'px;height:' . $block_size . 'px;border:0;font-size:10px;text-align:left;vertical-align:middle;float:left;"><div style="padding:8px 0 0 0;">- ' . $p . '</div></div>';
            $p -= 2;
        } else if (isset($numbering) && $numbering === 'all') {

            if (($h - $i + 1) % 2 === 0) {
                $html .= '<div style="width:' . $block_size . 'px;height:' . $block_size . 'px;border:0;font-size:10px;text-align:left;vertical-align:middle;float:left;"><div style="padding:8px 0 0 0;">-' . ($h - $i) . '</div></div>';
            } else {
                $html .= '<div style="width:' . $block_size . 'px;height:' . $block_size . 'px;border:0;font-size:10px;text-align:left;vertical-align:top;float:left;"></div>';
            }
        } else if (isset($numbering) && $numbering === 'even') {
            $html .= '<div style="width:' . $block_size . 'px;height:' . $block_size . 'px;border:0;font-size:10px;text-align:center;vertical-align:middle;float:left;"></div>';
        }
        $html .= '<div style="clear:both;"></div>';
    }
    $html .= '<div style="width:' . $block_size . 'px;height:' . $block_size . 'px;border:0;font-size:10px;text-align:center;vertical-align:top;float:left;"></div>';
    for ($i = $w; $i > 0; $i--) {
        $html .= '<div style="width:' . $block_size . 'px;height:' . $block_size . 'px;border:0;font-size:11px;text-align:center;vertical-align:top;float:left;">' . $i . '</div>';
    }
    $html .= '<div style="width:' . $block_size . 'px;height:' . $block_size . 'px;border:0;font-size:10px;text-align:center;vertical-align:top;float:left;"></div><div style="clear:both;"></div>';
    $html .= '</div>';
}

if ($values && $title && $numbering) {
    include('download.php');
    include('../admin/lang.php');

    $filename = "chart_" . date('Y') . "_" . date('m') . "_" . date('d') . "_" . date('H') . "_" . date('i') . "_" . date('s') . '.pdf';
    //paint_file();
    //$imname=$_SESSION['pat_name'].'.png';
    $h = intval($values['x_point']);
    $w = intval($values['y_point']);
    $header = range(1, $w);
    $data = array();
    for ($i = 0; $i < count($mas); $i++) {
        $data[] = $mas[$i];
    }
    $url = "https://www.szawl.eu";
    //$this->Image('favicon.jpg',10,10.5,15,8.5,"",$url);
    try {

        if ($w > 18) {
            $mpdf = new mPDF(['tempDir' => __DIR__ . '/../tmp', 'mode' => 'utf-8', 'format' => 'A4-L']);
        } else {
            $mpdf = new mPDF(['tempDir' => __DIR__ . '/../tmp', 'mode' => 'utf-8', 'format' => 'A4']);
        }
        //$pdf->AddFont('FreeSans','','freesans.php');
        //$mpdf->useOnlyCoreFonts = true;
        //$mpdf->SetDisplayMode('fullpage');
        $mpdf->AddFont('FreeSans', '', 'freesans.php');
        $mpdf->AddFont('FreeSerif', 'I', 'freeserif.php');
        $mpdf->SetFont('FreeSans', '', 12);
        //$mpdf->SetFontSize(4);
        $mpdf->SetTitle($title);
        //'<div style="width:100%;height:20px;border:thin solid #c0c0c0;background-color:rgb(85,26,139);text-align:center;vertical-align:middle;color:#fff;"> <img src="../images/logo.jpg" width="20" height="20" alt="Szawl Chart Generator v2" style="width:20px;height:20px;border:none;float:left;"/><div style="clear:both;"> '.$title_encoded.'</div></div>'
        $title_encoded = preg_replace('/\&\#([0-9]+)\;/m', code2utf($lang['title'], true), $str);
        $pdf_encoded = preg_replace('/\&\#([0-9]+)\;/m', code2utf($lang['pdfgenerated'], true), $str);
        $date_encoded = preg_replace('/\&\#([0-9]+)\;/m', code2utf(date('Y-n-j H:m:i'), true), $str);
        //$mpdf->SetHeader('{DATE j-m-Y}|{PAGENO}/{nb}|');
        //$mpdf->SetFooter('{PAGENO}');
        $mpdf->SetHTMLHeader('SZAWL.EU');
        //$mpdf->SetHTMLFooter('szawl.eu');
        //$mpdf->SetHTMLFooter('<div style="width:100%;height:20px;border:thin solid #c0c0c0;background-color:rgb(85,26,139);text-align:center;vertical-align:middle;font-size:10px;font-weight:normal;color:#fff;"><a href="'.$url.'" target="_blank">szawl.eu</a> '.$pdf_encoded.' '.$date_encoded.'</div>');
        //$mpdf->WriteHTML('<img src="./picture.php" width="'.$imagewidth.'" height="'.$imageheight.'" alt="Szawl Chart Generator v2" style="width:'.$imagewidth.'px;height:'.$imageheight.'px;border:none;float:left;"/>');
        //
        //$mpdf->image("picture.php",0,0,$imagewidth,$imageheight,'png','',true,false);
        /*$html='<!-- defines the headers/footers - this must occur before the headers/footers are set -->

<!--mpdf
<pageheader name="odds" content-right="Szawl.eu" header-style-right="color: #880000; font-style: italic;" line="1" />
<pageheader name="evens" content-right="{DATE j-m-Y}" content-center="{PAGENO}/{nb}" header-style="color: #880000; font-style: italic;" />
<pagefooter name="odds" content-right="Szawl.eu" footer-style-right="color: #880000; font-style: italic;" line="1" />
<pagefooter name="evens" content-right="{DATE j-m-Y}" content-center="{PAGENO}/{nb}" footer-style="color: #880000; font-style: italic;" />

<pageheader name="display" content-center="New header called Display" header-style="color: #000088; font-weight: bold;" />
mpdf-->

<!-- set the headers/footers - they will occur from here on in the document -->
<!--mpdf
<setpageheader name="odds" page="odd" value="on" show-this-page="1" />
<setpageheader name="evens" page="even" value="1" />
<setpagefooter name="odds" page="O" value="on" />
<setpagefooter name="evens" page="E" value="1" />
mpdf-->'.$html;*/
        $mpdf->WriteHTML($html);
        //$pdf->AddFont('FreeSerif','I','freeserif.php');
        //$pdf->AliasNbPages();
        //$pdf->AddPage();
        //$pdf->SetFont('FreeSans','',24);
        //$str = utf8_decode($title);
        //$str2 = iconv('UTF-8', 'windows-1252', $title);
        //$str3 = iconv('UTF-8', 'ISO-8859-2', $title);
        //$pdf->Cell(0,0,$title.' ',0,0,'L');
        //$mpdf->ln(225);
        //$mpdf->SetXY(20,50);
        //$mpdf->BuildTable($header,$data,$numbering);
        //$mpdf->WriteHTML('');
        //$pdf->Image($imname.' ',20,20,30,40);
        //$pdf->Cell(0,0,'text near page bottom',0,0,'C');
        //$pdf->Close();
        $mpdf->Output($filename, 'D');
        exit;
    } catch (Exception $e) {
        echo $e->getMessage();
    }
} else {
    die();
}
?>
