<?php
	$key=intval($_GET['k']);
        putenv('GDFONTPATH=' . realpath('.'));
	$font='FreeSans.ttf';
	$size=13;
	$imagewidth = 28;
	$imageheight = 28;

        $center_coord_x=round($imagewidth/2);
        $center_coord_y=round($imageheight/2);

	header ("Content-type: image/png");
	$im = @imagecreate ($imagewidth,$imageheight) or die("Не удается создать новую картинку!");
        $bg = ImageColorAllocate($im, 255, 255, 255);
	$linecolor = ImageColorAllocate($im, 93, 71, 139);
	$fillcolor = ImageColorAllocate($im, 85, 26, 139);
	$redcolor = ImageColorAllocate($im, 255, 0, 0);
	$black = ImageColorAllocate($im, 0, 0, 0);
        $color = imagecolorallocatealpha($im, 0, 0, 0, 127);
	if(isset($key)&&($key==0)){

		//ImageTTFText($im,$size,0,2,$ycord,$fillcolor,$font,"1");
		//imageFilledRectangle($im,0,0,20,15,$bg);
		ImageRectangle($im, 0, 0, $imagewidth-1, $imageheight-1,$black);
	}elseif(isset($key)&&($key==1)){
		//imageFilledRectangle($im,0,0,$imagewidth, $imageheight,$fillcolor);
		ImageLine($im, $center_coord_x, round($imageheight * 5 / 20), $center_coord_x, round($imageheight * 15 / 20), $fillcolor);
		ImageRectangle($im, 0, 0, $imagewidth - 1, $imageheight - 1, $black);
	}elseif(isset($key)&&($key==2)){

                imageFilledRectangle($im,0,0,$imagewidth, $imageheight,$bg);
                ImageRectangle($im, 0, 0, $imagewidth-1, $imageheight-1,$black);
		ImageLine($im, 20,$center_coord_y, $imagewidth-25 ,$center_coord_y, $fillcolor);
	}elseif(isset($key)&&($key==3)){
		//imageFilledRectangle($im,0,0,20,20,$bg);
		$values = array(
  			0 => round($imagewidth*4/20),     // x1
  			1 => round($imagewidth*16/20),    // y1
  			2 => round($imagewidth*16/20),    // x2
  			3 => round($imagewidth*4/20),     // y2
  			4 => round($imagewidth*16/20),    // x3
  			5 => round($imagewidth*16/20)     // y3
  		);
		imagefilledpolygon($im, $values, 3, $fillcolor );
		//ImageLine($im, 0, 20, 20 ,0, $fillcolor);
		ImageRectangle($im, 0, 0, $imagewidth-1, $imageheight-1,$black);
	}elseif(isset($key)&&($key==4)){
		imageFilledRectangle($im,0,0,$imagewidth, $imageheight,$bg);

                $round_of_circle_x=round($imagewidth*7/20);
                $round_of_circle_y=round($imageheight*7/20);
		imagearc($im, $center_coord_x, $center_coord_y, $round_of_circle_x, $round_of_circle_y, 0, 360,$fillcolor);
		ImageRectangle($im, 0, 0, $imagewidth-1, $imageheight-1,$black);
	}elseif(isset($key)&&($key==5)){
		imageFilledRectangle($im,0,0,$imagewidth, $imageheight,$bg);
		//ImageTTFText($im,10,0,$imagewidth-14,$imageheight-5,$fillcolor,$font,"V");
                ImageTTFText($im,$size,0,$imagewidth-round($imagewidth*15/20),$imageheight-round($imageheight*5/20),$fillcolor,$font,"V");
		ImageRectangle($im, 0, 0, $imagewidth-1, $imageheight-1,$black);
	}elseif(isset($key)&&($key==6)){
		imageFilledRectangle($im,0,0,$imagewidth, $imageheight,$bg);
		$values = array(
  			0 => round($imagewidth*4/20),     // x1
  			1 => round($imageheight*4/20),    // y1
  			2 => round($imagewidth*4/20),    // x2
  			3 => round($imageheight*16/20),    	// y2
  			4 => round($imagewidth*16/20),    // x3
  			5 => round($imageheight*16/20)     // y3
  		);
		imagefilledpolygon($im, $values, 3, $fillcolor );
		//ImageLine($im, 0, 0, 20 ,20, $fillcolor);
		ImageRectangle($im, 0, 0, $imagewidth-1, $imageheight-1,$black);
	}elseif(isset($key)&&($key==7)){
		imageFilledRectangle($im,0,0,$imagewidth, $imageheight,$bg);
		ImageTTFText($im,$size,0,$imagewidth-round($imagewidth*15/20),$imageheight-round($imageheight*5/20),$fillcolor,$font,"M");
		ImageRectangle($im, 0, 0, $imagewidth-1, $imageheight-1,$black);
	}elseif(isset($key)&&($key==8)){
		imageFilledRectangle($im,0,0,$imagewidth, $imageheight,$bg);
		ImageTTFText($im,$size,0,$imagewidth-round($imagewidth*14/20),$imageheight-round($imageheight*4/20),$fillcolor,$font,"V");
		ImageLine($im, round($imageheight*7/20), $center_coord_x, round($imageheight*12/20) ,$center_coord_y, $fillcolor);
		ImageRectangle($im, 0, 0, $imagewidth-1, $imageheight-1,$black);
	}elseif(isset($key)&&($key==9)){
		imageFilledRectangle($im,0,0,$imagewidth, $imageheight,$bg);
		ImageLine($im, $center_coord_x,round($imageheight*5/20), $center_coord_x ,round($imageheight*15/20), $fillcolor);
		ImageLine($im, $center_coord_x,round($imageheight*5/20), round($imagewidth*17/20) ,round($imageheight*15/20), $fillcolor);
		ImageLine($im, $center_coord_x,round($imageheight*5/20), round($imagewidth*3/20) , round($imageheight*15/20), $fillcolor);
		ImageRectangle($im, 0, 0, $imagewidth-1, $imageheight-1,$black);
	}elseif(isset($key)&&($key==10)){
		imageFilledRectangle($im,0,0,$imagewidth, $imageheight,$bg);
		imagearc($im, $center_coord_x, 0, $imagewidth, $imageheight, 0, 360,$fillcolor);
		ImageRectangle($im, 0, 0, $imagewidth-1, $imageheight-1,$black);
	}elseif(isset($key)&&($key==11)){
		imageFilledRectangle($im,0,0,$imagewidth, $imageheight,$bg);
		ImageTTFText($im,$size-4,0,$imagewidth-10,$imageheight-5,$fillcolor,$font,"b");
		ImageRectangle($im, 0, 0, $imagewidth-1, $imageheight-1,$black);
	}elseif(isset($key)&&($key==12)){
		imageFilledRectangle($im,0,0,$imagewidth, $imageheight,$bg);
		imagearc($im, $center_coord_x, $imageheight, $imagewidth, $imageheight, 180, 360,$fillcolor);
		ImageRectangle($im, 0, 0, $imagewidth-1, $imageheight-1,$black);
	}elseif(isset($key)&&($key==13)){
		imageFilledRectangle($im,0,0,$imagewidth, $imageheight,$bg);
		ImageLine($im, round($imagewidth*4/20),round($imageheight*4/20), round($imagewidth*4/20),round($imageheight*16/20), $fillcolor);
		ImageLine($im, round($imagewidth*4/20),round($imageheight*16/20), round($imagewidth*16/20) ,round($imageheight*16/20), $fillcolor);
		ImageLine($im, round($imagewidth*16/20),round($imageheight*16/20), round($imagewidth*4/20) , round($imageheight*4/20), $fillcolor);
		ImageLine($im, round($imagewidth*4/20), round($imageheight*12/20), round($imagewidth*12/20) ,round($imageheight*12/20), $fillcolor);
		ImageRectangle($im, 0, 0, $imagewidth-1, $imageheight-1,$black);
	}elseif(isset($key)&&($key==14)){
		imageFilledRectangle($im,0,0,$imagewidth, $imageheight,$bg);
		ImageLine($im, round($imagewidth*4/20),round($imageheight*16/20), round($imagewidth*16/20),round($imageheight*16/20), $fillcolor);
		ImageLine($im, round($imagewidth*16/20),round($imageheight*16/20), round($imagewidth*16/20),round($imageheight*4/20), $fillcolor);
		ImageLine($im, round($imagewidth*16/20),round($imageheight*4/20), round($imagewidth*4/20) , round($imageheight*16/20), $fillcolor);
		ImageLine($im, round($imagewidth*8/20), round($imageheight*12/20), round($imagewidth*16/20) ,round($imageheight*12/20), $fillcolor);
		ImageRectangle($im, 0, 0, $imagewidth-1, $imageheight-1,$black);
	}elseif(isset($key)&&($key==15)){
		imageFilledRectangle($im,0,0,$imagewidth, $imageheight,$bg);
		ImageLine($im, $center_coord_x,round($imageheight*5/20), $center_coord_x ,round($imageheight*15/20), $fillcolor);
		ImageLine($im, $center_coord_x,round($imageheight*5/20), round($imagewidth*17/20) ,round($imageheight*15/20), $fillcolor);
		ImageLine($im, $center_coord_x,round($imageheight*5/20), round($imagewidth*3/20) , round($imageheight*15/20), $fillcolor);
		ImageLine($im, round($imagewidth*5/20), round($imageheight*13/20), round($imagewidth*15/20) ,round($imageheight*13/20), $fillcolor);
		ImageRectangle($im, 0, 0, $imagewidth-1, $imageheight-1,$black);
	}elseif(isset($key)&&($key==16)){
		//imageFilledRectangle($im,0,0,$imagewidth, $imageheight,$bg);
		ImageLine($im, $center_coord_x,round($imageheight*5/20), round($imagewidth*15/20) ,round($imageheight*15/20), $fillcolor);
		ImageLine($im, $center_coord_x,round($imageheight*5/20), round($imagewidth*5/20)  ,round($imageheight*15/20), $fillcolor);
		ImageTTFText($im,$size-8,0,round($imagewidth*9/20),$imageheight-round($imageheight*4/20),$fillcolor,$font,"4");
		ImageRectangle($im, 0, 0, $imagewidth-1, $imageheight-1,$black);
	}elseif(isset($key)&&($key==17)){
		//imageFilledRectangle($im,0,0,$imagewidth, $imageheight,$bg);
		ImageLine($im, $center_coord_x,round($imageheight*5/20), round($imagewidth*15/20) ,round($imageheight*15/20), $fillcolor);
		ImageLine($im, $center_coord_x,round($imageheight*5/20), round($imagewidth*5/20) , round($imageheight*15/20), $fillcolor);
		ImageLine($im, round($imagewidth*8/20), round($imageheight*10/20), round($imagewidth*13/20) ,round($imageheight*10/20), $fillcolor);
		ImageTTFText($im,$size-8,0,round($imagewidth*9/20),$imageheight-round($imageheight*4/20),$fillcolor,$font,"4");
		ImageRectangle($im, 0, 0, $imagewidth-1, $imageheight-1,$black);
	}
        //imagesavealpha($im, true);
        //imagefill($im, 0, 0, $color);
	imagepng ($im);
	ImageDestroy($im);
?>
