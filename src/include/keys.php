<?php
$key = intval($_GET['k']);
putenv('GDFONTPATH=' . realpath('.'));
$font = 'arial.ttf';
$size = 12;
$imagewidth = 20;
$imageheight = 20;

$center_coord_x = round($imagewidth / 2);
$center_coord_y = round($imageheight / 2);

header("Content-type: image/png");
$im = @imagecreate($imagewidth, $imageheight)
or die("Не удается создать новую картинку!");
$bg = ImageColorAllocate($im, 255, 255, 255);
$linecolor = ImageColorAllocate($im, 93, 71, 139);
$fillcolor = ImageColorAllocate($im, 85, 26, 139);
$redcolor = ImageColorAllocate($im, 255, 0, 0);
$black = ImageColorAllocate($im, 0, 0, 0);

if (isset($key) && ($key == 0)) {
    //ImageTTFText($im,$size,0,2,$ycord,$fillcolor,$font,"1");
    imageFilledRectangle($im, 0, 0, 20, 15, $bg);
    ImageRectangle($im, 0, 0, 19, 19, $black);
} elseif (isset($key) && ($key == 1)) {
    ImageLine($im, 10, 5, 10, 15, $fillcolor);
    //imageFilledRectangle($im,0,0,20,20,$fillcolor);
    ImageRectangle($im, 0, 0, 19, 19, $black);
} elseif (isset($key) && ($key == 2)) {
    imageFilledRectangle($im, 0, 0, $imagewidth, $imageheight, $bg);
    ImageLine($im, 5, $center_coord_y, $imagewidth - 5, $center_coord_y, $fillcolor);
} elseif (isset($key) && ($key == 3)) {
    imageFilledRectangle($im, 0, 0, 20, 20, $bg);
    $values = array(
        0 => 4,     // x1
        1 => 16,    // y1
        2 => 16,    // x2
        3 => 4,        // y2
        4 => 16,    // x3
        5 => 16     // y3
    );
    imagefilledpolygon($im, $values, 3, $fillcolor);
    //ImageLine($im, 0, 20, 20 ,0, $fillcolor);
    ImageRectangle($im, 0, 0, 19, 19, $black);
} elseif (isset($key) && ($key == 4)) {
    imageFilledRectangle($im, 0, 0, 20, 20, $bg);
    imagearc($im, 10, 10, 7, 7, 0, 360, $fillcolor);
    ImageRectangle($im, 0, 0, 19, 19, $black);
} elseif (isset($key) && ($key == 5)) {
    imageFilledRectangle($im, 0, 0, 20, 20, $bg);
    ImageTTFText($im, 10, 0, $imagewidth - 14, $imageheight - 5, $fillcolor, $font, "V");
    ImageRectangle($im, 0, 0, 19, 19, $black);
} elseif (isset($key) && ($key == 6)) {
    imageFilledRectangle($im, 0, 0, 20, 20, $bg);
    $values = array(
        0 => 4,     // x1
        1 => 4,    // y1
        2 => 4,    // x2
        3 => 16,        // y2
        4 => 16,    // x3
        5 => 16     // y3
    );
    imagefilledpolygon($im, $values, 3, $fillcolor);
    //ImageLine($im, 0, 0, 20 ,20, $fillcolor);
    ImageRectangle($im, 0, 0, 19, 19, $black);
} elseif (isset($key) && ($key == 7)) {
    imageFilledRectangle($im, 0, 0, 20, 20, $bg);
    ImageTTFText($im, 10, 0, $imagewidth - 16, $imageheight - 5, $fillcolor, $font, "M");
    ImageRectangle($im, 0, 0, 19, 19, $black);
} elseif (isset($key) && ($key == 8)) {
    imageFilledRectangle($im, 0, 0, 20, 20, $bg);
    ImageTTFText($im, 10, 0, $imagewidth - 14, $imageheight - 5, $fillcolor, $font, "V");
    ImageLine($im, 7, 10, 13, 10, $fillcolor);
    ImageRectangle($im, 0, 0, 19, 19, $black);
} elseif (isset($key) && ($key == 9)) {
    imageFilledRectangle($im, 0, 0, 20, 20, $bg);
    ImageLine($im, 10, 5, 10, 15, $fillcolor);
    ImageLine($im, 10, 5, 17, 15, $fillcolor);
    ImageLine($im, 10, 5, 3, 15, $fillcolor);
    ImageRectangle($im, 0, 0, 19, 19, $black);
} elseif (isset($key) && ($key == 10)) {
    imageFilledRectangle($im, 0, 0, 20, 20, $bg);
    imagearc($im, 10, 0, 20, 20, 0, 360, $fillcolor);
    ImageRectangle($im, 0, 0, 19, 19, $black);
} elseif (isset($key) && ($key == 11)) {
    imageFilledRectangle($im, 0, 0, 20, 20, $bg);
    ImageTTFText($im, 8, 0, $imagewidth - 10, $imageheight - 5, $fillcolor, $font, "b");
    ImageRectangle($im, 0, 0, 19, 19, $black);
} elseif (isset($key) && ($key == 12)) {
    imageFilledRectangle($im, 0, 0, 20, 20, $bg);
    imagearc($im, 10, 20, 20, 20, 180, 360, $fillcolor);
    ImageRectangle($im, 0, 0, 19, 19, $black);
} elseif (isset($key) && ($key == 13)) {
    imageFilledRectangle($im, 0, 0, 20, 20, $bg);
    ImageLine($im, 4, 4, 4, 16, $fillcolor);
    ImageLine($im, 4, 16, 16, 16, $fillcolor);
    ImageLine($im, 16, 16, 4, 4, $fillcolor);
    ImageLine($im, 4, 12, 12, 12, $fillcolor);
    ImageRectangle($im, 0, 0, 19, 19, $black);
} elseif (isset($key) && ($key == 14)) {
    imageFilledRectangle($im, 0, 0, 20, 20, $bg);
    ImageLine($im, 4, 16, 16, 16, $fillcolor);
    ImageLine($im, 16, 16, 16, 4, $fillcolor);
    ImageLine($im, 16, 4, 4, 16, $fillcolor);
    ImageLine($im, 8, 12, 16, 12, $fillcolor);
    ImageRectangle($im, 0, 0, 19, 19, $black);
} elseif (isset($key) && ($key == 15)) {
    imageFilledRectangle($im, 0, 0, 20, 20, $bg);
    ImageLine($im, 10, 5, 10, 15, $fillcolor);
    ImageLine($im, 10, 5, 17, 15, $fillcolor);
    ImageLine($im, 10, 5, 3, 15, $fillcolor);
    ImageLine($im, 5, 13, 15, 13, $fillcolor);
    ImageRectangle($im, 0, 0, 19, 19, $black);
} elseif (isset($key) && ($key == 16)) {
    imageFilledRectangle($im, 0, 0, 20, 20, $bg);
    ImageLine($im, 10, 5, 15, 15, $fillcolor);
    ImageLine($im, 10, 5, 5, 15, $fillcolor);
    ImageTTFText($im, 4, 0, 9, $imageheight - 4, $fillcolor, $font, "4");
    ImageRectangle($im, 0, 0, 19, 19, $black);
} elseif (isset($key) && ($key == 17)) {
    imageFilledRectangle($im, 0, 0, 20, 20, $bg);
    ImageLine($im, 10, 5, 15, 15, $fillcolor);
    ImageLine($im, 10, 5, 5, 15, $fillcolor);
    ImageLine($im, 8, 10, 13, 10, $fillcolor);
    ImageTTFText($im, 4, 0, 9, $imageheight - 4, $fillcolor, $font, "4");
    ImageRectangle($im, 0, 0, 19, 19, $black);
}
imagepng($im);
ImageDestroy($im);
?>
