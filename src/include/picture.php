<?php
//require_once('ext.php');
session_start();
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
//if(file_exists('image_functio.php'))include('image_functio.php');
if (!isset($_SESSION['duomenys'])) die("Не удается создать новую картинку!");
if (isset($_SESSION['duomenys'])) {
    $values = json_decode(trim($_SESSION['duomenys']));
    unset($_SESSION['duomenys']);
    $title = trim($values->title);
    if (isset($title)) {
        $filename = $title;
    } else {
        $filename = "chart_" . date('Y') . "_" . date('n') . "_" . date('j') . "_" . date('G') . "_" . date('i') . "_" . date('s');
    }
    $duom = " x_point " . $values->x_point . "  y_point" . $values->y_point . "\n";
    $h = intval($values->x_point);
    $w = intval($values->y_point);
    $numbering = trim($values->numbering);
    $mas = array();
    for ($i = 0; $i < count($values->points); $i++) {
        if (strcmp($values->points[$i]->value, 'null') !== 0) {
            $val = intval($values->points[$i]->value);
        } else {
            $val = 1;
        }
        $mas[intval($values->points[$i]->y_coord)][intval($values->points[$i]->x_coord)] = $val;
    }
}
if (!empty($mas) && !empty($title) && !empty($numbering)) {
    putenv('GDFONTPATH=' . realpath('.'));
    $font = 'FreeSans.ttf';
    $size = 10;
    $fontangle = 0;
    //if($w<170){$imagewidth = 170;}else{$imagewidth = 170+($w-170);}
    $imagewidth = 60 + $w * 30;
    $imageheight = 30 + $h * 30;
    header('Content-type: image/png');
    $xcord = ($imagewidth / 2) - ($size / 2) - 2;
    $ycord = ($imageheight - 15);
    $im = @Imagecreate($imagewidth, $imageheight) or die ("Не удается создать новую картинку!");
    $bg = ImageColorAllocate($im, 255, 255, 255);
    $linecolor = ImageColorAllocate($im, 93, 71, 139);
    $fillcolor = ImageColorAllocate($im, 85, 26, 139);
    $redcolor = ImageColorAllocate($im, 255, 0, 0);
    $black = ImageColorAllocate($im, 0, 0, 0);
    //Imagecolortransparent($im,$bg);
    //imagelinethick($im, $imagewidth-20, $imageheight-20, $imagewidth, $imageheight, $redcolor, 2);
    if (isset($mas)) {
        $xCoord = 30;
        $yCoord = 30;
        //$top=count($mas)-1;
        for ($i = 0; $i < count($mas); $i++) {

            for ($j = 0; $j < count($mas[$i]); $j++) {
                $top = $i;
                if (isset($mas[$top][$j])) {

                    if ($mas[$top][$j] == 0) {
                        ImageLine($im, ($yCoord + $j * 30), ($xCoord + $i * 30) + 10, ($yCoord + $j * 30), ($xCoord + $i * 30) - 10, $fillcolor);
                        //imageFilledRectangle($im,($yCoord+$j*30-15),($xCoord+$i*30-15),($yCoord+$j*30-15)+30,($xCoord+$i*30-15)+30,$fillcolor);
                    } elseif ($mas[$top][$j] == 1) {
                        $kn = $mas[$top][$j];
                    } elseif ($mas[$top][$j] == 2) {
                        for ($th = 0; $th < 1; $th++) {
                            for ($p = -5; $p <= 5; $p++) {
                                imageSetPixel($im, ($yCoord + $j * 30) + $p, ($xCoord + $i * 30) + $th, $linecolor);
                            }
                        }
                    } elseif ($mas[$top][$j] == 3) {

                        //ImageLine($im, ($yCoord+$j*30)-15, ($xCoord+$i*30)+15, ($yCoord+$j*30)+15 , ($xCoord+$i*30)-15, $fillcolor);
                        ImageLine($im, ($yCoord + $j * 30) - 11, ($xCoord + $i * 30) + 11, ($yCoord + $j * 30) + 11, ($xCoord + $i * 30) - 11, $fillcolor);
                        ImageLine($im, ($yCoord + $j * 30) + 11, ($xCoord + $i * 30) - 11, ($yCoord + $j * 30) + 11, ($xCoord + $i * 30) + 11, $fillcolor);
                        ImageLine($im, ($yCoord + $j * 30) + 11, ($xCoord + $i * 30) + 11, ($yCoord + $j * 30) - 11, ($xCoord + $i * 30) + 11, $fillcolor);
                        imageFill($im, ($yCoord + $j * 30 + 5), ($xCoord + $i * 30 + 5), $fillcolor);
                    } elseif ($mas[$top][$j] == 4) {
                        imagearc($im, ($yCoord + $j * 30), ($xCoord + $i * 30), 12, 12, 0, 360, $fillcolor);
                    } elseif ($mas[$top][$j] == 5) {
                        ImageTTFText($im, $size + 5, 0, ($yCoord + $j * 30 - 6), ($xCoord + $i * 30 + 7), $linecolor, $font, 'V');
                    } elseif ($mas[$top][$j] == 6) {
                        ImageLine($im, ($yCoord + $j * 30) - 11, ($xCoord + $i * 30) - 11, ($yCoord + $j * 30) - 11, ($xCoord + $i * 30) + 11, $fillcolor);
                        ImageLine($im, ($yCoord + $j * 30) - 11, ($xCoord + $i * 30) + 11, ($yCoord + $j * 30) + 11, ($xCoord + $i * 30) + 11, $fillcolor);
                        ImageLine($im, ($yCoord + $j * 30) + 11, ($xCoord + $i * 30) + 11, ($yCoord + $j * 30) - 11, ($xCoord + $i * 30) - 11, $fillcolor);
                        //imageSetPixel($im,($yCoord+$j*30)+10,($xCoord+$i*30)+10,$linecolor);
                        imageFill($im, ($yCoord + $j * 30 - 4), ($xCoord + $i * 30 + 2), $fillcolor);
                    } elseif ($mas[$top][$j] == 7) {
                        ImageTTFText($im, $size + 5, 0, ($yCoord + $j * 30 - 6), ($xCoord + $i * 30 + 7), $linecolor, $font, 'M');
                    } elseif ($mas[$top][$j] == 8) {
                        ImageTTFText($im, $size + 5, 0, ($yCoord + $j * 30 - 6), ($xCoord + $i * 30 + 7), $linecolor, $font, 'V');
                        //imageSetPixel($im,($yCoord+$j*30)-10,($xCoord+$i*30),$linecolor);
                        //imageSetPixel($im,($yCoord+$j*30)-9,($xCoord+$i*30),$linecolor);
                        for ($th = -1; $th < 1; $th++) {
                            for ($p = -5; $p <= 5; $p++) {
                                imageSetPixel($im, ($yCoord + $j * 30) + $p, ($xCoord + $i * 30) + $th, $linecolor);
                            }
                        }
                    } elseif ($mas[$top][$j] == 9) {
                        ImageLine($im, ($yCoord + $j * 30), ($xCoord + $i * 30) + 10, ($yCoord + $j * 30), ($xCoord + $i * 30) - 10, $fillcolor);
                        ImageLine($im, ($yCoord + $j * 30) - 10, ($xCoord + $i * 30) + 10, ($yCoord + $j * 30), ($xCoord + $i * 30) - 10, $fillcolor);
                        ImageLine($im, ($yCoord + $j * 30) + 10, ($xCoord + $i * 30) + 10, ($yCoord + $j * 30), ($xCoord + $i * 30) - 10, $fillcolor);
                    } elseif ($mas[$top][$j] == 10) {
                        imagearc($im, ($yCoord + $j * 30), ($xCoord + $i * 30) - 15, 30, 30, 0, 180, $fillcolor);
                    } elseif ($mas[$top][$j] == 11) {
                        ImageTTFText($im, 8, 0, ($yCoord + $j * 30 - 3), ($xCoord + $i * 30 + 7), $linecolor, $font, 'b');
                    } elseif ($mas[$top][$j] == 12) {
                        imagearc($im, ($yCoord + $j * 30), ($xCoord + $i * 30) + 15, 30, 30, 180, 360, $fillcolor);
                    } elseif ($mas[$top][$j] == 13) {
                        ImageLine($im, ($yCoord + $j * 30) - 11, ($xCoord + $i * 30) - 11, ($yCoord + $j * 30) - 11, ($xCoord + $i * 30) + 11, $fillcolor);
                        ImageLine($im, ($yCoord + $j * 30) - 11, ($xCoord + $i * 30) + 11, ($yCoord + $j * 30) + 11, ($xCoord + $i * 30) + 11, $fillcolor);
                        ImageLine($im, ($yCoord + $j * 30) + 11, ($xCoord + $i * 30) + 11, ($yCoord + $j * 30) - 11, ($xCoord + $i * 30) - 11, $fillcolor);
                        for ($th = 3; $th < 4; $th++) {
                            for ($p = -10; $p <= 3; $p++) {
                                imageSetPixel($im, ($yCoord + $j * 30) + $p, ($xCoord + $i * 30) + $th, $linecolor);
                            }
                        }
                    } elseif ($mas[$top][$j] == 14) {
                        ImageLine($im, ($yCoord + $j * 30) - 11, ($xCoord + $i * 30) + 11, ($yCoord + $j * 30) + 11, ($xCoord + $i * 30) - 11, $fillcolor);
                        ImageLine($im, ($yCoord + $j * 30) + 11, ($xCoord + $i * 30) - 11, ($yCoord + $j * 30) + 11, ($xCoord + $i * 30) + 11, $fillcolor);
                        ImageLine($im, ($yCoord + $j * 30) + 11, ($xCoord + $i * 30) + 11, ($yCoord + $j * 30) - 11, ($xCoord + $i * 30) + 11, $fillcolor);
                        for ($th = 3; $th < 4; $th++) {
                            for ($p = -3; $p <= 10; $p++) {
                                imageSetPixel($im, ($yCoord + $j * 30) + $p, ($xCoord + $i * 30) + $th, $linecolor);
                            }
                        }
                    } elseif ($mas[$top][$j] == 15) {
                        ImageLine($im, ($yCoord + $j * 30), ($xCoord + $i * 30) + 10, ($yCoord + $j * 30), ($xCoord + $i * 30) - 10, $fillcolor);
                        ImageLine($im, ($yCoord + $j * 30) - 10, ($xCoord + $i * 30) + 10, ($yCoord + $j * 30), ($xCoord + $i * 30) - 10, $fillcolor);
                        ImageLine($im, ($yCoord + $j * 30) + 10, ($xCoord + $i * 30) + 10, ($yCoord + $j * 30), ($xCoord + $i * 30) - 10, $fillcolor);
                        for ($th = 5; $th < 6; $th++) {
                            for ($p = -8; $p <= 8; $p++) {
                                imageSetPixel($im, ($yCoord + $j * 30) + $p, ($xCoord + $i * 30) + $th, $linecolor);
                            }
                        }
                    } elseif ($mas[$top][$j] == 16) {
                        ImageLine($im, ($yCoord + $j * 30) - 10, ($xCoord + $i * 30) + 10, ($yCoord + $j * 30), ($xCoord + $i * 30) - 10, $fillcolor);
                        ImageLine($im, ($yCoord + $j * 30) + 10, ($xCoord + $i * 30) + 10, ($yCoord + $j * 30), ($xCoord + $i * 30) - 10, $fillcolor);
                        ImageTTFText($im, 7, 0, ($yCoord + $j * 30 - 1), ($xCoord + $i * 30 + 10), $linecolor, $font, '4');
                    } elseif ($mas[$top][$j] == 17) {
                        ImageLine($im, ($yCoord + $j * 30) - 10, ($xCoord + $i * 30) + 10, ($yCoord + $j * 30), ($xCoord + $i * 30) - 10, $fillcolor);
                        ImageLine($im, ($yCoord + $j * 30) + 10, ($xCoord + $i * 30) + 10, ($yCoord + $j * 30), ($xCoord + $i * 30) - 10, $fillcolor);
                        for ($th = 0; $th < 1; $th++) {
                            for ($p = -5; $p <= 5; $p++) {
                                imageSetPixel($im, ($yCoord + $j * 30) + $p, ($xCoord + $i * 30) + $th, $linecolor);
                            }
                        }
                        ImageTTFText($im, 7, 0, ($yCoord + $j * 30 - 1), ($xCoord + $i * 30 + 10), $linecolor, $font, '4');
                    } else {
                        //imageChar($im, 2,($yCoord+$j*30-2) ,($xCoord+$i*30-7) , $mas[$top][$j], $black);
                        //ImageTTFText($im,8,0,($yCoord+$j*30-4),($xCoord+$i*30+4),$linecolor,$font,$mas[$top][$j]);
                        //imageChar($im, 2,($yCoord+$j*30-5) ,($xCoord+$i*30-2) , $mas[$top][$j], $redcolor);
                    }
                }
                if ($j > 0) ImageLine($im, ($j * 30 + 15), $imageheight - 15, ($j * 30 + 15), 15, $linecolor);
            }
            if ($i > 0) ImageLine($im, 15, ($i * 30 + 15), $imagewidth - 45, ($i * 30 + 15), $linecolor);
            //$top--;
        }
    }
    if (isset($title)) ImageTTFText($im, $size, 0, 5, 10, $black, $font, $title);
    ImageRectangle($im, 15, 15, $imagewidth - 45, $imageheight - 15, $black);
    if (isset($numbering) && ($numbering == 'all')) {
        $top = $h;
        for ($i = 1; $i <= $h; $i++) {
            $yCoord = 0;
            if ($top % 2 != 0) ImageLine($im, $imagewidth - ($size + 2) - 30, ($yCoord + $i * 30), $imagewidth - ($size + 2) - 32, ($yCoord + $i * 30), $black);
            if ($top % 2 != 0) ImageString($im, 2, $imagewidth - 30, ($yCoord + $i * 30 - 7), $top, $black);
            $top--;
        }
        $top = $h;
        for ($i = 1; $i <= $h; $i++) {
            $yCoord = 0;
            if ($top % 2 == 0) ImageLine($im, 12, ($yCoord + $i * 30), 16, ($yCoord + $i * 30), $black);
            if ($top % 2 == 0) ImageString($im, 2, 2, ($yCoord + $i * 30 - 7), $top, $black);
            $top--;
        }
    } elseif (isset($numbering) && ($numbering == 'odd')) {
        $str = $h * 2 - 1;
        for ($i = 1; $i <= $h; $i++) {
            $yCoord = 0;
            ImageLine($im, $imagewidth - ($size + 2) - 30, ($yCoord + $i * 30), $imagewidth - ($size + 2) - 32, ($yCoord + $i * 30), $black);
            ImageString($im, 2, $imagewidth - 30, ($yCoord + $i * 30 - 7), $str, $black);
            $str -= 2;
        }
    } elseif (isset($numbering) && ($numbering == 'even')) {
        $str = 2 * $h;
        for ($i = 1; $i <= $h; $i++) {
            $yCoord = 0;
            ImageLine($im, 12, ($yCoord + $i * 30), 16, ($yCoord + $i * 30), $black);
            ImageString($im, 2, 2, ($yCoord + $i * 30 - 7), $str, $black);
            $str -= 2;
        }
    } else {
        $str = $h * 2 - 1;
        for ($i = 1; $i <= $h; $i++) {
            $yCoord = 0;
            ImageLine($im, $imagewidth - ($size + 2) - 30, ($yCoord + $i * 30), $imagewidth - ($size + 2) - 32, ($yCoord + $i * 30), $black);
            ImageString($im, 2, $imagewidth - 30, ($yCoord + $i * 30 - 7), $str, $black);
            $str -= 2;
        }
    }
    for ($i = $w; $i >= 1; $i--) {
        $xCoord = $imagewidth - 30;
        ImageLine($im, ($xCoord + $i * 30), $imageheight - 13, ($xCoord + $i * 30), $imageheight - 15, $black);
        ImageString($im, 2, ($xCoord - $i * 30 - 2), $imageheight - 13, $i, $black);
    }
    //ImageLine($im, 20, $imageheight-18, 20 ,0, $black);
    //Imagettftext($im,$size,$fontangle,$xcord,$ycord,$black,$font,$adr);
    //Imagestring($im, $size, $imagewidth -170, $imageheight-15, "http://szawl.eu", $black);
    //ImageTTFText($im,$size,0,2,$imageheight-2,$black,$font,"http://szawl.eu");
    imageStringUp($im, 2, $imagewidth - ($size + 2), $imageheight - 2, "http://szawl.eu/chart/", $black);
    imagepng($im);
    ImageDestroy($im);
} else {
    die('no data');
}
?>
