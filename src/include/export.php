<?php
session_start();

if (isset($_SESSION['duomenys'])) {
    $json_data = trim($_SESSION['duomenys']);
    $values = json_decode($json_data);
    unset($_SESSION['duomenys']);
    if (function_exists('json_last_error')) {
        switch (json_last_error()) {
            case JSON_ERROR_NONE:
                //echo ' - Ошибок нет';
                break;
            case JSON_ERROR_DEPTH:
                exit();
                break;
            case JSON_ERROR_STATE_MISMATCH:
                exit();
                break;
            case JSON_ERROR_CTRL_CHAR:
                exit();
                break;
            case JSON_ERROR_SYNTAX:
                exit();
                break;
            case JSON_ERROR_UTF8:
                exit();
                break;
            default:
                exit();
                break;
        }
    }

    if (!empty($values)) {
        $title = trim($values->title);
        if (isset($title)) {
            $filename = $title;
        } else {
            $filename = "chart_" . date('Y') . "_" . date('n') . "_" . date('j') . "_" . date('G') . "_" . date('i') . "_" . date('s');
        }

        $duom = str_replace(array(" ", '&', '\''), array("_", '&amp;', '&apos;'), $json_data);
        $ext = "json";   // file extension
        $mime_type = (strstr($_SERVER["HTTP_USER_AGENT"], "MSIE") == true || strstr($_SERVER["HTTP_USER_AGENT"], "Opera") == true) ? 'application/octetstream' : 'application/octet-stream';
        header('Content-Type: ' . $mime_type);
        if (strstr($_SERVER["HTTP_USER_AGENT"], "MSIE") == false) {
            header('Content-Disposition: inline; filename="' . $filename . '.' . $ext . '"');
            header("Content-Transfer-Encoding: binary");
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            print $duom;

        } else {
            header('Content-Disposition: attachment; filename="' . $filename . '.' . $ext . '"');
            header("Content-Transfer-Encoding: binary");
            header('Expires: 0');
            header('Pragma: no-cache');
            print $duom;
        }

    }
    exit();

} else {
    exit();
}
