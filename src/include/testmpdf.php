<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
use Mpdf\Mpdf;

require_once __DIR__ . '/../vendor/autoload.php';
//var_dump(__DIR__);
$mpdf = new Mpdf(['tempDir' => __DIR__ . '/../tmp']);
//var_dump($mpdf);
$mpdf->WriteHTML('<h1>Hello world!</h1>');
$mpdf->Output();
