<?php

error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);


if (!isset($dir) && empty($dir)) {
    $dir='';
}
$lng='en';
$file_name='./'.$dir.'admin/document.json';
if( !isset($_SESSION['chart_generator_szawl_visitor']['lang']) && empty($_SESSION['chart_generator_szawl_visitor']['lang'])) {
    $_SESSION['chart_generator_szawl_visitor']['lang']=get_client_language(array('ru','lt','pl','uk','fr','de','es','it','fi'),'en');
} 
if (isset($_POST['lang_sel']) && !empty($_POST['lang_sel'])) {
    $_SESSION['chart_generator_szawl_visitor']['lang'] = trim($_POST['lang_sel']);
}
$lang=null;
if(isset($file_name) && !empty($file_name) && file_exists($file_name)) {
    $source=file_get_contents($file_name);
    if(!empty($source)) {
        $mas_source=json_decode($source);
        if (!empty($mas_source) && is_object($mas_source)) {
            $lang=array();
            if (0 === strcmp($_SESSION['chart_generator_szawl_visitor']['lang'], 'en')) {
                
                $lang['title'] = (string)$mas_source->title->en;
                $lang['keywords'] = (string)$mas_source->keywords->en;
                $lang['author'] = (string)$mas_source->author->en;
                $lang['description'] = (string)$mas_source->description->en;
                $lang['writer'] = (string)$mas_source->writer->en;
                $lang['contacts'] = (string)$mas_source->contacts->en;
                $lang['url'] = (string)$mas_source->url->en;
                $lang['lang'] = (string)$mas_source->lang->en;
                $lang['draw'] = (string)$mas_source->draw->en;
                $lang['clear'] = (string)$mas_source->clear->en;
                $lang['clearsure'] = (string)$mas_source->clearsure->en;
                $lang['all'] = (string)$mas_source->all->en;
                $lang['odd'] = (string)$mas_source->odd->en;
                $lang['even'] = (string)$mas_source->even->en;
                $lang['keys'] = (string)$mas_source->keys->en;
                $lang['chart_name'] = (string)$mas_source->chart_name->en;
                $lang['export_txt'] = (string)$mas_source->export_txt->en;
                $lang['export_image'] = (string)$mas_source->export_image->en;
                $lang['export_pdf'] = (string)$mas_source->export_pdf->en;
                $lang['export_json'] = (string)$mas_source->export_json->en;
                $lang['tools'] = (string)$mas_source->tools->en;
                $lang['error1'] = (string)$mas_source->error1->en;
                $lang['error2'] = (string)$mas_source->error2->en;
                $lang['error3'] = (string)$mas_source->error3->en;
                $lang['error4'] = (string)$mas_source->error4->en;
                $lang['weekdays'] = $mas_source->weekdays->en;
                $lang['months'] = array(
                    (string)$mas_source->mounths->mounth_1->en,
                    (string)$mas_source->mounths->mounth_2->en,
                    (string)$mas_source->mounths->mounth_3->en,
                    (string)$mas_source->mounths->mounth_4->en,
                    (string)$mas_source->mounths->mounth_5->en,
                    (string)$mas_source->mounths->mounth_6->en,
                    (string)$mas_source->mounths->mounth_7->en,
                    (string)$mas_source->mounths->mounth_8->en,
                    (string)$mas_source->mounths->mounth_9->en,
                    (string)$mas_source->mounths->mounth_10->en,
                    (string)$mas_source->mounths->mounth_11->en,
                    (string)$mas_source->mounths->mounth_12->en);
                $lang['readlast']=10;
                $lang['copyrights'] = ((string)$mas_source->copyrights->en).date('Y');
                $lang['nomessages'] = (string)$mas_source->nomessages->en;
                $lang['share'] = (string)$mas_source->share->en;
                $lang['readpost'] = (string)$mas_source->readpost->en;
                $lang['commentpost'] = (string)$mas_source->commentpost->en;
                $lang['tagspost'] = (string)$mas_source->tagspost->en;
                $lang['back'] = (string)$mas_source->back->en;
                $lang['nohaking'] = (string)$mas_source->nohaking->en;
                $lang['pdfgenerated']=(string)$mas_source->pdfgenerated->en;
                $lang['langbar'] = $mas_source->langbar;
                $lang['menu_shawl'] = (string)$mas_source->menu_shawl->en;
                $lang['menu_knit_doily'] = (string)$mas_source->menu_knit_doily->en;
                $lang['menu_tablecloths'] = (string)$mas_source->menu_tablecloths->en;
                $lang['menu_baktus_scarf'] = (string)$mas_source->menu_baktus_scarf->en;
                $lang['menu_pullover'] = (string)$mas_source->menu_pullover->en;
                $lang['welcome'] = (string)$mas_source->welcome->en;
                $lang['helpshow'] = (string)$mas_source->helpshow->en;
                $lang['and_save'] = (string)$mas_source->and_save->en;
                $lang['move'] = (string)$mas_source->move->en;
                $lang['helpwrite'] = array(
                    $mas_source->helpwrite->helwrite_1->en,
                    $mas_source->helpwrite->helwrite_2->en,
                    $mas_source->helpwrite->helwrite_3->en,
                    $mas_source->helpwrite->helwrite_4->en);
                $lang['helphide'] = (string)$mas_source->helphide->en;
                $lang['selecthide']=(string)$mas_source->selecthide->en;
                $lang['keysdes'] = array(
                    (string)$mas_source->keysdes->keysdes_0->en,
                    (string)$mas_source->keysdes->keysdes_1->en,
                    (string)$mas_source->keysdes->keysdes_2->en,
                    (string)$mas_source->keysdes->keysdes_3->en,
                    (string)$mas_source->keysdes->keysdes_4->en,
                    (string)$mas_source->keysdes->keysdes_5->en,
                    (string)$mas_source->keysdes->keysdes_6->en,
                    (string)$mas_source->keysdes->keysdes_7->en,
                    (string)$mas_source->keysdes->keysdes_8->en,
                    (string)$mas_source->keysdes->keysdes_9->en,
                    (string)$mas_source->keysdes->keysdes_10->en,
                    (string)$mas_source->keysdes->keysdes_11->en,
                    (string)$mas_source->keysdes->keysdes_12->en,
                    (string)$mas_source->keysdes->keysdes_13->en,
                    (string)$mas_source->keysdes->keysdes_14->en,
                    (string)$mas_source->keysdes->keysdes_15->en,
                    (string)$mas_source->keysdes->keysdes_16->en,
                    (string)$mas_source->keysdes->keysdes_17->en,
                    (string)$mas_source->keysdes->keysdes_X->en);
                $lang['row'] = (string)$mas_source->row->en;
                $lang['column'] = (string)$mas_source->column->en;
                $lang['suredeleterow']=(string)$mas_source->suredeleterow->en;
                $lang['suredeletecolumn']=(string)$mas_source->suredeletecolumn->en;
                $lang['writesupport']=(string)$mas_source->writesupport->en;
                $lang['faq'] = (string)$mas_source->faq->en;
                $lang['step'] = (string)$mas_source->step->en;
                $lang['step1'] = (string)$mas_source->step1->en;
                $lang['step2'] = (string)$mas_source->step2->en;
                $lang['step3'] = (string)$mas_source->step3->en;
                $lang['step4'] = (string)$mas_source->step4->en;
                $lang['step5'] = (string)$mas_source->step5->en;
                $lang['step6'] = (string)$mas_source->step6->en;
                $lang['step7'] = (string)$mas_source->step7->en;
                $lang['change_row'] = (string)$mas_source->change_row->en;
                $lang['change_column'] = (string)$mas_source->change_column->en;
                $lang['wait_to_download'] = (string)$mas_source->wait_to_download->en;
                $lang['reset_canvas'] = (string)$mas_source->reset_canvas->en;
                $lang['delete_block'] = (string)$mas_source->delete_block->en;
                $lang['add'] = (string)$mas_source->add->en;
                $lang['remove'] = (string)$mas_source->remove->en;
                
            } else if (0 === strcmp($_SESSION['chart_generator_szawl_visitor']['lang'] ,'ru')) {
                
                $lang['title'] = (string)$mas_source->title->ru;
                $lang['keywords'] = (string)$mas_source->keywords->ru;
                $lang['author'] = (string)$mas_source->author->ru;
                $lang['description'] = (string)$mas_source->description->ru;
                $lang['writer'] = (string)$mas_source->writer->ru;
                $lang['contacts'] = (string)$mas_source->contacts->ru;
                $lang['url'] = (string)$mas_source->url->ru;
                $lang['lang'] = (string)$mas_source->lang->ru;
                $lang['draw'] = (string)$mas_source->draw->ru;
                $lang['clear'] = (string)$mas_source->clear->ru;
                $lang['clearsure'] = (string)$mas_source->clearsure->ru;
                $lang['all'] = (string)$mas_source->all->ru;
                $lang['odd'] = (string)$mas_source->odd->ru;
                $lang['even'] = (string)$mas_source->even->ru;
                $lang['keys'] = (string)$mas_source->keys->ru;
                $lang['chart_name'] = (string)$mas_source->chart_name->ru;
                $lang['export_txt'] = (string)$mas_source->export_txt->ru;
                $lang['export_image'] = (string)$mas_source->export_image->ru;
                $lang['export_pdf'] = (string)$mas_source->export_pdf->ru;
                $lang['export_json'] = (string)$mas_source->export_json->ru;
                $lang['tools'] = (string)$mas_source->tools->ru;
                $lang['error1'] = (string)$mas_source->error1->ru;
                $lang['error2'] = (string)$mas_source->error2->ru;
                $lang['error3'] = (string)$mas_source->error3->ru;
                $lang['error4'] = (string)$mas_source->error4->ru;
                $lang['weekdays'] = $mas_source->weekdays->ru;
                $lang['months'] = array(
                    (string)$mas_source->mounths->mounth_1->ru,
                    (string)$mas_source->mounths->mounth_2->ru,
                    (string)$mas_source->mounths->mounth_3->ru,
                    (string)$mas_source->mounths->mounth_4->ru,
                    (string)$mas_source->mounths->mounth_5->ru,
                    (string)$mas_source->mounths->mounth_6->ru,
                    (string)$mas_source->mounths->mounth_7->ru,
                    (string)$mas_source->mounths->mounth_8->ru,
                    (string)$mas_source->mounths->mounth_9->ru,
                    (string)$mas_source->mounths->mounth_10->ru,
                    (string)$mas_source->mounths->mounth_11->ru,
                    (string)$mas_source->mounths->mounth_12->ru);
                $lang['readlast']=10;
                $lang['copyrights'] = ((string)$mas_source->copyrights->ru).date('Y');
                $lang['nomessages'] = (string)$mas_source->nomessages->ru;
                $lang['share'] = (string)$mas_source->share->ru;
                $lang['readpost'] = (string)$mas_source->readpost->ru;
                $lang['commentpost'] = (string)$mas_source->commentpost->ru;
                $lang['tagspost'] = (string)$mas_source->tagspost->ru;
                $lang['back'] = (string)$mas_source->back->ru;
                $lang['nohaking'] = (string)$mas_source->nohaking->ru;
                $lang['pdfgenerated']=(string)$mas_source->pdfgenerated->ru;
                $lang['langbar'] = $mas_source->langbar;
                $lang['menu_shawl'] = (string)$mas_source->menu_shawl->ru;
                $lang['menu_knit_doily'] = (string)$mas_source->menu_knit_doily->ru;
                $lang['menu_tablecloths'] = (string)$mas_source->menu_tablecloths->ru;
                $lang['menu_baktus_scarf'] = (string)$mas_source->menu_baktus_scarf->ru;
                $lang['menu_pullover'] = (string)$mas_source->menu_pullover->ru;
                $lang['welcome'] = (string)$mas_source->welcome->ru;
                $lang['helpshow'] = (string)$mas_source->helpshow->ru;
                $lang['and_save'] = (string)$mas_source->and_save->ru;
                $lang['move'] = (string)$mas_source->move->ru;
                $lang['helpwrite'] = array(
                    $mas_source->helpwrite->helwrite_1->ru,
                    $mas_source->helpwrite->helwrite_2->ru,
                    $mas_source->helpwrite->helwrite_3->ru,
                    $mas_source->helpwrite->helwrite_4->ru);
                $lang['helphide'] = (string)$mas_source->helphide->ru;
                $lang['selecthide']=(string)$mas_source->selecthide->ru;
                $lang['keysdes'] = array(
                    (string)$mas_source->keysdes->keysdes_0->ru,
                    (string)$mas_source->keysdes->keysdes_1->ru,
                    (string)$mas_source->keysdes->keysdes_2->ru,
                    (string)$mas_source->keysdes->keysdes_3->ru,
                    (string)$mas_source->keysdes->keysdes_4->ru,
                    (string)$mas_source->keysdes->keysdes_5->ru,
                    (string)$mas_source->keysdes->keysdes_6->ru,
                    (string)$mas_source->keysdes->keysdes_7->ru,
                    (string)$mas_source->keysdes->keysdes_8->ru,
                    (string)$mas_source->keysdes->keysdes_9->ru,
                    (string)$mas_source->keysdes->keysdes_10->ru,
                    (string)$mas_source->keysdes->keysdes_11->ru,
                    (string)$mas_source->keysdes->keysdes_12->ru,
                    (string)$mas_source->keysdes->keysdes_13->ru,
                    (string)$mas_source->keysdes->keysdes_14->ru,
                    (string)$mas_source->keysdes->keysdes_15->ru,
                    (string)$mas_source->keysdes->keysdes_16->ru,
                    (string)$mas_source->keysdes->keysdes_17->ru,
                    (string)$mas_source->keysdes->keysdes_X->ru);
                $lang['row'] = (string)$mas_source->row->ru;
                $lang['column'] = (string)$mas_source->column->ru;
                $lang['suredeleterow']=(string)$mas_source->suredeleterow->ru;
                $lang['suredeletecolumn']=(string)$mas_source->suredeletecolumn->ru;
                $lang['writesupport']=(string)$mas_source->writesupport->ru;
                $lang['faq'] = (string)$mas_source->faq->ru;
                $lang['step'] = (string)$mas_source->step->ru;
                $lang['step1'] = (string)$mas_source->step1->ru;
                $lang['step2'] = (string)$mas_source->step2->ru;
                $lang['step3'] = (string)$mas_source->step3->ru;
                $lang['step4'] = (string)$mas_source->step4->ru;
                $lang['step5'] = (string)$mas_source->step5->ru;
                $lang['step6'] = (string)$mas_source->step6->ru;
                $lang['step7'] = (string)$mas_source->step7->ru;
                $lang['change_row'] = (string)$mas_source->change_row->ru;
                $lang['change_column'] = (string)$mas_source->change_column->ru;
                $lang['wait_to_download'] = (string)$mas_source->wait_to_download->ru;
                $lang['reset_canvas'] = (string)$mas_source->reset_canvas->ru;
                $lang['delete_block'] = (string)$mas_source->delete_block->ru;
                $lang['add'] = (string)$mas_source->add->ru;
                $lang['remove'] = (string)$mas_source->remove->ru;
                
            } else if (0 === strcmp($_SESSION['chart_generator_szawl_visitor']['lang'], 'pl')) {
                
                $lang['title'] = (string)$mas_source->title->pl;
                $lang['keywords'] = (string)$mas_source->keywords->pl;
                $lang['author'] = (string)$mas_source->author->pl;
                $lang['description'] = (string)$mas_source->description->pl;
                $lang['writer'] = (string)$mas_source->writer->pl;
                $lang['contacts'] = (string)$mas_source->contacts->pl;
                $lang['url'] = (string)$mas_source->url->pl;
                $lang['lang'] = (string)$mas_source->lang->pl;
                $lang['draw'] = (string)$mas_source->draw->pl;
                $lang['clear'] = (string)$mas_source->clear->pl;
                $lang['clearsure'] = (string)$mas_source->clearsure->pl;
                $lang['all'] = (string)$mas_source->all->pl;
                $lang['odd'] = (string)$mas_source->odd->pl;
                $lang['even'] = (string)$mas_source->even->pl;
                $lang['keys'] = (string)$mas_source->keys->pl;
                $lang['chart_name'] = (string)$mas_source->chart_name->pl;
                $lang['export_txt'] = (string)$mas_source->export_txt->pl;
                $lang['export_image'] = (string)$mas_source->export_image->pl;
                $lang['export_pdf'] = (string)$mas_source->export_pdf->pl;
                $lang['export_json'] = (string)$mas_source->export_json->pl;
                $lang['tools'] = (string)$mas_source->tools->pl;
                $lang['error1'] = (string)$mas_source->error1->pl;
                $lang['error2'] = (string)$mas_source->error2->pl;
                $lang['error3'] = (string)$mas_source->error3->pl;
                $lang['error4'] = (string)$mas_source->error4->pl;
                $lang['weekdays'] = $mas_source->weekdays->pl;
                $lang['months'] = array(
                    (string)$mas_source->mounths->mounth_1->pl,
                    (string)$mas_source->mounths->mounth_2->pl,
                    (string)$mas_source->mounths->mounth_3->pl,
                    (string)$mas_source->mounths->mounth_4->pl,
                    (string)$mas_source->mounths->mounth_5->pl,
                    (string)$mas_source->mounths->mounth_6->pl,
                    (string)$mas_source->mounths->mounth_7->pl,
                    (string)$mas_source->mounths->mounth_8->pl,
                    (string)$mas_source->mounths->mounth_9->pl,
                    (string)$mas_source->mounths->mounth_10->pl,
                    (string)$mas_source->mounths->mounth_11->pl,
                    (string)$mas_source->mounths->mounth_12->pl);
                $lang['readlast']=10;
                $lang['copyrights'] = ((string)$mas_source->copyrights->pl).date('Y');
                $lang['nomessages'] = (string)$mas_source->nomessages->pl;
                $lang['share'] = (string)$mas_source->share->pl;
                $lang['readpost'] = (string)$mas_source->readpost->pl;
                $lang['commentpost'] = (string)$mas_source->commentpost->pl;
                $lang['tagspost'] = (string)$mas_source->tagspost->pl;
                $lang['back'] = (string)$mas_source->back->pl;
                $lang['nohaking'] = (string)$mas_source->nohaking->pl;
                $lang['pdfgenerated']=(string)$mas_source->pdfgenerated->pl;
                $lang['langbar'] = $mas_source->langbar;
                $lang['menu_shawl'] = (string)$mas_source->menu_shawl->pl;
                $lang['menu_knit_doily'] = (string)$mas_source->menu_knit_doily->pl;
                $lang['menu_tablecloths'] = (string)$mas_source->menu_tablecloths->pl;
                $lang['menu_baktus_scarf'] = (string)$mas_source->menu_baktus_scarf->pl;
                $lang['menu_pullover'] = (string)$mas_source->menu_pullover->pl;
                $lang['welcome'] = (string)$mas_source->welcome->pl;
                $lang['helpshow'] = (string)$mas_source->helpshow->pl;
                $lang['and_save'] = (string)$mas_source->and_save->pl;
                $lang['move'] = (string)$mas_source->move->pl;
                $lang['helpwrite'] = array(
                    $mas_source->helpwrite->helwrite_1->pl,
                    $mas_source->helpwrite->helwrite_2->pl,
                    $mas_source->helpwrite->helwrite_3->pl,
                    $mas_source->helpwrite->helwrite_4->pl);
                $lang['helphide'] = (string)$mas_source->helphide->pl;
                $lang['selecthide']=(string)$mas_source->selecthide->pl;
                $lang['keysdes'] = array(
                    (string)$mas_source->keysdes->keysdes_0->pl,
                    (string)$mas_source->keysdes->keysdes_1->pl,
                    (string)$mas_source->keysdes->keysdes_2->pl,
                    (string)$mas_source->keysdes->keysdes_3->pl,
                    (string)$mas_source->keysdes->keysdes_4->pl,
                    (string)$mas_source->keysdes->keysdes_5->pl,
                    (string)$mas_source->keysdes->keysdes_6->pl,
                    (string)$mas_source->keysdes->keysdes_7->pl,
                    (string)$mas_source->keysdes->keysdes_8->pl,
                    (string)$mas_source->keysdes->keysdes_9->pl,
                    (string)$mas_source->keysdes->keysdes_10->pl,
                    (string)$mas_source->keysdes->keysdes_11->pl,
                    (string)$mas_source->keysdes->keysdes_12->pl,
                    (string)$mas_source->keysdes->keysdes_13->pl,
                    (string)$mas_source->keysdes->keysdes_14->pl,
                    (string)$mas_source->keysdes->keysdes_15->pl,
                    (string)$mas_source->keysdes->keysdes_16->pl,
                    (string)$mas_source->keysdes->keysdes_17->pl,
                    (string)$mas_source->keysdes->keysdes_X->pl);
                $lang['row'] = (string)$mas_source->row->pl;
                $lang['column'] = (string)$mas_source->column->pl;
                $lang['suredeleterow']=(string)$mas_source->suredeleterow->pl;
                $lang['suredeletecolumn']=(string)$mas_source->suredeletecolumn->pl;
                $lang['writesupport']=(string)$mas_source->writesupport->pl;
                $lang['faq'] = (string)$mas_source->faq->pl;
                $lang['step'] = (string)$mas_source->step->pl;
                $lang['step1'] = (string)$mas_source->step1->pl;
                $lang['step2'] = (string)$mas_source->step2->pl;
                $lang['step3'] = (string)$mas_source->step3->pl;
                $lang['step4'] = (string)$mas_source->step4->pl;
                $lang['step5'] = (string)$mas_source->step5->pl;
                $lang['step6'] = (string)$mas_source->step6->pl;
                $lang['step7'] = (string)$mas_source->step7->pl;
                $lang['change_row'] = (string)$mas_source->change_row->pl;
                $lang['change_column'] = (string)$mas_source->change_column->pl;
                $lang['wait_to_download'] = (string)$mas_source->wait_to_download->pl;
                $lang['reset_canvas'] = (string)$mas_source->reset_canvas->pl;
                $lang['delete_block'] = (string)$mas_source->delete_block->pl;
                $lang['add'] = (string)$mas_source->add->pl;
                $lang['remove'] = (string)$mas_source->remove->pl;
                
            } else if (0 === strcmp($_SESSION['chart_generator_szawl_visitor']['lang'], 'lt')) {
                
                $lang['title'] = (string)$mas_source->title->lt;
                $lang['keywords'] = (string)$mas_source->keywords->lt;
                $lang['author'] = (string)$mas_source->author->lt;
                $lang['description'] = (string)$mas_source->description->lt;
                $lang['writer'] = (string)$mas_source->writer->lt;
                $lang['contacts'] = (string)$mas_source->contacts->lt;
                $lang['url'] = (string)$mas_source->url->lt;
                $lang['lang'] = (string)$mas_source->lang->lt;
                $lang['draw'] = (string)$mas_source->draw->lt;
                $lang['clear'] = (string)$mas_source->clear->lt;
                $lang['clearsure'] = (string)$mas_source->clearsure->lt;
                $lang['all'] = (string)$mas_source->all->lt;
                $lang['odd'] = (string)$mas_source->odd->lt;
                $lang['even'] = (string)$mas_source->even->lt;
                $lang['keys'] = (string)$mas_source->keys->lt;
                $lang['chart_name'] = (string)$mas_source->chart_name->lt;
                $lang['export_txt'] = (string)$mas_source->export_txt->lt;
                $lang['export_image'] = (string)$mas_source->export_image->lt;
                $lang['export_pdf'] = (string)$mas_source->export_pdf->lt;
                $lang['export_json'] = (string)$mas_source->export_json->lt;
                $lang['tools'] = (string)$mas_source->tools->lt;
                $lang['error1'] = (string)$mas_source->error1->lt;
                $lang['error2'] = (string)$mas_source->error2->lt;
                $lang['error3'] = (string)$mas_source->error3->lt;
                $lang['error4'] = (string)$mas_source->error4->lt;
                $lang['weekdays'] = $mas_source->weekdays->lt;
                $lang['months'] = array(
                    (string)$mas_source->mounths->mounth_1->lt,
                    (string)$mas_source->mounths->mounth_2->lt,
                    (string)$mas_source->mounths->mounth_3->lt,
                    (string)$mas_source->mounths->mounth_4->lt,
                    (string)$mas_source->mounths->mounth_5->lt,
                    (string)$mas_source->mounths->mounth_6->lt,
                    (string)$mas_source->mounths->mounth_7->lt,
                    (string)$mas_source->mounths->mounth_8->lt,
                    (string)$mas_source->mounths->mounth_9->lt,
                    (string)$mas_source->mounths->mounth_10->lt,
                    (string)$mas_source->mounths->mounth_11->lt,
                    (string)$mas_source->mounths->mounth_12->lt);
                $lang['readlast']=10;
                $lang['copyrights'] = ((string)$mas_source->copyrights->lt).date('Y');
                $lang['nomessages'] = (string)$mas_source->nomessages->lt;
                $lang['share'] = (string)$mas_source->share->lt;
                $lang['readpost'] = (string)$mas_source->readpost->lt;
                $lang['commentpost'] = (string)$mas_source->commentpost->lt;
                $lang['tagspost'] = (string)$mas_source->tagspost->lt;
                $lang['back'] = (string)$mas_source->back->lt;
                $lang['nohaking'] = (string)$mas_source->nohaking->lt;
                $lang['pdfgenerated']=(string)$mas_source->pdfgenerated->lt;
                $lang['langbar'] = $mas_source->langbar;
                $lang['menu_shawl'] = (string)$mas_source->menu_shawl->lt;
                $lang['menu_knit_doily'] = (string)$mas_source->menu_knit_doily->lt;
                $lang['menu_tablecloths'] = (string)$mas_source->menu_tablecloths->lt;
                $lang['menu_baktus_scarf'] = (string)$mas_source->menu_baktus_scarf->lt;
                $lang['menu_pullover'] = (string)$mas_source->menu_pullover->lt;
                $lang['welcome'] = (string)$mas_source->welcome->lt;
                $lang['helpshow'] = (string)$mas_source->helpshow->lt;
                $lang['and_save'] = (string)$mas_source->and_save->lt;
                $lang['move'] = (string)$mas_source->move->lt;
                $lang['helpwrite'] = array(
                    $mas_source->helpwrite->helwrite_1->lt,
                    $mas_source->helpwrite->helwrite_2->lt,
                    $mas_source->helpwrite->helwrite_3->lt,
                    $mas_source->helpwrite->helwrite_4->lt);
                $lang['helphide'] = (string)$mas_source->helphide->lt;
                $lang['selecthide']=(string)$mas_source->selecthide->lt;
                $lang['keysdes'] = array(
                    (string)$mas_source->keysdes->keysdes_0->lt,
                    (string)$mas_source->keysdes->keysdes_1->lt,
                    (string)$mas_source->keysdes->keysdes_2->lt,
                    (string)$mas_source->keysdes->keysdes_3->lt,
                    (string)$mas_source->keysdes->keysdes_4->lt,
                    (string)$mas_source->keysdes->keysdes_5->lt,
                    (string)$mas_source->keysdes->keysdes_6->lt,
                    (string)$mas_source->keysdes->keysdes_7->lt,
                    (string)$mas_source->keysdes->keysdes_8->lt,
                    (string)$mas_source->keysdes->keysdes_9->lt,
                    (string)$mas_source->keysdes->keysdes_10->lt,
                    (string)$mas_source->keysdes->keysdes_11->lt,
                    (string)$mas_source->keysdes->keysdes_12->lt,
                    (string)$mas_source->keysdes->keysdes_13->lt,
                    (string)$mas_source->keysdes->keysdes_14->lt,
                    (string)$mas_source->keysdes->keysdes_15->lt,
                    (string)$mas_source->keysdes->keysdes_16->lt,
                    (string)$mas_source->keysdes->keysdes_17->lt,
                    (string)$mas_source->keysdes->keysdes_X->lt);
                $lang['row'] = (string)$mas_source->row->lt;
                $lang['column'] = (string)$mas_source->column->lt;
                $lang['suredeleterow']=(string)$mas_source->suredeleterow->lt;
                $lang['suredeletecolumn']=(string)$mas_source->suredeletecolumn->lt;
                $lang['writesupport']=(string)$mas_source->writesupport->lt;
                $lang['faq'] = (string)$mas_source->faq->lt;
                $lang['step'] = (string)$mas_source->step->lt;
                $lang['step1'] = (string)$mas_source->step1->lt;
                $lang['step2'] = (string)$mas_source->step2->lt;
                $lang['step3'] = (string)$mas_source->step3->lt;
                $lang['step4'] = (string)$mas_source->step4->lt;
                $lang['step5'] = (string)$mas_source->step5->lt;
                $lang['step6'] = (string)$mas_source->step6->lt;
                $lang['step7'] = (string)$mas_source->step7->lt;
                $lang['change_row'] = (string)$mas_source->change_row->lt;
                $lang['change_column'] = (string)$mas_source->change_column->lt;
                $lang['wait_to_download'] = (string)$mas_source->wait_to_download->lt;
                $lang['reset_canvas'] = (string)$mas_source->reset_canvas->lt;
                $lang['delete_block'] = (string)$mas_source->delete_block->lt;
                $lang['add'] = (string)$mas_source->add->lt;
                $lang['remove'] = (string)$mas_source->remove->lt;
                
            } else if (0 === strcmp($_SESSION['chart_generator_szawl_visitor']['lang'], 'fr')) {
                
                $lang['title'] = (string)$mas_source->title->fr;
                $lang['keywords'] = (string)$mas_source->keywords->fr;
                $lang['author'] = (string)$mas_source->author->fr;
                $lang['description'] = (string)$mas_source->description->fr;
                $lang['writer'] = (string)$mas_source->writer->fr;
                $lang['contacts'] = (string)$mas_source->contacts->fr;
                $lang['url'] = (string)$mas_source->url->fr;
                $lang['lang'] = (string)$mas_source->lang->fr;
                $lang['draw'] = (string)$mas_source->draw->fr;
                $lang['clear'] = (string)$mas_source->clear->fr;
                $lang['clearsure'] = (string)$mas_source->clearsure->fr;
                $lang['all'] = (string)$mas_source->all->fr;
                $lang['odd'] = (string)$mas_source->odd->fr;
                $lang['even'] = (string)$mas_source->even->fr;
                $lang['keys'] = (string)$mas_source->keys->fr;
                $lang['chart_name'] = (string)$mas_source->chart_name->fr;
                $lang['export_txt'] = (string)$mas_source->export_txt->fr;
                $lang['export_image'] = (string)$mas_source->export_image->fr;
                $lang['export_pdf'] = (string)$mas_source->export_pdf->fr;
                $lang['export_json'] = (string)$mas_source->export_json->fr;
                $lang['tools'] = (string)$mas_source->tools->fr;
                $lang['error1'] = (string)$mas_source->error1->fr;
                $lang['error2'] = (string)$mas_source->error2->fr;
                $lang['error3'] = (string)$mas_source->error3->fr;
                $lang['error4'] = (string)$mas_source->error4->fr;
                $lang['weekdays'] = $mas_source->weekdays->fr;
                $lang['months'] = array(
                    (string)$mas_source->mounths->mounth_1->fr,
                    (string)$mas_source->mounths->mounth_2->fr,
                    (string)$mas_source->mounths->mounth_3->fr,
                    (string)$mas_source->mounths->mounth_4->fr,
                    (string)$mas_source->mounths->mounth_5->fr,
                    (string)$mas_source->mounths->mounth_6->fr,
                    (string)$mas_source->mounths->mounth_7->fr,
                    (string)$mas_source->mounths->mounth_8->fr,
                    (string)$mas_source->mounths->mounth_9->fr,
                    (string)$mas_source->mounths->mounth_10->fr,
                    (string)$mas_source->mounths->mounth_11->fr,
                    (string)$mas_source->mounths->mounth_12->fr);
                $lang['readlast']=10;
                $lang['copyrights'] = ((string)$mas_source->copyrights->fr).date('Y');
                $lang['nomessages'] = (string)$mas_source->nomessages->fr;
                $lang['share'] = (string)$mas_source->share->fr;
                $lang['readpost'] = (string)$mas_source->readpost->fr;
                $lang['commentpost'] = (string)$mas_source->commentpost->fr;
                $lang['tagspost'] = (string)$mas_source->tagspost->fr;
                $lang['back'] = (string)$mas_source->back->fr;
                $lang['nohaking'] = (string)$mas_source->nohaking->fr;
                $lang['pdfgenerated']=(string)$mas_source->pdfgenerated->fr;
                $lang['langbar'] = $mas_source->langbar;
                $lang['menu_shawl'] = (string)$mas_source->menu_shawl->fr;
                $lang['menu_knit_doily'] = (string)$mas_source->menu_knit_doily->fr;
                $lang['menu_tablecloths'] = (string)$mas_source->menu_tablecloths->fr;
                $lang['menu_baktus_scarf'] = (string)$mas_source->menu_baktus_scarf->fr;
                $lang['menu_pullover'] = (string)$mas_source->menu_pullover->fr;
                $lang['welcome'] = (string)$mas_source->welcome->fr;
                $lang['helpshow'] = (string)$mas_source->helpshow->fr;
                $lang['and_save'] = (string)$mas_source->and_save->fr;
                $lang['move'] = (string)$mas_source->move->fr;
                $lang['helpwrite'] = array(
                    $mas_source->helpwrite->helwrite_1->fr,
                    $mas_source->helpwrite->helwrite_2->fr,
                    $mas_source->helpwrite->helwrite_3->fr,
                    $mas_source->helpwrite->helwrite_4->fr);
                $lang['helphide'] = (string)$mas_source->helphide->fr;
                $lang['selecthide']=(string)$mas_source->selecthide->fr;
                $lang['keysdes'] = array(
                    (string)$mas_source->keysdes->keysdes_0->fr,
                    (string)$mas_source->keysdes->keysdes_1->fr,
                    (string)$mas_source->keysdes->keysdes_2->fr,
                    (string)$mas_source->keysdes->keysdes_3->fr,
                    (string)$mas_source->keysdes->keysdes_4->fr,
                    (string)$mas_source->keysdes->keysdes_5->fr,
                    (string)$mas_source->keysdes->keysdes_6->fr,
                    (string)$mas_source->keysdes->keysdes_7->fr,
                    (string)$mas_source->keysdes->keysdes_8->fr,
                    (string)$mas_source->keysdes->keysdes_9->fr,
                    (string)$mas_source->keysdes->keysdes_10->fr,
                    (string)$mas_source->keysdes->keysdes_11->fr,
                    (string)$mas_source->keysdes->keysdes_12->fr,
                    (string)$mas_source->keysdes->keysdes_13->fr,
                    (string)$mas_source->keysdes->keysdes_14->fr,
                    (string)$mas_source->keysdes->keysdes_15->fr,
                    (string)$mas_source->keysdes->keysdes_16->fr,
                    (string)$mas_source->keysdes->keysdes_17->fr,
                    (string)$mas_source->keysdes->keysdes_X->fr);
                $lang['row'] = (string)$mas_source->row->fr;
                $lang['column'] = (string)$mas_source->column->fr;
                $lang['suredeleterow']=(string)$mas_source->suredeleterow->fr;
                $lang['suredeletecolumn']=(string)$mas_source->suredeletecolumn->fr;
                $lang['writesupport']=(string)$mas_source->writesupport->fr;
                $lang['faq'] = (string)$mas_source->faq->fr;
                $lang['step'] = (string)$mas_source->step->fr;
                $lang['step1'] = (string)$mas_source->step1->fr;
                $lang['step2'] = (string)$mas_source->step2->fr;
                $lang['step3'] = (string)$mas_source->step3->fr;
                $lang['step4'] = (string)$mas_source->step4->fr;
                $lang['step5'] = (string)$mas_source->step5->fr;
                $lang['step6'] = (string)$mas_source->step6->fr;
                $lang['step7'] = (string)$mas_source->step7->fr;
                $lang['change_row'] = (string)$mas_source->change_row->fr;
                $lang['change_column'] = (string)$mas_source->change_column->fr;
                $lang['wait_to_download'] = (string)$mas_source->wait_to_download->fr;
                $lang['reset_canvas'] = (string)$mas_source->reset_canvas->fr;
                $lang['delete_block'] = (string)$mas_source->delete_block->fr;
                $lang['add'] = (string)$mas_source->add->fr;
                $lang['remove'] = (string)$mas_source->remove->fr;
                
            } else if (0 === strcmp($_SESSION['chart_generator_szawl_visitor']['lang'], 'de')) {
                
                $lang['title'] = (string)$mas_source->title->de;
                $lang['keywords'] = (string)$mas_source->keywords->de;
                $lang['author'] = (string)$mas_source->author->de;
                $lang['description'] = (string)$mas_source->description->de;
                $lang['writer'] = (string)$mas_source->writer->de;
                $lang['contacts'] = (string)$mas_source->contacts->de;
                $lang['url'] = (string)$mas_source->url->de;
                $lang['lang'] = (string)$mas_source->lang->de;
                $lang['draw'] = (string)$mas_source->draw->de;
                $lang['clear'] = (string)$mas_source->clear->de;
                $lang['clearsure'] = (string)$mas_source->clearsure->de;
                $lang['all'] = (string)$mas_source->all->de;
                $lang['odd'] = (string)$mas_source->odd->de;
                $lang['even'] = (string)$mas_source->even->de;
                $lang['keys'] = (string)$mas_source->keys->de;
                $lang['chart_name'] = (string)$mas_source->chart_name->de;
                $lang['export_txt'] = (string)$mas_source->export_txt->de;
                $lang['export_image'] = (string)$mas_source->export_image->de;
                $lang['export_pdf'] = (string)$mas_source->export_pdf->de;
                $lang['export_json'] = (string)$mas_source->export_json->de;
                $lang['tools'] = (string)$mas_source->tools->de;
                $lang['error1'] = (string)$mas_source->error1->de;
                $lang['error2'] = (string)$mas_source->error2->de;
                $lang['error3'] = (string)$mas_source->error3->de;
                $lang['error4'] = (string)$mas_source->error4->de;
                $lang['weekdays'] = $mas_source->weekdays->de;
                $lang['months'] = array(
                    (string)$mas_source->mounths->mounth_1->de,
                    (string)$mas_source->mounths->mounth_2->de,
                    (string)$mas_source->mounths->mounth_3->de,
                    (string)$mas_source->mounths->mounth_4->de,
                    (string)$mas_source->mounths->mounth_5->de,
                    (string)$mas_source->mounths->mounth_6->de,
                    (string)$mas_source->mounths->mounth_7->de,
                    (string)$mas_source->mounths->mounth_8->de,
                    (string)$mas_source->mounths->mounth_9->de,
                    (string)$mas_source->mounths->mounth_10->de,
                    (string)$mas_source->mounths->mounth_11->de,
                    (string)$mas_source->mounths->mounth_12->de);
                $lang['readlast']=10;
                $lang['copyrights'] = ((string)$mas_source->copyrights->de).date('Y');
                $lang['nomessages'] = (string)$mas_source->nomessages->de;
                $lang['share'] = (string)$mas_source->share->de;
                $lang['readpost'] = (string)$mas_source->readpost->de;
                $lang['commentpost'] = (string)$mas_source->commentpost->de;
                $lang['tagspost'] = (string)$mas_source->tagspost->de;
                $lang['back'] = (string)$mas_source->back->de;
                $lang['nohaking'] = (string)$mas_source->nohaking->de;
                $lang['pdfgenerated']=(string)$mas_source->pdfgenerated->de;
                $lang['langbar'] = $mas_source->langbar;
                $lang['menu_shawl'] = (string)$mas_source->menu_shawl->de;
                $lang['menu_knit_doily'] = (string)$mas_source->menu_knit_doily->de;
                $lang['menu_tablecloths'] = (string)$mas_source->menu_tablecloths->de;
                $lang['menu_baktus_scarf'] = (string)$mas_source->menu_baktus_scarf->de;
                $lang['menu_pullover'] = (string)$mas_source->menu_pullover->de;
                $lang['welcome'] = (string)$mas_source->welcome->de;
                $lang['helpshow'] = (string)$mas_source->helpshow->de;
                $lang['and_save'] = (string)$mas_source->and_save->de;
                $lang['move'] = (string)$mas_source->move->de;
                $lang['helpwrite'] = array(
                    $mas_source->helpwrite->helwrite_1->de,
                    $mas_source->helpwrite->helwrite_2->de,
                    $mas_source->helpwrite->helwrite_3->de,
                    $mas_source->helpwrite->helwrite_4->de);
                $lang['helphide'] = (string)$mas_source->helphide->de;
                $lang['selecthide']=(string)$mas_source->selecthide->de;
                $lang['keysdes'] = array(
                    (string)$mas_source->keysdes->keysdes_0->de,
                    (string)$mas_source->keysdes->keysdes_1->de,
                    (string)$mas_source->keysdes->keysdes_2->de,
                    (string)$mas_source->keysdes->keysdes_3->de,
                    (string)$mas_source->keysdes->keysdes_4->de,
                    (string)$mas_source->keysdes->keysdes_5->de,
                    (string)$mas_source->keysdes->keysdes_6->de,
                    (string)$mas_source->keysdes->keysdes_7->de,
                    (string)$mas_source->keysdes->keysdes_8->de,
                    (string)$mas_source->keysdes->keysdes_9->de,
                    (string)$mas_source->keysdes->keysdes_10->de,
                    (string)$mas_source->keysdes->keysdes_11->de,
                    (string)$mas_source->keysdes->keysdes_12->de,
                    (string)$mas_source->keysdes->keysdes_13->de,
                    (string)$mas_source->keysdes->keysdes_14->de,
                    (string)$mas_source->keysdes->keysdes_15->de,
                    (string)$mas_source->keysdes->keysdes_16->de,
                    (string)$mas_source->keysdes->keysdes_17->de,
                    (string)$mas_source->keysdes->keysdes_X->de);
                $lang['row'] = (string)$mas_source->row->de;
                $lang['column'] = (string)$mas_source->column->de;
                $lang['suredeleterow']=(string)$mas_source->suredeleterow->de;
                $lang['suredeletecolumn']=(string)$mas_source->suredeletecolumn->de;
                $lang['writesupport']=(string)$mas_source->writesupport->de;
                $lang['faq'] = (string)$mas_source->faq->de;
                $lang['step'] = (string)$mas_source->step->de;
                $lang['step1'] = (string)$mas_source->step1->de;
                $lang['step2'] = (string)$mas_source->step2->de;
                $lang['step3'] = (string)$mas_source->step3->de;
                $lang['step4'] = (string)$mas_source->step4->de;
                $lang['step5'] = (string)$mas_source->step5->de;
                $lang['step6'] = (string)$mas_source->step6->de;
                $lang['step7'] = (string)$mas_source->step7->de;
                $lang['change_row'] = (string)$mas_source->change_row->de;
                $lang['change_column'] = (string)$mas_source->change_column->de;
                $lang['wait_to_download'] = (string)$mas_source->wait_to_download->de;
                $lang['reset_canvas'] = (string)$mas_source->reset_canvas->de;
                $lang['delete_block'] = (string)$mas_source->delete_block->de;
                $lang['add'] = (string)$mas_source->add->de;
                $lang['remove'] = (string)$mas_source->remove->de;
                
            } else if (0 === strcmp($_SESSION['chart_generator_szawl_visitor']['lang'], 'es')) {
                
                $lang['title'] = (string)$mas_source->title->es;
                $lang['keywords'] = (string)$mas_source->keywords->es;
                $lang['author'] = (string)$mas_source->author->es;
                $lang['description'] = (string)$mas_source->description->es;
                $lang['writer'] = (string)$mas_source->writer->es;
                $lang['contacts'] = (string)$mas_source->contacts->es;
                $lang['url'] = (string)$mas_source->url->es;
                $lang['lang'] = (string)$mas_source->lang->es;
                $lang['draw'] = (string)$mas_source->draw->es;
                $lang['clear'] = (string)$mas_source->clear->es;
                $lang['clearsure'] = (string)$mas_source->clearsure->es;
                $lang['all'] = (string)$mas_source->all->es;
                $lang['odd'] = (string)$mas_source->odd->es;
                $lang['even'] = (string)$mas_source->even->es;
                $lang['keys'] = (string)$mas_source->keys->es;
                $lang['chart_name'] = (string)$mas_source->chart_name->es;
                $lang['export_txt'] = (string)$mas_source->export_txt->es;
                $lang['export_image'] = (string)$mas_source->export_image->es;
                $lang['export_pdf'] = (string)$mas_source->export_pdf->es;
                $lang['export_json'] = (string)$mas_source->export_json->es;
                $lang['tools'] = (string)$mas_source->tools->es;
                $lang['error1'] = (string)$mas_source->error1->es;
                $lang['error2'] = (string)$mas_source->error2->es;
                $lang['error3'] = (string)$mas_source->error3->es;
                $lang['error4'] = (string)$mas_source->error4->es;
                $lang['weekdays'] = $mas_source->weekdays->es;
                $lang['months'] = array(
                    (string)$mas_source->mounths->mounth_1->es,
                    (string)$mas_source->mounths->mounth_2->es,
                    (string)$mas_source->mounths->mounth_3->es,
                    (string)$mas_source->mounths->mounth_4->es,
                    (string)$mas_source->mounths->mounth_5->es,
                    (string)$mas_source->mounths->mounth_6->es,
                    (string)$mas_source->mounths->mounth_7->es,
                    (string)$mas_source->mounths->mounth_8->es,
                    (string)$mas_source->mounths->mounth_9->es,
                    (string)$mas_source->mounths->mounth_10->es,
                    (string)$mas_source->mounths->mounth_11->es,
                    (string)$mas_source->mounths->mounth_12->es);
                $lang['readlast']=10;
                $lang['copyrights'] = ((string)$mas_source->copyrights->es).date('Y');
                $lang['nomessages'] = (string)$mas_source->nomessages->es;
                $lang['share'] = (string)$mas_source->share->es;
                $lang['readpost'] = (string)$mas_source->readpost->es;
                $lang['commentpost'] = (string)$mas_source->commentpost->es;
                $lang['tagspost'] = (string)$mas_source->tagspost->es;
                $lang['back'] = (string)$mas_source->back->es;
                $lang['nohaking'] = (string)$mas_source->nohaking->es;
                $lang['pdfgenerated']=(string)$mas_source->pdfgenerated->es;
                $lang['langbar'] = $mas_source->langbar;
                $lang['menu_shawl'] = (string)$mas_source->menu_shawl->es;
                $lang['menu_knit_doily'] = (string)$mas_source->menu_knit_doily->es;
                $lang['menu_tablecloths'] = (string)$mas_source->menu_tablecloths->es;
                $lang['menu_baktus_scarf'] = (string)$mas_source->menu_baktus_scarf->es;
                $lang['menu_pullover'] = (string)$mas_source->menu_pullover->es;
                $lang['welcome'] = (string)$mas_source->welcome->es;
                $lang['helpshow'] = (string)$mas_source->helpshow->es;
                $lang['and_save'] = (string)$mas_source->and_save->es;
                $lang['move'] = (string)$mas_source->move->es;
                $lang['helpwrite'] = array(
                    $mas_source->helpwrite->helwrite_1->es,
                    $mas_source->helpwrite->helwrite_2->es,
                    $mas_source->helpwrite->helwrite_3->es,
                    $mas_source->helpwrite->helwrite_4->es);
                $lang['helphide'] = (string)$mas_source->helphide->es;
                $lang['selecthide']=(string)$mas_source->selecthide->es;
                $lang['keysdes'] = array(
                    (string)$mas_source->keysdes->keysdes_0->es,
                    (string)$mas_source->keysdes->keysdes_1->es,
                    (string)$mas_source->keysdes->keysdes_2->es,
                    (string)$mas_source->keysdes->keysdes_3->es,
                    (string)$mas_source->keysdes->keysdes_4->es,
                    (string)$mas_source->keysdes->keysdes_5->es,
                    (string)$mas_source->keysdes->keysdes_6->es,
                    (string)$mas_source->keysdes->keysdes_7->es,
                    (string)$mas_source->keysdes->keysdes_8->es,
                    (string)$mas_source->keysdes->keysdes_9->es,
                    (string)$mas_source->keysdes->keysdes_10->es,
                    (string)$mas_source->keysdes->keysdes_11->es,
                    (string)$mas_source->keysdes->keysdes_12->es,
                    (string)$mas_source->keysdes->keysdes_13->es,
                    (string)$mas_source->keysdes->keysdes_14->es,
                    (string)$mas_source->keysdes->keysdes_15->es,
                    (string)$mas_source->keysdes->keysdes_16->es,
                    (string)$mas_source->keysdes->keysdes_17->es,
                    (string)$mas_source->keysdes->keysdes_X->es);
                $lang['row'] = (string)$mas_source->row->es;
                $lang['column'] = (string)$mas_source->column->es;
                $lang['suredeleterow']=(string)$mas_source->suredeleterow->es;
                $lang['suredeletecolumn']=(string)$mas_source->suredeletecolumn->es;
                $lang['writesupport']=(string)$mas_source->writesupport->es;
                $lang['faq'] = (string)$mas_source->faq->es;
                $lang['step'] = (string)$mas_source->step->es;
                $lang['step1'] = (string)$mas_source->step1->es;
                $lang['step2'] = (string)$mas_source->step2->es;
                $lang['step3'] = (string)$mas_source->step3->es;
                $lang['step4'] = (string)$mas_source->step4->es;
                $lang['step5'] = (string)$mas_source->step5->es;
                $lang['step6'] = (string)$mas_source->step6->es;
                $lang['step7'] = (string)$mas_source->step7->es;
                $lang['change_row'] = (string)$mas_source->change_row->es;
                $lang['change_column'] = (string)$mas_source->change_column->es;
                $lang['wait_to_download'] = (string)$mas_source->wait_to_download->es;
                $lang['reset_canvas'] = (string)$mas_source->reset_canvas->es;
                $lang['delete_block'] = (string)$mas_source->delete_block->es;
                $lang['add'] = (string)$mas_source->add->es;
                $lang['remove'] = (string)$mas_source->remove->es;
                
            } else if (0 === strcmp($_SESSION['chart_generator_szawl_visitor']['lang'], 'it')) {
                
                $lang['title'] = (string)$mas_source->title->it;
                $lang['keywords'] = (string)$mas_source->keywords->it;
                $lang['author'] = (string)$mas_source->author->it;
                $lang['description'] = (string)$mas_source->description->it;
                $lang['writer'] = (string)$mas_source->writer->it;
                $lang['contacts'] = (string)$mas_source->contacts->it;
                $lang['url'] = (string)$mas_source->url->it;
                $lang['lang'] = (string)$mas_source->lang->it;
                $lang['draw'] = (string)$mas_source->draw->it;
                $lang['clear'] = (string)$mas_source->clear->it;
                $lang['clearsure'] = (string)$mas_source->clearsure->it;
                $lang['all'] = (string)$mas_source->all->it;
                $lang['odd'] = (string)$mas_source->odd->it;
                $lang['even'] = (string)$mas_source->even->it;
                $lang['keys'] = (string)$mas_source->keys->it;
                $lang['chart_name'] = (string)$mas_source->chart_name->it;
                $lang['export_txt'] = (string)$mas_source->export_txt->it;
                $lang['export_image'] = (string)$mas_source->export_image->it;
                $lang['export_pdf'] = (string)$mas_source->export_pdf->it;
                $lang['export_json'] = (string)$mas_source->export_json->it;
                $lang['tools'] = (string)$mas_source->tools->it;
                $lang['error1'] = (string)$mas_source->error1->it;
                $lang['error2'] = (string)$mas_source->error2->it;
                $lang['error3'] = (string)$mas_source->error3->it;
                $lang['error4'] = (string)$mas_source->error4->it;
                $lang['weekdays'] = $mas_source->weekdays->it;
                $lang['months'] = array(
                    (string)$mas_source->mounths->mounth_1->it,
                    (string)$mas_source->mounths->mounth_2->it,
                    (string)$mas_source->mounths->mounth_3->it,
                    (string)$mas_source->mounths->mounth_4->it,
                    (string)$mas_source->mounths->mounth_5->it,
                    (string)$mas_source->mounths->mounth_6->it,
                    (string)$mas_source->mounths->mounth_7->it,
                    (string)$mas_source->mounths->mounth_8->it,
                    (string)$mas_source->mounths->mounth_9->it,
                    (string)$mas_source->mounths->mounth_10->it,
                    (string)$mas_source->mounths->mounth_11->it,
                    (string)$mas_source->mounths->mounth_12->it);
                $lang['readlast']=10;
                $lang['copyrights'] = ((string)$mas_source->copyrights->it).date('Y');
                $lang['nomessages'] = (string)$mas_source->nomessages->it;
                $lang['share'] = (string)$mas_source->share->it;
                $lang['readpost'] = (string)$mas_source->readpost->it;
                $lang['commentpost'] = (string)$mas_source->commentpost->it;
                $lang['tagspost'] = (string)$mas_source->tagspost->it;
                $lang['back'] = (string)$mas_source->back->it;
                $lang['nohaking'] = (string)$mas_source->nohaking->it;
                $lang['pdfgenerated']=(string)$mas_source->pdfgenerated->it;
                $lang['langbar'] = $mas_source->langbar;
                $lang['menu_shawl'] = (string)$mas_source->menu_shawl->it;
                $lang['menu_knit_doily'] = (string)$mas_source->menu_knit_doily->it;
                $lang['menu_tablecloths'] = (string)$mas_source->menu_tablecloths->it;
                $lang['menu_baktus_scarf'] = (string)$mas_source->menu_baktus_scarf->it;
                $lang['menu_pullover'] = (string)$mas_source->menu_pullover->it;
                $lang['welcome'] = (string)$mas_source->welcome->it;
                $lang['helpshow'] = (string)$mas_source->helpshow->it;
                $lang['and_save'] = (string)$mas_source->and_save->it;
                $lang['move'] = (string)$mas_source->move->it;
                $lang['helpwrite'] = array(
                    $mas_source->helpwrite->helwrite_1->it,
                    $mas_source->helpwrite->helwrite_2->it,
                    $mas_source->helpwrite->helwrite_3->it,
                    $mas_source->helpwrite->helwrite_4->it);
                $lang['helphide'] = (string)$mas_source->helphide->it;
                $lang['selecthide']=(string)$mas_source->selecthide->it;
                $lang['keysdes'] = array(
                    (string)$mas_source->keysdes->keysdes_0->it,
                    (string)$mas_source->keysdes->keysdes_1->it,
                    (string)$mas_source->keysdes->keysdes_2->it,
                    (string)$mas_source->keysdes->keysdes_3->it,
                    (string)$mas_source->keysdes->keysdes_4->it,
                    (string)$mas_source->keysdes->keysdes_5->it,
                    (string)$mas_source->keysdes->keysdes_6->it,
                    (string)$mas_source->keysdes->keysdes_7->it,
                    (string)$mas_source->keysdes->keysdes_8->it,
                    (string)$mas_source->keysdes->keysdes_9->it,
                    (string)$mas_source->keysdes->keysdes_10->it,
                    (string)$mas_source->keysdes->keysdes_11->it,
                    (string)$mas_source->keysdes->keysdes_12->it,
                    (string)$mas_source->keysdes->keysdes_13->it,
                    (string)$mas_source->keysdes->keysdes_14->it,
                    (string)$mas_source->keysdes->keysdes_15->it,
                    (string)$mas_source->keysdes->keysdes_16->it,
                    (string)$mas_source->keysdes->keysdes_17->it,
                    (string)$mas_source->keysdes->keysdes_X->it);
                $lang['row'] = (string)$mas_source->row->it;
                $lang['column'] = (string)$mas_source->column->it;
                $lang['suredeleterow']=(string)$mas_source->suredeleterow->it;
                $lang['suredeletecolumn']=(string)$mas_source->suredeletecolumn->it;
                $lang['writesupport']=(string)$mas_source->writesupport->it;
                $lang['faq'] = (string)$mas_source->faq->it;
                $lang['step'] = (string)$mas_source->step->it;
                $lang['step1'] = (string)$mas_source->step1->it;
                $lang['step2'] = (string)$mas_source->step2->it;
                $lang['step3'] = (string)$mas_source->step3->it;
                $lang['step4'] = (string)$mas_source->step4->it;
                $lang['step5'] = (string)$mas_source->step5->it;
                $lang['step6'] = (string)$mas_source->step6->it;
                $lang['step7'] = (string)$mas_source->step7->it;
                $lang['change_row'] = (string)$mas_source->change_row->it;
                $lang['change_column'] = (string)$mas_source->change_column->it;
                $lang['wait_to_download'] = (string)$mas_source->wait_to_download->it;
                $lang['reset_canvas'] = (string)$mas_source->reset_canvas->it;
                $lang['delete_block'] = (string)$mas_source->delete_block->it;
                $lang['add'] = (string)$mas_source->add->it;
                $lang['remove'] = (string)$mas_source->remove->it;
                
            } else if (0 === strcmp($_SESSION['chart_generator_szawl_visitor']['lang'], 'fi')) {
                
                $lang['title'] = (string)$mas_source->title->fi;
                $lang['keywords'] = (string)$mas_source->keywords->fi;
                $lang['author'] = (string)$mas_source->author->fi;
                $lang['description'] = (string)$mas_source->description->fi;
                $lang['writer'] = (string)$mas_source->writer->fi;
                $lang['contacts'] = (string)$mas_source->contacts->fi;
                $lang['url'] = (string)$mas_source->url->fi;
                $lang['lang'] = (string)$mas_source->lang->fi;
                $lang['draw'] = (string)$mas_source->draw->fi;
                $lang['clear'] = (string)$mas_source->clear->fi;
                $lang['clearsure'] = (string)$mas_source->clearsure->fi;
                $lang['all'] = (string)$mas_source->all->fi;
                $lang['odd'] = (string)$mas_source->odd->fi;
                $lang['even'] = (string)$mas_source->even->fi;
                $lang['keys'] = (string)$mas_source->keys->fi;
                $lang['chart_name'] = (string)$mas_source->chart_name->fi;
                $lang['export_txt'] = (string)$mas_source->export_txt->fi;
                $lang['export_image'] = (string)$mas_source->export_image->fi;
                $lang['export_pdf'] = (string)$mas_source->export_pdf->fi;
                $lang['export_json'] = (string)$mas_source->export_json->fi;
                $lang['tools'] = (string)$mas_source->tools->fi;
                $lang['error1'] = (string)$mas_source->error1->fi;
                $lang['error2'] = (string)$mas_source->error2->fi;
                $lang['error3'] = (string)$mas_source->error3->fi;
                $lang['error4'] = (string)$mas_source->error4->fi;
                $lang['weekdays'] = $mas_source->weekdays->fi;
                $lang['months'] = array(
                    (string)$mas_source->mounths->mounth_1->fi,
                    (string)$mas_source->mounths->mounth_2->fi,
                    (string)$mas_source->mounths->mounth_3->fi,
                    (string)$mas_source->mounths->mounth_4->fi,
                    (string)$mas_source->mounths->mounth_5->fi,
                    (string)$mas_source->mounths->mounth_6->fi,
                    (string)$mas_source->mounths->mounth_7->fi,
                    (string)$mas_source->mounths->mounth_8->fi,
                    (string)$mas_source->mounths->mounth_9->fi,
                    (string)$mas_source->mounths->mounth_10->fi,
                    (string)$mas_source->mounths->mounth_11->fi,
                    (string)$mas_source->mounths->mounth_12->fi);
                $lang['readlast']=10;
                $lang['copyrights'] = ((string)$mas_source->copyrights->fi).date('Y');
                $lang['nomessages'] = (string)$mas_source->nomessages->fi;
                $lang['share'] = (string)$mas_source->share->fi;
                $lang['readpost'] = (string)$mas_source->readpost->fi;
                $lang['commentpost'] = (string)$mas_source->commentpost->fi;
                $lang['tagspost'] = (string)$mas_source->tagspost->fi;
                $lang['back'] = (string)$mas_source->back->fi;
                $lang['nohaking'] = (string)$mas_source->nohaking->fi;
                $lang['pdfgenerated']=(string)$mas_source->pdfgenerated->fi;
                $lang['langbar'] = $mas_source->langbar;
                $lang['menu_shawl'] = (string)$mas_source->menu_shawl->fi;
                $lang['menu_knit_doily'] = (string)$mas_source->menu_knit_doily->fi;
                $lang['menu_tablecloths'] = (string)$mas_source->menu_tablecloths->fi;
                $lang['menu_baktus_scarf'] = (string)$mas_source->menu_baktus_scarf->fi;
                $lang['menu_pullover'] = (string)$mas_source->menu_pullover->fi;
                $lang['welcome'] = (string)$mas_source->welcome->fi;
                $lang['helpshow'] = (string)$mas_source->helpshow->fi;
                $lang['and_save'] = (string)$mas_source->and_save->fi;
                $lang['move'] = (string)$mas_source->move->fi;
                $lang['helpwrite'] = array(
                    $mas_source->helpwrite->helwrite_1->fi,
                    $mas_source->helpwrite->helwrite_2->fi,
                    $mas_source->helpwrite->helwrite_3->fi,
                    $mas_source->helpwrite->helwrite_4->fi);
                $lang['helphide'] = (string)$mas_source->helphide->fi;
                $lang['selecthide']=(string)$mas_source->selecthide->fi;
                $lang['keysdes'] = array(
                    (string)$mas_source->keysdes->keysdes_0->fi,
                    (string)$mas_source->keysdes->keysdes_1->fi,
                    (string)$mas_source->keysdes->keysdes_2->fi,
                    (string)$mas_source->keysdes->keysdes_3->fi,
                    (string)$mas_source->keysdes->keysdes_4->fi,
                    (string)$mas_source->keysdes->keysdes_5->fi,
                    (string)$mas_source->keysdes->keysdes_6->fi,
                    (string)$mas_source->keysdes->keysdes_7->fi,
                    (string)$mas_source->keysdes->keysdes_8->fi,
                    (string)$mas_source->keysdes->keysdes_9->fi,
                    (string)$mas_source->keysdes->keysdes_10->fi,
                    (string)$mas_source->keysdes->keysdes_11->fi,
                    (string)$mas_source->keysdes->keysdes_12->fi,
                    (string)$mas_source->keysdes->keysdes_13->fi,
                    (string)$mas_source->keysdes->keysdes_14->fi,
                    (string)$mas_source->keysdes->keysdes_15->fi,
                    (string)$mas_source->keysdes->keysdes_16->fi,
                    (string)$mas_source->keysdes->keysdes_17->fi,
                    (string)$mas_source->keysdes->keysdes_X->fi);
                $lang['row'] = (string)$mas_source->row->fi;
                $lang['column'] = (string)$mas_source->column->fi;
                $lang['suredeleterow']=(string)$mas_source->suredeleterow->fi;
                $lang['suredeletecolumn']=(string)$mas_source->suredeletecolumn->fi;
                $lang['writesupport']=(string)$mas_source->writesupport->fi;
                $lang['faq'] = (string)$mas_source->faq->fi;
                $lang['step'] = (string)$mas_source->step->fi;
                $lang['step1'] = (string)$mas_source->step1->fi;
                $lang['step2'] = (string)$mas_source->step2->fi;
                $lang['step3'] = (string)$mas_source->step3->fi;
                $lang['step4'] = (string)$mas_source->step4->fi;
                $lang['step5'] = (string)$mas_source->step5->fi;
                $lang['step6'] = (string)$mas_source->step6->fi;
                $lang['step7'] = (string)$mas_source->step7->fi;
                $lang['change_row'] = (string)$mas_source->change_row->fi;
                $lang['change_column'] = (string)$mas_source->change_column->fi;
                $lang['wait_to_download'] = (string)$mas_source->wait_to_download->fi;
                $lang['reset_canvas'] = (string)$mas_source->reset_canvas->fi;
                $lang['delete_block'] = (string)$mas_source->delete_block->fi;
                $lang['add'] = (string)$mas_source->add->fi;
                $lang['remove'] = (string)$mas_source->remove->fi;
                
            } else if (0 === strcmp($_SESSION['chart_generator_szawl_visitor']['lang'], 'uk')) {
                $lang['title'] = (string)$mas_source->title->uk;
                $lang['keywords'] = (string)$mas_source->keywords->uk;
                $lang['author'] = (string)$mas_source->author->uk;
                $lang['description'] = (string)$mas_source->description->uk;
                $lang['writer'] = (string)$mas_source->writer->uk;
                $lang['contacts'] = (string)$mas_source->contacts->uk;
                $lang['url'] = (string)$mas_source->url->uk;
                $lang['lang'] = (string)$mas_source->lang->uk;
                $lang['draw'] = (string)$mas_source->draw->uk;
                $lang['clear'] = (string)$mas_source->clear->uk;
                $lang['clearsure'] = (string)$mas_source->clearsure->uk;
                $lang['all'] = (string)$mas_source->all->uk;
                $lang['odd'] = (string)$mas_source->odd->uk;
                $lang['even'] = (string)$mas_source->even->uk;
                $lang['keys'] = (string)$mas_source->keys->uk;
                $lang['chart_name'] = (string)$mas_source->chart_name->uk;
                $lang['export_txt'] = (string)$mas_source->export_txt->uk;
                $lang['export_image'] = (string)$mas_source->export_image->uk;
                $lang['export_pdf'] = (string)$mas_source->export_pdf->uk;
                $lang['export_json'] = (string)$mas_source->export_json->uk;
                $lang['tools'] = (string)$mas_source->tools->uk;
                $lang['error1'] = (string)$mas_source->error1->uk;
                $lang['error2'] = (string)$mas_source->error2->uk;
                $lang['error3'] = (string)$mas_source->error3->uk;
                $lang['error4'] = (string)$mas_source->error4->uk;
                $lang['weekdays'] = $mas_source->weekdays->uk;
                $lang['months'] = array(
                    (string)$mas_source->mounths->mounth_1->uk,
                    (string)$mas_source->mounths->mounth_2->uk,
                    (string)$mas_source->mounths->mounth_3->uk,
                    (string)$mas_source->mounths->mounth_4->uk,
                    (string)$mas_source->mounths->mounth_5->uk,
                    (string)$mas_source->mounths->mounth_6->uk,
                    (string)$mas_source->mounths->mounth_7->uk,
                    (string)$mas_source->mounths->mounth_8->uk,
                    (string)$mas_source->mounths->mounth_9->uk,
                    (string)$mas_source->mounths->mounth_10->uk,
                    (string)$mas_source->mounths->mounth_11->uk,
                    (string)$mas_source->mounths->mounth_12->uk);
                $lang['readlast']=10;
                $lang['copyrights'] = ((string)$mas_source->copyrights->uk).date('Y');
                $lang['nomessages'] = (string)$mas_source->nomessages->uk;
                $lang['share'] = (string)$mas_source->share->uk;
                $lang['readpost'] = (string)$mas_source->readpost->uk;
                $lang['commentpost'] = (string)$mas_source->commentpost->uk;
                $lang['tagspost'] = (string)$mas_source->tagspost->uk;
                $lang['back'] = (string)$mas_source->back->uk;
                $lang['nohaking'] = (string)$mas_source->nohaking->uk;
                $lang['pdfgenerated']=(string)$mas_source->pdfgenerated->uk;
                $lang['langbar'] = $mas_source->langbar;
                $lang['menu_shawl'] = (string)$mas_source->menu_shawl->uk;
                $lang['menu_knit_doily'] = (string)$mas_source->menu_knit_doily->uk;
                $lang['menu_tablecloths'] = (string)$mas_source->menu_tablecloths->uk;
                $lang['menu_baktus_scarf'] = (string)$mas_source->menu_baktus_scarf->uk;
                $lang['menu_pullover'] = (string)$mas_source->menu_pullover->uk;
                $lang['welcome'] = (string)$mas_source->welcome->uk;
                $lang['helpshow'] = (string)$mas_source->helpshow->uk;
                $lang['and_save'] = (string)$mas_source->and_save->uk;
                $lang['move'] = (string)$mas_source->move->uk;
                $lang['helpwrite'] = array(
                    $mas_source->helpwrite->helwrite_1->uk,
                    $mas_source->helpwrite->helwrite_2->uk,
                    $mas_source->helpwrite->helwrite_3->uk,
                    $mas_source->helpwrite->helwrite_4->uk);
                $lang['helphide'] = (string)$mas_source->helphide->uk;
                $lang['selecthide']=(string)$mas_source->selecthide->uk;
                $lang['keysdes'] = array(
                    (string)$mas_source->keysdes->keysdes_0->uk,
                    (string)$mas_source->keysdes->keysdes_1->uk,
                    (string)$mas_source->keysdes->keysdes_2->uk,
                    (string)$mas_source->keysdes->keysdes_3->uk,
                    (string)$mas_source->keysdes->keysdes_4->uk,
                    (string)$mas_source->keysdes->keysdes_5->uk,
                    (string)$mas_source->keysdes->keysdes_6->uk,
                    (string)$mas_source->keysdes->keysdes_7->uk,
                    (string)$mas_source->keysdes->keysdes_8->uk,
                    (string)$mas_source->keysdes->keysdes_9->uk,
                    (string)$mas_source->keysdes->keysdes_10->uk,
                    (string)$mas_source->keysdes->keysdes_11->uk,
                    (string)$mas_source->keysdes->keysdes_12->uk,
                    (string)$mas_source->keysdes->keysdes_13->uk,
                    (string)$mas_source->keysdes->keysdes_14->uk,
                    (string)$mas_source->keysdes->keysdes_15->uk,
                    (string)$mas_source->keysdes->keysdes_16->uk,
                    (string)$mas_source->keysdes->keysdes_17->uk,
                    (string)$mas_source->keysdes->keysdes_X->uk);
                $lang['row'] = (string)$mas_source->row->uk;
                $lang['column'] = (string)$mas_source->column->uk;
                $lang['suredeleterow']=(string)$mas_source->suredeleterow->uk;
                $lang['suredeletecolumn']=(string)$mas_source->suredeletecolumn->uk;
                $lang['writesupport']=(string)$mas_source->writesupport->uk;
                $lang['faq'] = (string)$mas_source->faq->uk;
                $lang['step'] = (string)$mas_source->step->uk;
                $lang['step1'] = (string)$mas_source->step1->uk;
                $lang['step2'] = (string)$mas_source->step2->uk;
                $lang['step3'] = (string)$mas_source->step3->uk;
                $lang['step4'] = (string)$mas_source->step4->uk;
                $lang['step5'] = (string)$mas_source->step5->uk;
                $lang['step6'] = (string)$mas_source->step6->uk;
                $lang['step7'] = (string)$mas_source->step7->uk;
                $lang['change_row'] = (string)$mas_source->change_row->uk;
                $lang['change_column'] = (string)$mas_source->change_column->uk;
                $lang['wait_to_download'] = (string)$mas_source->wait_to_download->uk;
                $lang['reset_canvas'] = (string)$mas_source->reset_canvas->uk;
                $lang['delete_block'] = (string)$mas_source->delete_block->uk;
                $lang['add'] = (string)$mas_source->add->uk;
                $lang['remove'] = (string)$mas_source->remove->uk;
                
            } else {
                $lang['title']='Knitting chart generator version 2';
		$lang['keywords']='knitting chart,generator,chart,схемы,генератор схем,mezgimas';
      		$lang['author']='Vladimir Poplavskij';
      		$lang['writer']='Anna';
		$lang['contacts']='Contacts';
      		$lang['url']='http://szawl.eu/';
      		$lang['lang']='en';
      		$lang['draw']='Draw';
      		$lang['clear']='Clear';
                $lang['clearsure']='Are you sure to clear all data?';
      		$lang['all']='All';
      		$lang['odd']='Odd';
      		$lang['even']='Even';
      		$lang['keys']='Keys';
      		$lang['chart_name']='Chart title';
      		$lang['export_txt']='Export symbols to TXT file';
      		$lang['export_image']='Save chart as PNG image';
      		$lang['export_pdf']='Save as PDF file';
		$lang['tools']='Tools from';
      		$lang['error1']='Empty textarea!';
      		$lang['error2']='To much spaces in line!';
      		$lang['error3']='No this number in chart keys!';
      		$lang['error4']='No input chars in textarea!';
      		$lang['weekdays']=array('Mn','Tu','We','Th','Fr','Sa','Su');
      		$lang['mounths']=array('January','February','March','April','May','June','July','August','September','October','November','December');
      		$lang['readlast']=10;
      		$lang['description']='Knitting chart generator';
      		$lang['copyrights']='All rights reserved.'.date('Y');
      		$lang['nomessages']='No messages';
      		$lang['share']='Share';
		$lang['readpost']='read';
      		$lang['commentpost']='comments';
      		$lang['tagspost']='Tags';
      		$lang['back']='Backs';
      		$lang['nohaking']='No hacking attempt!';
                $lang['pdfgenerated']='pdf file generated';
		$lang['langbar']='<img src="images/gb.png" width="16" height="11" onclick="javascript:select_lang(\'en\');" class="lng" border="0"> <img src="images/lt.png" onclick="javascript:select_lang(\'lt\');" class="lng"  width="16" height="11" border="0"> <img src="images/ru.png" onclick="javascript:select_lang(\'ru\');" class="lng"  width="16" height="11" border="0"> <img src="images/pl.png" onclick="javascript:select_lang(\'pl\');" class="lng"  width="16" height="11" border="0"> <img src="images/de.png" onclick="javascript:select_lang(\'de\');" class="lng"  width="16" height="11" border="0"> <img src="images/fr.png" onclick="javascript:select_lang(\'fr\');" class="lng"  width="16" height="11" border="0"> <img src="images/it.png" onclick="javascript:select_lang(\'it\');" class="lng"  width="16" height="11" border="0"> <img src="images/es.png" onclick="javascript:select_lang(\'es\');" class="lng"  width="16" height="11" border="0">';
		$lang['menu']=array('szawl'=>'Shawl','salfetki'=>'Napkins','skaterti'=>'Tablecloths','bagtusy'=>'Baktus scarf','svitery'=>'Sweaters');
		$lang['welcome']='Welcome! ';
		$lang['helpshow']='See guidance on the use';
		$lang['and_save']='<span style="color:#8b0a50;font-weight:bold;">and save chart into PNG, TXT or PDF formats</span>';
		$lang['move']='Move';
		$lang['helpwrite']='<p>Set the number of rows and columns. You can add or delete later . Clicking on the plus - <img src="images/plus.png" width="10" height="10"  style="" alt=""> - in the upper right corner adds a column , minus - <img src="images/minus.gif" width="10" height="10"  style="" alt=""> -deletes a column . In the lower right corner of the press on the plus - <img src="images/plus.png" width="10" height="10"  style="" alt=""> - adds a string to minus - <img src="images/minus.gif" width="10" height="10"  style="" alt="">  - deletes the row . Data when you delete a row or column will be lost ! Also have to write a number of columns or rows in the input field . Upper limit the number of rows and columns - 30. </p><br/>
                    <p> To enter a new sign - click on the desired cell. It will turn red with a yellow border. Next click on the right way in the right table . Cell displays the selected key .</p><br/> 
                    <p>To remove incorrectly selected key double click on the cell , it must stand with the yellow border , and on a table with the key icon will be deleted. Click on the delete icon , cell cleared. To clear all the cells easier to use icon -<img src="images/document_empty.png" width="15" height="15"  style="" alt="">- cleaning the whole area.</p><br/>
                    <p>After adding at least one cell , above the table keys will appear to generate and download a document or image :<br/><img src="images/file_extension_png.png" width="15" height="15"  style="" alt=""> -  image format png, best suited for insertion sites and online diaries <br/><img src="images/file_extension_pdf.png" width="15" height="15"  style="" alt="">  - pdf document is convenient for storage on your computer and print <br/><img src="images/json.png" width="15" height="15"  style="" alt=""> - json document useful for storing information about the scheme drawn</p>';
		$lang['helphide']='Close guide';
                $lang['selecthide']='Close';
		$lang['keysdes']=array('no stitch','knit one stitch','purl one stitch','knit 2 stitches together','yarn over','with yarn in back','k2tog through back loop, or slip one, knit one','Knit into the back of the st','with yarn in front','knit  (3) stitches together','cast on','slip stitch','bind off','Purl  (2) stitches together','Purl  (2) stitches together','Purl  (3) stitches together','Knit (4) stitches together','Purl  (4) stitches together');
		$lang['row']='row';
                $lang['suredeleterow']='Are you sure delete row?';
		$lang['column']='column';
                $lang['suredeletecolumn']='Are you sure delete column?';
                $lang['writesupport']='Write if find bug or have any problem <img src="include/email.php" style="" alt="Email">';
		$lang['faq']='<a href="help/" target="_blank"><span>Visual help (open in the new Window)</span><img width="15" height="15" src="help/images/FAQ64.png" style="border:none;" class="lng"></a>';
		$lang['step']='Step';
		$lang['step1']='Symbols indicate the number, which stands near the symbol';
		$lang['step2']='Name of chart';
		$lang['step3']='Selectable, any scheme of the series will draw all the odd or even';
		$lang['step4']='Writing as knit - right to left. You can start writing at the top or the bottom right corner';
		$lang['step5']='Be sure to write the spaces between numbers.';
		$lang['step6']='Press the draw-image will be generated on the right side of your browser';
		$lang['step7']='Click Save - in the form of images or text file';
                $lang['change_row']='Change row';
                $lang['change_column']='Change column';
                $lang['wait_to_download'] = 'Wait to download file';
                $lang['reset_canvas'] = 'Reset canvas';
                $lang['delete_block'] = 'Remove the data of the selected block';
                $lang['add'] = 'add';
                $lang['remove'] = 'remove';
            }
        }

    }
   
} else {
    $lang['title']='Knitting chart generator version 2';
    $lang['keywords']='knitting chart,generator,chart,схемы,генератор схем,mezgimas';
    $lang['author']='Vladimir Poplavskij';
    $lang['writer']='Anna';
    $lang['contacts']='Contacts';
    $lang['url']='http://szawl.eu/';
    $lang['lang']='en';
    $lang['draw']='Draw';
    $lang['clear']='Clear';
    $lang['clearsure']='Are you sure to clear all data?';
    $lang['all']='All';
    $lang['odd']='Odd';
    $lang['even']='Even';
    $lang['keys']='Keys';
    $lang['chart_name']='Chart title';
    $lang['export_txt']='Export symbols to TXT file';
    $lang['export_image']='Save chart as PNG image';
    $lang['export_pdf']='Save as PDF file';
    $lang['tools']='Tools from';
    $lang['error1']='Empty textarea!';
    $lang['error2']='To much spaces in line!';
    $lang['error3']='No this number in chart keys!';
    $lang['error4']='No input chars in textarea!';
    $lang['weekdays']=array('Mn','Tu','We','Th','Fr','Sa','Su');
    $lang['mounths']=array('January','February','March','April','May','June','July','August','September','October','November','December');
    $lang['readlast']=10;
    $lang['description']='Knitting chart generator';
    $lang['copyrights']='All rights reserved.'.date('Y');
    $lang['nomessages']='No messages';
    $lang['share']='Share';
    $lang['readpost']='read';
    $lang['commentpost']='comments';
    $lang['tagspost']='Tags';
    $lang['back']='Backs';
    $lang['nohaking']='No hacking attempt!';
    $lang['pdfgenerated']='pdf file generated';
    $lang['langbar']='<img src="images/gb.png" width="16" height="11" onclick="javascript:select_lang(\'en\');" class="lng" border="0"> <img src="images/lt.png" onclick="javascript:select_lang(\'lt\');" class="lng"  width="16" height="11" border="0"> <img src="images/ru.png" onclick="javascript:select_lang(\'ru\');" class="lng"  width="16" height="11" border="0"> <img src="images/pl.png" onclick="javascript:select_lang(\'pl\');" class="lng"  width="16" height="11" border="0"> <img src="images/de.png" onclick="javascript:select_lang(\'de\');" class="lng"  width="16" height="11" border="0"> <img src="images/fr.png" onclick="javascript:select_lang(\'fr\');" class="lng"  width="16" height="11" border="0"> <img src="images/it.png" onclick="javascript:select_lang(\'it\');" class="lng"  width="16" height="11" border="0"> <img src="images/es.png" onclick="javascript:select_lang(\'es\');" class="lng"  width="16" height="11" border="0">';
    $lang['menu']=array('szawl'=>'Shawl','salfetki'=>'Napkins','skaterti'=>'Tablecloths','bagtusy'=>'Baktus scarf','svitery'=>'Sweaters');
    $lang['welcome']='Welcome! ';
    $lang['helpshow']='See guidance on the use';
    $lang['and_save']='<span style="color:#8b0a50;font-weight:bold;">and save chart into PNG, TXT or PDF formats</span>';
    $lang['move']='Move';
    $lang['helpwrite']='<p>Set the number of rows and columns. You can add or delete later . Clicking on the plus - <img src="images/plus.png" width="10" height="10"  style="" alt=""> - in the upper right corner adds a column , minus - <img src="images/minus.gif" width="10" height="10"  style="" alt=""> -deletes a column . In the lower right corner of the press on the plus - <img src="images/plus.png" width="10" height="10"  style="" alt=""> - adds a string to minus - <img src="images/minus.gif" width="10" height="10"  style="" alt="">  - deletes the row . Data when you delete a row or column will be lost ! Also have to write a number of columns or rows in the input field . Upper limit the number of rows and columns - 30. </p><br/>
        <p> To enter a new sign - click on the desired cell. It will turn red with a yellow border. Next click on the right way in the right table . Cell displays the selected key .</p><br/> 
        <p>To remove incorrectly selected key double click on the cell , it must stand with the yellow border , and on a table with the key icon will be deleted. Click on the delete icon , cell cleared. To clear all the cells easier to use icon -<img src="images/document_empty.png" width="15" height="15"  style="" alt="">- cleaning the whole area.</p><br/>
        <p>After adding at least one cell , above the table keys will appear to generate and download a document or image :<br/><img src="images/file_extension_png.png" width="15" height="15"  style="" alt=""> -  image format png, best suited for insertion sites and online diaries <br/><img src="images/file_extension_pdf.png" width="15" height="15"  style="" alt="">  - pdf document is convenient for storage on your computer and print <br/><img src="images/json.png" width="15" height="15"  style="" alt=""> - json document useful for storing information about the scheme drawn</p>';
    $lang['helphide']='Close guide';
    $lang['selecthide']='Close';
    $lang['keysdes']=array('no stitch','knit one stitch','purl one stitch','knit 2 stitches together','yarn over','with yarn in back','k2tog through back loop, or slip one, knit one','Knit into the back of the st','with yarn in front','knit  (3) stitches together','cast on','slip stitch','bind off','Purl  (2) stitches together','Purl  (2) stitches together','Purl  (3) stitches together','Knit (4) stitches together','Purl  (4) stitches together');
    $lang['row']='row';
    $lang['suredeleterow']='Are you sure delete row?';
    $lang['column']='column';
    $lang['suredeletecolumn']='Are you sure delete column?';
    $lang['writesupport']='Write if find bug or have any problem <img src="include/email.php" style="" alt="Email">';
    $lang['faq']='<a href="help/" target="_blank"><span>Visual help (open in the new Window)</span><img width="15" height="15" src="help/images/FAQ64.png" style="border:none;" class="lng"></a>';
    $lang['step']='Step';
    $lang['step1']='Symbols indicate the number, which stands near the symbol';
    $lang['step2']='Name of chart';
    $lang['step3']='Selectable, any scheme of the series will draw all the odd or even';
    $lang['step4']='Writing as knit - right to left. You can start writing at the top or the bottom right corner';
    $lang['step5']='Be sure to write the spaces between numbers.';
    $lang['step6']='Press the draw-image will be generated on the right side of your browser';
    $lang['step7']='Click Save - in the form of images or text file';
    $lang['change_row']='Change row';
    $lang['change_column']='Change column';
    $lang['wait_to_download'] = 'Wait to download file';
    $lang['reset_canvas'] = 'Reset canvas';
    $lang['delete_block'] = 'Remove the data of the selected block';
    $lang['add'] = 'add';
    $lang['remove'] = 'remove';
}

  	
function get_client_language($availableLanguages, $default='en'){
        if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
                $langs=explode(',',$_SERVER['HTTP_ACCEPT_LANGUAGE']);

                foreach ($langs as $value){
                        $choice=substr($value,0,2);
                        if(in_array($choice, $availableLanguages)){
                                return $choice;
                        }
                }
        } 
        return $default;
}
