<?php
	session_start();
	if(!isset($_SESSION['duom']))die("Не удается создать новую картинку!");
	if($_SESSION['mas']&&$_SESSION['pat_name']&&$_SESSION['numbering']){
		include('download.php');
		require("fpdf/fpdf.php");
		class myPDF extends FPDF {
			public $title = "Szawl Chart Generator";
			//Page header method
			function Header() {
				$this->SetFont('Times','',12);
				$w = $this->GetStringWidth($this->title)+150;
				$this->SetDrawColor(0,0,180);
				$this->SetFillColor(170,169,148);
				$this->SetTextColor(0,0,255);
				$this->SetLineWidth(1);
				$this->Cell($w,9,$this->title,1,1,'C',1);	
				$this->Ln(10);
				$this->Image('../images/logo.jpg',10,10,10,8.5);
			}
			//Page footer method
			function Footer()     {
				//Position at 1.5 cm from bottom
				$this->SetY(-15);
				$this->SetFont('Arial','I',8);
				$this->Cell(0,10,' Page '.$this->PageNo().'/{nb}',0,0,'C');
			}
			function BuildTable($header,$data,$numbering) {
				//Colors, line width and bold font
				$this->SetFillColor(255,0,0);
				$this->SetTextColor(255);
				$this->SetDrawColor(128,0,0);
				$this->SetLineWidth(.2);
				//   $this->SetFont('Arial','','B');
				//Header
				// make an array for the column widths
				$w=85;
				$p=0;
				$max=0;
				$n=0;
				$h=count($data);
				// send the headers to the PDF document
				//Color and font restoration
				$this->SetFillColor(0);
				$this->SetTextColor(0);
				$this->SetFont('Arial','',20);
				//now spool out the data from the $data array
				$fill=false;  // used to alternate row color backgrounds
				$this->SetXY(20,35);
				for($i=0;$i<count($data);$i++){
					for($j=0;$j<count($data[$i]);$j++){
						//$this->Cell(10,10,$data[$i][$j],'C',0,'C',$fill);
						$this->Image('img/keys'.$data[$i][$j].'.jpeg',20+$n,35+$p,10,10);
						$n+=10;
					}
					
					$p+=10;
					$this->SetXY(20,35+$p);
					if($max<$n)$max=$n;
					$n=0;
				}
				$this->SetFont('Arial','',10);
				$this->SetXY(20,35+$p);
				for($i=(count($header)-1);$i>=0;$i--){
					$this->Cell(10,10,$header[$i],'C',0,'C',$fill);
					
				}
				$p=0;
				$this->SetXY(20+$max,35);
				if($numbering=='all'){
					for ($i = $h; $i >= 1; $i--){
						if($i%2==0){
							$this->Cell(10,10,$i,'C',0,'C',$fill);
							$p+=10;
							$this->SetXY(20+$max,35+$p);
						}else{
							$this->Cell(10,10,$i,'C',0,'C',$fill);
							$p+=10;
							$this->SetXY(10,35+$p);
						}
					}
					$this->SetXY(20+$max,35+$p);
				}elseif($numbering=='odd'){
					$str=$h*2-1;
					for ($i = $h; $i >= 1; $i--){
						$this->Cell(10,10,$str,'C',0,'C',$fill);
						
						$p+=10;
						$this->SetXY(20+$max,35+$p);
						$str-=2;
					}
				}elseif($numbering=='even'){
					$str=2*$h;
					$this->SetXY(10,35+$p);
					for ($i = $h; $i >= 1; $i--){
						$this->Cell(10,10,$str,'C',0,'C',$fill);
						
						$p+=10;
						$this->SetXY(10,35+$p);
						$str-=2;
					}
				}
				//foreach($data as $row=>$value)
				//{
					//$w=$i-($value*10);
					//$this->Cell($w,6,$row,'LR',0,'L',$fill);
					// set colors to show a URL style link
					//$this->SetTextColor(0,0,255);
					//$this->SetFont('', 'U');
					//$this->Cell($w[1],6,$row[1],'LR',0,'L',$fill, 'http://www.oreilly.com');
					// restore normal color settings
					//$this->SetTextColor(0);
					//$this->SetFont('');
					//$this->Cell($w[2],6,$row[2],'LR',0,'C',$fill);
					
					// flips from true to false and vise versa
					//$fill =! $fill;
				//}
				//$this->Cell(array_sum($w),0,'','T');
			}
		}
		if(isset($_SESSION['pat_name'])){$filename = $_SESSION['pat_name'];}else{$filename = "chart_".date('Y')."_".date('n')."_".date('j')."_".date('G')."_".date('i')."_".date('s');}
		//paint_file();
		//$imname=$_SESSION['pat_name'].'.png';
		$max2=0;
		for($i=0;$i<count($_SESSION['mas']);$i++){
			if($max2<count($_SESSION['mas'][$i]))$max2=count($_SESSION['mas'][$i]);
		}
		$w=$max2;
		$header=range(1,$w);
		$data=array();
		for($i=0;$i<count($_SESSION['mas']);$i++){
			$data[]=$_SESSION['mas'][$i];
		}
		$url = "http://www.szawl.eu" ;
		//$this->Image('favicon.jpg',10,10.5,15,8.5,"",$url);
		if($w>18){$pdf = new myPDF('L', 'mm', 'A4');}else{$pdf = new myPDF('P', 'mm', 'A4');}
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$pdf->SetFont('Times','',24);
		$pdf->Cell(0,0,$filename.' ',0,0,'L');
		$pdf->ln(225);
		$pdf->SetXY(20,20);
		$pdf->BuildTable($header,$data,$_SESSION['numbering']);
		//$pdf->Image($imname.' ',20,20,30,40);
		//$pdf->Cell(0,0,'text near page bottom',0,0,'C');
		//$pdf->Close();
		$pdf->Output();
	}else{
		die();
	}
?>
