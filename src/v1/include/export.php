<?php
	session_start();
	if(isset($_SESSION['duom'])){
		//$string = "bla bla bla";  // this can be a variable string or a row from a sql query or something else...
		if(isset($_SESSION['pat_name'])){$filename = $_SESSION['pat_name'];}else{$filename = "chart_".date('Y')."_".date('n')."_".date('j')."_".date('G')."_".date('i')."_".date('s');}
   	$ext = "txt";   // file extension
   	$mime_type = (strstr($_SERVER["HTTP_USER_AGENT"],"MSIE")==true || strstr($_SERVER["HTTP_USER_AGENT"],"Opera")==true)
   	? 'application/octetstream'
   	: 'application/octet-stream';
   	header('Content-Type: ' . $mime_type);
  		if (strstr($_SERVER["HTTP_USER_AGENT"],"MSIE")==false)
   	{
      	header('Content-Disposition: inline; filename="' . $filename . '.' . $ext . '"');
      	header("Content-Transfer-Encoding: binary");
      	header('Expires: 0');
     		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
      	header('Pragma: public');
      	print $_SESSION['duom'];
      	
  	 	} else {
      	header('Content-Disposition: attachment; filename="' . $filename . '.' . $ext . '"');
      	header("Content-Transfer-Encoding: binary");
      	header('Expires: 0');
      	header('Pragma: no-cache');
     		print $_SESSION['duom'];
   	}
   	exit();
   }
?>
