<?php
      //dla jezykow
      session_start();
      $_SESSION['agent']=$_SERVER["HTTP_USER_AGENT"];
      //require_once('include/ext.php');
      $browser = substr(strrchr($_SESSION['agent'], ")"), 1);
      $tok = explode(" ",$_SESSION['agent']);
      if(isset($_POST['lang_sel'])){$_SESSION['lang_id']=trim($_POST['lang_sel']);}
      if(!isset($_SESSION['lang_id']))$_SESSION['lang_id']='en';
  if(file_exists('../admin/lang.php')) include('../admin/lang.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
	<?php if(isset($lang['title'])){echo "<title>".$lang['title']."</title>\n";}else{echo "<title>Knitting chart generator</title> \n";}?>
	<META NAME = "AUTHOR" CONTENT = "Wlaimierz Poplavskij">
	<META NAME = "COPYRIGHT" CONTENT = "Wlaimierz Poplavskij">
	<?php if(isset($lang['description'])){echo "<META NAME = \"DESCRIPTION\" CONTENT = \"".$lang['description']."\">";}else{echo '<META NAME = "DESCRIPTION" CONTENT = "Knitting chart generator http://szawl.eu/chart/">';}?>
	<META NAME = "KEYWORDS" CONTENT = "knitting,chart">
	<META HTTP-EQUIV = "CONTENT-TYPE" CONTENT = "CHARSET=utf-8">
	<META HTTP-EQUIV = "CONTENT-LANGUAGE" CONTENT = "ru">
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
	<link rel="stylesheet" type="text/css" href="development-bundle/themes/smoothness/ui.all.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/tabsTheme.css">
	<?php
						if(isset($_SESSION['lang_id']))echo '<link rel="stylesheet" type="text/css" href="css/bg_'.$_SESSION['lang_id'].'.css">';
						echo "\n";

	?>
	<script type="text/javascript" src="js/std.js"></script>
<body>
<div id="up">
<h1><?php if(isset($lang['title'])){ echo $lang['title'].' HELP';} ?></h1>
	<div id="langbar">
	<?php if(isset($lang['langbar'])){ echo $lang['langbar'];} ?> <?php if(isset($lang['tools'])){ echo $lang['tools'].' <a href="http://szawl.eu" class="b">http://szawl.eu</a><br>';} ?>
</div>
</div>
<div id="mid">
      <div id="leftcontent">
		<div id="myTabs">
	  <ul>
  	    <li><a href="#a"><?php if(isset($lang['step'])){ echo $lang['step'];} ?> 1</a></li>
        <li><a href="#b"><?php if(isset($lang['step'])){ echo $lang['step'];} ?> 2</a></li>
        <li><a href="#c"><?php if(isset($lang['step'])){ echo $lang['step'];} ?> 3</a></li>
        <li><a href="#d"><?php if(isset($lang['step'])){ echo $lang['step'];} ?> 4</a></li>
        <li><a href="#e"><?php if(isset($lang['step'])){ echo $lang['step'];} ?> 5</a></li>
        <li><a href="#f"><?php if(isset($lang['step'])){ echo $lang['step'];} ?> 6</a></li>
	<li><a href="#g"><?php if(isset($lang['step'])){ echo $lang['step'];} ?> 7</a></li>
      </ul>
      <div id="a"><div id="pic"><div id="frame1"></div><div id="help1"><?php if(isset($lang['step1'])){ echo $lang['step1'];} ?></div></div></div>
      <div id="b"><div id="pic"><div id="frame2"></div><div id="help2"><?php if(isset($lang['step2'])){ echo $lang['step2'];} ?></div></div></div>
      <div id="c"><div id="pic"><div id="frame3"></div><div id="help3"><?php if(isset($lang['step3'])){ echo $lang['step3'];} ?></div></div></div>
      <div id="d"><div id="pic1a"><div id="frame4"></div><div id="help4"><?php if(isset($lang['step4'])){ echo $lang['step4'];} ?></div></div></div>
      <div id="e"><div id="pic1b"><div id="frame5"></div><div id="help5"><?php if(isset($lang['step5'])){ echo $lang['step5'];} ?></div></div></div>
      <div id="f"><div id="pic1b"><div id="frame6"></div><div id="help6"><?php if(isset($lang['step6'])){ echo $lang['step6'];} ?></div></div></div>
      <div id="g"><div id="pic2"><div id="frame7"></div><div id="help7"><?php if(isset($lang['step7'])){ echo $lang['step7'];} ?></div></div></div>
	</div>
    <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="development-bundle/ui/ui.core.js"></script>
    <script type="text/javascript" src="development-bundle/ui/ui.tabs.js"></script>
    <script type="text/javascript">
	  //define function to be executed on document ready
	  $(function(){
	    //create the tabs
	    $("#myTabs").tabs();
	  });
	</script>

		</div>
 </div>
 </body>
 </html>
