<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
error_reporting(E_ALL);
ini_set('display_errors', FALSE);
ini_set('display_startup_errors', FALSE);
ini_set('error_reporting', E_ALL & ~E_WARNING);
//$report_warn=error_reporting(E_ERROR | E_PARSE);

date_default_timezone_set('UTC');

    //dla jezykow
      session_start();
       $_SESSION['chart_generator_szawl_visitor']['agent']=$_SERVER["HTTP_USER_AGENT"];
      //require_once('include/ext.php');
      $browser = substr(strrchr($_SESSION['chart_generator_szawl_visitor']['agent'], ")"), 1);
      $tok = explode(" ",$_SESSION['chart_generator_szawl_visitor']['agent']);
      $_SESSION['chart_generator_szawl_visitor']['page'] = 'test';
      //require_once('include/ext.php');

      if(file_exists('admin/lang.php')) {
          $dir='';
          include_once('admin/lang.php');

      }
      if(file_exists('admin/funkc.php')) {
          include_once('admin/funkc.php');

      }

      if(isset($lang['all'])&&isset($lang['even'])&&isset($lang['odd'])){
          $radios='<div class="selected2">'.$lang['all'].' <input name="numbering" type="radio" id="numbering_all"  value="all" checked="checked"></div><div class="selected2"> '.$lang['odd'].' <input name="numbering" type="radio" id="numbering_odd" value="odd"></div><div class="selected2"> '.$lang['even'].' <input name="numbering" type="radio" id="numbering_even" value="even"></div>';
      }else{
          $radios='<div class="selected2">All <input name="numbering" type="radio" id="numbering_all"  value="all" checked="checked"></div><div class="selected2"> Odd <input name="numbering" type="radio" id="numbering_odd" value="odd"></div><div class="selected2"> Even <input name="numbering" type="radio" id="numbering_even" value="even"></div>';
      }
      $nameinput='Chart_1';
?>
<!doctype html>

<html lang="<?php if(isset($_SESSION['chart_generator_szawl_visitor']['lang'])){ echo $_SESSION['chart_generator_szawl_visitor']['lang'];} ?>">
<head>
  <meta charset="utf-8">
  <?php if(isset($lang['title'])){echo "<title>".$lang['title']."</title>\n";}else{echo "<title>Knitting chart generator szawl.eu</title> \n";}?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name = "author" content = "Wlodzimierz Poplavskij">

        <?php
            if ( isset($lang['description']) ) {
                echo '<meta name = "description" content = "'.$lang['description'].'">';
            } else {
                echo '<meta name = "description" content = "Knitting chart generator http://szawl.eu/chart/">';
            }
            if ( isset($lang['keywords']) ) {
                echo '<meta name = "keywords" content = "'.$lang['keywords'].'">';
            } else {
                echo '<meta name = "keywords" content = "knitting,chart">';
            }
        ?>
	<link rel="apple-touch-icon" href="apple-touch-icon.png">
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
        <link rel="stylesheet" href="css/bootstrap/bootstrap-responsive.css" type="text/css" media="screen" title="master" charset="utf-8">
        <link rel="stylesheet" href="css/bootstrap/bootstrap-theme.css" type="text/css" media="screen" title="master" charset="utf-8">
        <link rel="stylesheet" href="css/bootstrap/bootstrap.css" type="text/css" media="screen" title="master" charset="utf-8">
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" type="text/css" href="highslide/highslide.css" />
        <link rel="stylesheet" href="css/hint/hint.min.css">
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body>
    <?php if (0 === strcmp('index',$_SESSION['chart_generator_szawl_visitor']['page'])) { ?>
    <!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-MXMC4P"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MXMC4P');</script>
<!-- End Google Tag Manager -->
    <?php } ?>
<div class="container">
    <div class="row topline">
        <div class="col-sm-12"><h1 title="http://szawl.eu/chart/ &beta;"><?php if(isset($lang['title'])){ echo $lang['title'];} ?> </h1></div>
    </div>
    <div class="row menu_panel">
        <div class="col-sm-12">
            <div id="langbar">
            <?php
                if(isset($lang['langbar'])){
                    echo ShowLanguages($lang['langbar']).' ';
                }
                if(isset($lang['tools'])){
                    echo '<div class="tools"> '.$lang['tools'].' <a href="http://szawl.eu" class="b">http://szawl.eu</a>';
                    if(function_exists('GetIconsByKey')){
                        echo '<a href="help/" target="_blank"> <img src="'.GetIconsByKey('question').'" width="20" height="20" class="lng"></a>';
                    } else {
                        echo '<a href="help/" target="_blank"> <img src="images/questionmark.png" width="20" height="20" class="lng"></a>';
                    }
                    echo '</div> <div class="clearfix"></div>';}
            ?>
            </div>
        </div>
    </div>
    <div class="row plain">
        <div class="canvas col-sm-6">
            <div><?php if(isset($lang['welcome'])){ echo $lang['welcome'];} ?></div>
            <form name="main0" enctype="multipart/form-data" action="index.php" method="post" id="form0">
                <div id="top_form"><div class="chart_name"><?php echo $lang['chart_name'];?></div> <input type="text" size="25" name="named" value="<?php echo $nameinput;?>" class="named"><div class="clearfix"></div></div>
                <div id="middle_form">
                    <div id="scroll">
                        <div id="draggable" style="display:none;">
                            <div id="top_x_axis" class="clearfix"></div>
                            <div id="top_y_axis"></div>
                            <div id="area" class="area"></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <input type="hidden" id="inputkommand" name="inputkommand" value="">
                    <input type="hidden" id="selectedblock" name="selectedblock" value="null">
                    <input type="hidden" id="area_x" name="area_x" value="">
                    <input type="hidden" id="area_y" name="area_y" value="">
                    <div class="right_columns">
                        <div class="info small"><?php if(isset($lang['change_column'])){ echo $lang['change_column'];} ?></div>
                        <div class="clearfix"></div>
                        <div class="right_columns_plus" class="hint--top hint--rounded hint--info" data-hint="<?php if(isset($lang['column']) && isset($lang['add'])){ echo ''.$lang['add'].' '.$lang['column'];} ?>"><img src="<?php if(function_exists('GetIconsByKey')){ echo GetIconsByKey('plus');} else {echo 'images/plus.png';}?>" width="20" height="20"/></div>
                        <div class="right_columns_minus" class="hint--top hint--rounded hint--info" data-hint="<?php if(isset($lang['column']) && isset($lang['remove'])){ echo ''.$lang['remove'].' '.$lang['column'];} ?>"><img src="<?php if(function_exists('GetIconsByKey')){ echo GetIconsByKey('minus');} else {echo 'images/minus.gif';}?>" width="20" height="20"/></div>
                        <div class="clearfix"></div>
                        <input type="text" id="right_columns_value" name="right_columns_value" value="10">
                    </div>
                    <div class="bottom_rows">
                        <div class="info small"><?php if(isset($lang['change_row'])){ echo $lang['change_row'];} ?></div>
                        <div class="clearfix"></div>
                        <div class="bottom_rows_plus" class="hint--top hint--rounded hint--info" data-hint="<?php if(isset($lang['row']) && isset($lang['add'])){ echo $lang['add'].' '.$lang['row'];} ?>"><img src="<?php if(function_exists('GetIconsByKey')){ echo GetIconsByKey('plus');} else {echo 'images/plus.png';}?>" width="20" height="20"/></div>
                        <div class="bottom_rows_minus" class="hint--top hint--rounded hint--info" data-hint="<?php if(isset($lang['column']) && isset($lang['remove'])){ echo $lang['remove'].' '.$lang['row'];} ?>"><img src="<?php if(function_exists('GetIconsByKey')){ echo GetIconsByKey('minus');} else {echo 'images/minus.gif';}?>" width="20" height="20"/></div>
                        <div class="clearfix"></div>
                        <input type="text" id="bottom_rows_value" name="bottom_rows_value" value="10">
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div id="bottom_form"><?php echo $radios;?> <div class="clearfix"></div></div>
            </form>
            <input type="hidden" id="controller" value="null">
            <div id="sure_text_column"><?php if(isset($lang['suredeletecolumn'])){ echo $lang['suredeletecolumn'];} ?></div>
            <div id="sure_text_row"><?php if(isset($lang['suredeleterow'])){ echo $lang['suredeleterow'];} ?></div>
            <div id="sure_text_clear"><?php if(isset($lang['clearsure'])){ echo $lang['clearsure'];} ?></div>
            <div class="clearfix"></div>
        </div>
        <div class="textures col-sm-6">
            <div id="control_icons">

            <?php
            if(imageTypes() && IMG_PNG && isset($lang['helpshow'])&& isset($lang['helpwrite'])&& isset($lang['helphide'])) {
                echo '<div class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">';
                if(isset($lang['helpshow'])){ echo $lang['helpshow'];}else {echo 'Show help';}
                echo ' <span class="caret"></span></a>';
                echo '<ul class="dropdown-menu" role="menu">';
                if (is_array($lang['helpwrite'])) {
                    foreach ($lang['helpwrite'] as $key => $value) {
                        echo '<li><a href="#" onclick="return hs.htmlExpand(this, { contentId: \'highslide-html-'.md5($value[0]).'\',width: \'300\', headingEval: \'this.a.title\', wrapperClassName: \'titlebar\' } );" class="highslide">'.$value[0].'</a></li>
                      <li class="divider"></li>';
                    }
                } else {
                    echo '<li><a href="#" onclick="return hs.htmlExpand(this, { contentId: \'highslide-html\',width: \'300\', headingEval: \'this.a.title\', wrapperClassName: \'titlebar\' } );" class="highslide">'.$value[0].'';
                    ?><img src="<?php if(function_exists('GetIconsByKey')){ echo GetIconsByKey('info');} else {echo 'images/info_rhombus.png';}?>" width="32" height="32" id="showhelp" style="" alt="<?php if(isset($lang['helpshow'])){ echo $lang['helpshow'];} ?>" title="<?php if(isset($lang['helpshow'])){ echo $lang['helpshow'];} ?>">
                        <?php
                    echo '</a></li><li class="divider"></li>';
                }
                echo '</ul></div>';

                ?>
                    <div href="#" onclick="return hs.htmlExpand(this, { contentId: 'highslide-html',width: '300', headingEval: 'this.a.title',
			wrapperClassName: 'titlebar' } );" class="highslide hint--top hint--rounded hint--info" title="<?php if(isset($lang['helpshow'])){ echo $lang['helpshow'];} ?>" data-hint="<?php if(isset($lang['helpshow'])){ echo $lang['helpshow'];} ?>">

                    </div>
                <div class="highslide-html-content" id="highslide-html">
                        <div class="highslide-header">
                                <ul>
                                        <li class="highslide-move">
                                                <a href="#" onclick="return false"><?php if(isset($lang['move'])){ echo $lang['move'];} ?></a>
                                        </li>
                                        <li class="highslide-close">
                                                <a href="#" onclick="return hs.close(this);"><?php if(isset($lang['helphide'])){ echo $lang['helphide'];} ?></a>
                                        </li>
                                </ul>
                        </div>
                        <div class="highslide-body"><?php

                        if (!is_array($lang['helpwrite'])) {
                            echo ''.$lang['helpwrite'];
                        }

                        ?><br>
                            <div id="idhelp"><?php if(isset($lang['faq'])){ echo $lang['faq'];} ?></div>
                            </div>
                    <div class="highslide-footer">
                        <div>
                            <span class="highslide-resize" title="Resize">
                                <span></span>
                            </span>
                        </div>
                    </div>
                </div>
                    <?php
                 //echo "Картинки PNG отображаются <br />";
                if (is_array($lang['helpwrite'])) {
                    foreach ($lang['helpwrite'] as $key => $value) {
                        $value_show = $value[1];
                        $rep_images = $value_show;
                        if(function_exists('GetIconsByKey')){
                            $rep_images = str_replace('{{plus_image}}','<img src="'.GetIconsByKey('plus').'" width="10" height="10"/>',$rep_images);
                            $rep_images = str_replace('{{minus_image}}','<img src="'.GetIconsByKey('minus').'" width="10" height="10"/>',$rep_images);
                            $rep_images = str_replace('{{reset_image}}','<img src="'.GetIconsByKey('reset').'" width="15" height="15"/>',$rep_images);
                            $rep_images = str_replace('{{json_image}}','<img src="'.GetIconsByKey('json').'" width="15" height="15"/>',$rep_images);
                            $rep_images = str_replace('{{png_image}}','<img src="'.GetIconsByKey('png').'" width="15" height="15"/>',$rep_images);
                            $rep_images = str_replace('{{pdf_image}}','<img src="'.GetIconsByKey('pdf').'" width="15" height="15"/>',$rep_images);

                        } else {
                            $rep_images = str_replace('{{plus_image}}','<img src="images/plus.png" width="10" height="10"/>',$rep_images);
                            $rep_images = str_replace('{{minus_image}}','<img src="images/minus.gif" width="10" height="10"/>',$rep_images);
                            $rep_images = str_replace('{{reset_image}}','<img src="images/document_empty.png" width="15" height="15"/>',$rep_images);
                            $rep_images = str_replace('{{json_image}}','<img src="images/json.png" width="15" height="15"/>',$rep_images);
                            $rep_images = str_replace('{{png_image}}','<img src="images/file_extension_png.png" width="15" height="15"/>',$rep_images);
                            $rep_images = str_replace('{{pdf_image}}','<img src="images/file_extension_pdf.png" width="15" height="15"/>',$rep_images);

                        }
                        ?>
                            <div class="highslide-html-content" id="highslide-html<?php echo '-'.md5($value[0]); ?>">
                        <div class="highslide-header">
                                <ul>
                                        <li class="highslide-move">
                                                <a href="#" onclick="return false"><?php if(isset($lang['move'])){ echo $lang['move'];} ?></a>
                                        </li>
                                        <li class="highslide-close">
                                                <a href="#" onclick="return hs.close(this);"><?php if(isset($lang['helphide'])){ echo $lang['helphide'];} ?></a>
                                        </li>
                                </ul>
                        </div>
                        <div class="highslide-body"><?php

                        echo '<h4 class="list-group-item-heading">'.$value[0].'</h4>
        <p class="list-group-item-text">'.$rep_images.'</p>';

                        ?><br>
                            <div id="idhelp"><?php if(isset($lang['faq'])){ echo $lang['faq'];} ?></div>
                            </div>
                    <div class="highslide-footer">
                        <div>
                            <span class="highslide-resize" title="Resize">
                                <span></span>
                            </span>
                        </div>
                    </div>
                </div>
                        <?php
                    }
                }

                echo '';
            }
            ?>

                <div id="loader_block"><span data-hint="<?php if(isset($lang['wait_to_download'])){ echo $lang['wait_to_download'];} ?>" class="hint--top hint--rounded hint--info"><img src="<?php if(function_exists('GetIconsByKey')){ echo GetIconsByKey('download');} else {echo 'images/loader.gif';}?>" width="28" height="28" alt="<?php if(isset($lang['wait_to_download'])){ echo $lang['wait_to_download'];} ?>" title="<?php if(isset($lang['wait_to_download'])){ echo $lang['wait_to_download'];} ?>" ></span></div>
                <div data-hint="<?php if(isset($lang['reset_canvas'])){ echo $lang['reset_canvas'];} ?>" class="hint--top hint--rounded hint--info"><img src="<?php if(function_exists('GetIconsByKey')){ echo GetIconsByKey('reset');} else {echo 'images/document_empty.png';}?>" width="28" height="28" id="resetimage" style="" alt="<?php if(isset($lang['reset_canvas'])){ echo $lang['reset_canvas'];} ?> http://szawl.eu/chart/v2"></div>
                <div data-hint="<?php if(isset($lang['export_image'])){ echo $lang['export_image'];} ?>" class="hint--top hint--rounded hint--info"><img src="<?php if(function_exists('GetIconsByKey')){ echo GetIconsByKey('png');} else {echo 'images/file_extension_png.png';}?>" width="28" height="28" id="downloadimage" style="" alt="<?php if(isset($lang['export_image'])){ echo $lang['export_image'];} ?> http://szawl.eu/chart/v2"></div>
                <div data-hint="<?php if(isset($lang['export_pdf'])){ echo $lang['export_pdf'];} ?>" class="hint--top hint--rounded hint--info"><img src="<?php if(function_exists('GetIconsByKey')){ echo GetIconsByKey('pdf');} else {echo 'images/file_extension_pdf.png';}?>" width="28" height="28" id="createpdf" style="" alt="<?php if(isset($lang['export_pdf'])){ echo $lang['export_pdf'];} ?> http://szawl.eu/chart/v2"></div>
                <div data-hint="<?php if(isset($lang['delete_block'])){ echo $lang['delete_block'];} ?>" class="hint--top hint--rounded hint--info"><img src="<?php if(function_exists('GetIconsByKey')){ echo GetIconsByKey('delete_block');} else {echo 'images/delete.png';}?>" width="28" height="28" id="emptyblock" alt="<?php if(isset($lang['delete_block'])){ echo $lang['delete_block'];} ?> http://szawl.eu/chart/v2"></div>
                <div class="clearfix"></div>
          </div>
          <div class="keys">
              <div class="full"><?php if(isset($lang['keys'])){ echo $lang['keys'];} ?></div>
              <div class="boxer">
                <div class="box-row">
                    <div class="box append border" id="key0" title="<?php if(isset($lang['keysdes'][0])){ echo $lang['keysdes'][0];}  ?>">
                        <img src="<?php if(function_exists('GetImageByKey')){ echo GetImageByKey('key0');} else {echo 'images/keys1.png';}?>" alt="http://szawl.eu/chart/v2 Knitting chart generator by szawl.eu" class="k" title="http://szawl.eu/chart/v2 Knitting chart generator by szawl.eu">   -
                        <?php if(isset($lang['keysdes'][0])){ echo $lang['keysdes'][0];} ?>
                    </div>
                    <div class="box append border" id="key1" title="<?php if(isset($lang['keysdes'][1])){ echo $lang['keysdes'][1];}  ?>">
                        <img src="<?php if(function_exists('GetImageByKey')){ echo GetImageByKey('key1');} else {echo 'images/keys0.png';}?>" alt="http://szawl.eu/chart/v2 Knitting chart generator by szawl.eu" class="k" title="http://szawl.eu/chart/v2 Knitting chart generator by szawl.eu">   -
                        <?php if(isset($lang['keysdes'][1])){ echo $lang['keysdes'][1];} ?>
                    </div>
                    <div class="box append border" id="key2" title="<?php if(isset($lang['keysdes'][2])){ echo $lang['keysdes'][2];}  ?>">
                       <img src="<?php if(function_exists('GetImageByKey')){ echo GetImageByKey('key2');} else {echo 'images/keys2.png';}?>" alt="http://szawl.eu/chart/v2 Knitting chart generator by szawl.eu" class="k" title="http://szawl.eu/chart/v2 Knitting chart generator by szawl.eu">   -
                       <?php if(isset($lang['keysdes'][2])){ echo $lang['keysdes'][2];} ?>
                    </div>
                </div>
                <div class="box-row">
                    <div class="box append border" id="key3" title="<?php if(isset($lang['keysdes'][3])){ echo $lang['keysdes'][3];}  ?>">
                        <img src="<?php if(function_exists('GetImageByKey')){ echo GetImageByKey('key3');} else {echo 'images/keys3.png';}?>" alt="http://szawl.eu/chart/v2 Knitting chart generator by szawl.eu" class="k" title="http://szawl.eu/chart/v2 Knitting chart generator by szawl.eu">   -
                        <?php if(isset($lang['keysdes'][3])){ echo $lang['keysdes'][3];} ?>
                    </div>
                    <div class="box append border" id="key4" title="<?php if(isset($lang['keysdes'][4])){ echo $lang['keysdes'][4];}  ?>">
                        <img src="<?php if(function_exists('GetImageByKey')){ echo GetImageByKey('key4');} else {echo 'images/keys4.png';}?>" alt="http://szawl.eu/chart/v2 Knitting chart generator by szawl.eu" class="k" title="http://szawl.eu/chart/v2 Knitting chart generator by szawl.eu">   -
                        <?php if(isset($lang['keysdes'][4])){ echo $lang['keysdes'][4];} ?>
                    </div>
                    <div class="box append border" id="key5" title="<?php if(isset($lang['keysdes'][5])){ echo $lang['keysdes'][5];}  ?>">
                        <img src="<?php if(function_exists('GetImageByKey')){ echo GetImageByKey('key5');} else {echo 'images/keys5.png';}?>" alt="http://szawl.eu/chart/v2 Knitting chart generator by szawl.eu" class="k" title="http://szawl.eu/chart/v2 Knitting chart generator by szawl.eu">   -
                        <?php if(isset($lang['keysdes'][5])){ echo $lang['keysdes'][5];} ?>
                    </div>
                </div>
                <div class="box-row">
                    <div class="box append border" id="key6" title="<?php if(isset($lang['keysdes'][6])){ echo $lang['keysdes'][6];}  ?>">
                        <img src="<?php if(function_exists('GetImageByKey')){ echo GetImageByKey('key6');} else {echo 'images/keys6.png';}?>" alt="http://szawl.eu/chart/v2 Knitting chart generator by szawl.eu" class="k" title="http://szawl.eu/chart/v2 Knitting chart generator by szawl.eu">   -
                        <?php if(isset($lang['keysdes'][6])){ echo $lang['keysdes'][6];} ?>
                    </div>
                    <div class="box append border" id="key7" title="<?php if(isset($lang['keysdes'][7])){ echo $lang['keysdes'][7];}  ?>">
                        <img src="<?php if(function_exists('GetImageByKey')){ echo GetImageByKey('key7');} else {echo 'images/keys7.png';}?>" alt="http://szawl.eu/chart/v2 Knitting chart generator by szawl.eu" class="k" title="http://szawl.eu/chart/v2 Knitting chart generator by szawl.eu">   -
                        <?php if(isset($lang['keysdes'][7])){ echo $lang['keysdes'][7];} ?>
                    </div>
                    <div class="box append border" id="key8" title="<?php if(isset($lang['keysdes'][8])){ echo $lang['keysdes'][8];}  ?>">
                        <img src="<?php if(function_exists('GetImageByKey')){ echo GetImageByKey('key8');} else {echo 'images/keys8.png';}?>" alt="http://szawl.eu/chart/v2 Knitting chart generator by szawl.eu" class="k" title="http://szawl.eu/chart/v2 Knitting chart generator by szawl.eu">   -
                        <?php if(isset($lang['keysdes'][8])){ echo $lang['keysdes'][8];} ?>
                    </div>
                </div>
                <div class="box-row">
                    <div class="box append border" id="key9" title="<?php if(isset($lang['keysdes'][9])){ echo $lang['keysdes'][9];}  ?>">
                        <img src="<?php if(function_exists('GetImageByKey')){ echo GetImageByKey('key9');} else {echo 'images/keys9.png';}?>" alt="http://szawl.eu/chart/v2 Knitting chart generator by szawl.eu" class="k" title="http://szawl.eu/chart/v2 Knitting chart generator by szawl.eu">   -
                        <?php if(isset($lang['keysdes'][9])){ echo $lang['keysdes'][9];} ?>
                    </div>
                    <div class="box append border" id="key10" title="<?php if(isset($lang['keysdes'][10])){ echo $lang['keysdes'][10];}  ?>">
                        <img src="<?php if(function_exists('GetImageByKey')){ echo GetImageByKey('key10');} else {echo 'images/keys10.png';}?>" alt="http://szawl.eu/chart/v2 Knitting chart generator by szawl.eu" class="k" title="http://szawl.eu/chart/v2 Knitting chart generator by szawl.eu">   -
                        <?php if(isset($lang['keysdes'][10])){ echo $lang['keysdes'][10];} ?>
                    </div>
                    <div class="box append border" id="key11" title="<?php if(isset($lang['keysdes'][11])){ echo $lang['keysdes'][11];}  ?>">
                        <img src="<?php if(function_exists('GetImageByKey')){ echo GetImageByKey('key11');} else {echo 'images/keys11.png';}?>" alt="http://szawl.eu/chart/v2 Knitting chart generator by szawl.eu" class="k" title="http://szawl.eu/chart/v2 Knitting chart generator by szawl.eu">   -
                        <?php if(isset($lang['keysdes'][11])){ echo $lang['keysdes'][11];} ?>
                    </div>
                </div>
                <div class="box-row">
                    <div class="box append border" id="key12" title="<?php if(isset($lang['keysdes'][12])){ echo $lang['keysdes'][12];}  ?>">
                        <img src="<?php if(function_exists('GetImageByKey')){ echo GetImageByKey('key12');} else {echo 'images/keys12.png';}?>" alt="http://szawl.eu/chart/v2 Knitting chart generator by szawl.eu" class="k" title="http://szawl.eu/chart/v2 Knitting chart generator by szawl.eu">   -
                        <?php if(isset($lang['keysdes'][12])){ echo $lang['keysdes'][12];} ?>
                    </div>
                    <div class="box append border" id="key13" title="<?php if(isset($lang['keysdes'][13])){ echo $lang['keysdes'][13];}  ?>">
                        <img src="<?php if(function_exists('GetImageByKey')){ echo GetImageByKey('key13');} else {echo 'images/keys13.png';}?>" alt="http://szawl.eu/chart/v2 Knitting chart generator by szawl.eu" class="k" title="http://szawl.eu/chart/v2 Knitting chart generator by szawl.eu">   -
                        <?php if(isset($lang['keysdes'][13])){ echo $lang['keysdes'][13];} ?>
                    </div>
                    <div class="box append border" id="key14" title="<?php if(isset($lang['keysdes'][14])){ echo $lang['keysdes'][14];}  ?>">
                        <img src="<?php if(function_exists('GetImageByKey')){ echo GetImageByKey('key14');} else {echo 'images/keys14.png';}?>" alt="http://szawl.eu/chart/v2 Knitting chart generator by szawl.eu" class="k" title="http://szawl.eu/chart/v2 Knitting chart generator by szawl.eu">   -
                        <?php if(isset($lang['keysdes'][14])){ echo $lang['keysdes'][14];} ?>
                    </div>
                </div>
                <div class="box-row">
                    <div class="box append border" id="key15" title="<?php if(isset($lang['keysdes'][15])){ echo $lang['keysdes'][15];}  ?>">
                        <img src="<?php if(function_exists('GetImageByKey')){ echo GetImageByKey('key15');} else {echo 'images/keys15.png';}?>" alt="http://szawl.eu/chart/v2 Knitting chart generator by szawl.eu" class="k" title="http://szawl.eu/chart/v2 Knitting chart generator by szawl.eu">   -
                        <?php if(isset($lang['keysdes'][15])){ echo $lang['keysdes'][15];} ?>
                    </div>
                    <div class="box append border" id="key16" title="<?php if(isset($lang['keysdes'][16])){ echo $lang['keysdes'][16];}  ?>">
                        <img src="<?php if(function_exists('GetImageByKey')){ echo GetImageByKey('key16');} else {echo 'images/keys16.png';}?>" alt="http://szawl.eu/chart/v2 Knitting chart generator by szawl.eu" class="k" title="http://szawl.eu/chart/v2 Knitting chart generator by szawl.eu">   -
                        <?php if(isset($lang['keysdes'][16])){ echo $lang['keysdes'][16];} ?>
                    </div>
                    <div class="box append border" id="key17" title="<?php if(isset($lang['keysdes'][17])){ echo $lang['keysdes'][17];}  ?>">
                        <img src="<?php if(function_exists('GetImageByKey')){ echo GetImageByKey('key17');} else {echo 'images/keys17.png';}?>" alt="http://szawl.eu/chart/v2 Knitting chart generator by szawl.eu" class="k" title="http://szawl.eu/chart/v2 Knitting chart generator by szawl.eu">   -
                        <?php if(isset($lang['keysdes'][17])){ echo $lang['keysdes'][17];} ?>
                    </div>
                </div>
              </div>
              <div class="full medium"><?php
                    if(isset($_SESSION['error'])){echo $_SESSION['error'];unset($_SESSION['error']);}
                    if(isset($lang['writesupport'])){echo '<h5>'.$lang['writesupport'].'<img src="include/email.php" style="" alt="Email me"></h5>';}
              ?></div>
           </div>
        </div>
    </div>
    <div class="row footer">

        <div class="col-sm-12"><div class="copyright"> Created by  <a href="http://poplauki.eu" target="_blank">Poplauki</a> <?php if(isset($lang['copyrights'])){ echo $lang['copyrights'];} ?></div> </div>
    </div>
</div>
<?php //<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.3/jquery.min.js"></script>?>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.12.3.min.js"><\/script>')</script>
<script src="js/vendor/bootstrap.js" type="text/javascript"></script>
<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<script src="js/plugins.js"></script>
<script src="js/main.js"></script>
</body>
</html>
