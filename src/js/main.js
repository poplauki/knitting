let  $draggable = null;
$(document).ready(function () {
    hs.graphicsDir = 'highslide/graphics/';
    hs.outlineType = 'rounded-white';
    hs.creditsPosition = 'bottom left';
    hs.anchor = 'top'
    hs.allowSizeReduction = false;
    hs.enableKeyListener = false;
    hs.captionEval = 'this.a.title';

    hs.lang = GetHSLang($('html').attr('lang'));

    /*let  language = window.navigator.userLanguage || window.navigator.language;
    let  user_select_language=false;
    console.log('language ',language);
    console.log('language html ',$('html').attr('lang'));

    if (Modernizr.localstorage) {
        // window.localStorage is available!
        let  settings = getSzawlState();
        if ('undefined' !== typeof settings) {
            console.log('settings ',settings);
            //user_select_language = settings['user_select_language'];
        }
    } else {
        // нет встроенной поддержки HTML5-хранилища
    }

    if ('undefined' !== typeof $('html').attr('lang') && -1 < language.indexOf($('html').attr('lang')) && !user_select_language) {
        saveSzawlValue('user_select_language',false);
        saveSzawlValue('browser_select_language',true);
        saveSzawlValue('lang',language);
        select_lang(language);
    }*/
    //select_lang(language);
    let  area = $("#area");
    if ('undefined' !== typeof area) {
        let area_html = area.html();


        if ('' === area_html) {
            fillSquares(11, 11);
            //area.html('' + fillSquares(11, 11));
            $('#area_x').val(10);
            $('#area_y').val(10);
            fillNumbersX(2000);
            fillNumbersY(2000);
            $('#draggable').show();
        }

        $draggable = $('#draggable').draggabilly({});

        $draggable.on( 'dragStart', function( event, pointer ) {
            //area.addClass('selectedarea');
            area.css('cursor', 'move');
        });

        $draggable.on( 'dragMove', function( event, pointer, moveVector ) {
            area.css('cursor', 'move');
        });

        $draggable.on( 'dragEnd', function( event, pointer ) {
            //area.removeClass('selectedarea');
            area.css('cursor', 'pointer');
        });
    }
    $('.append').on('click',function () {
        let  thi = $(this);
        let  title = thi.attr('id');

        if (title) {
            let  selected_block = $('#selectedblock');

            if (selected_block.val() === 'null') {

            } else {
                let  mas = title.split("y");
                let  block = $('#' + selected_block.val());
                let  selected = $('#' + selected_block.val() + '_value');
                if (mas[1] !== -1 && block) {
                    selected.val(parseInt(mas[1]));

                    block.css({'background-image': 'url(\'images/keys_build_' + mas[1] + '.png\')', 'background-position': 'center center', 'background-repeat': 'none', 'background-color': '#fff', 'border': 'thin solid black'});
                    $('#printimage').show();
                    $('#resetimage').show();
                    $('#downloadsource').show();
                    $('#downloadimage').show();
                    $('#createpdf').show();
                }
                selected_block.val(null);
            }

        }
    });
});
$(document).on("change", "input[name=numbering]", function (e) {
    let  thi = $(this);
    let  value = thi.val();

});
$(document).on("change", "#right_columns_value", function (e) {
    let  thi = $(this);
    let  value = parseInt(thi.val());
    let  area_x = $('#area_x');
    let  area_y = $('#area_y');
    let  get_columns = $('#right_columns_value');
    if (value <= 60 && value > 0) {
        if (parseInt(area_y.val()) !== value) {
            let  diff = Math.abs(parseInt(area_y.val()) - value);
            if (parseInt(area_y.val()) > value) {

                let  you_sure = confirm("" + $('#sure_text_column').html());
                if (you_sure === true) {
                    let  elem = $('#block0_0');
                    let  i = 1, j = parseInt(area_y.val()) - 1;
                    for (i = 1; i <= parseInt(area_x.val()); i++) {
                        for (j = value; j <= parseInt(area_y.val()); j++) {
                            elem = $('#block' + i + '_' + j + '');
                            if (elem) {
                                elem.remove();
                            }
                        }
                    }

                    area_y.val(value);
                    get_columns.val(value);
                }
            } else {
                let  lines = parseInt(area_x.val());
                let  i = 1, j = parseInt(area_y.val()) + diff;
                for (i = 1; i <= lines; i++) {
                    for (j = parseInt(area_y.val()); j < (parseInt(area_y.val()) + diff); j++) {
                        $('<div/>').attr('id','block' + i + '_' + j + '').attr('class','area_block').attr('title','x: ' + i + ' y: ' + j + '').insertBefore('#block' + i + '_endline');

                        let  block = $('#block' + i + '_' + j + '');
                        $('<input/>').attr('id','block' + i + '_' + j + '_x').attr('type','hidden').val(i).appendTo(block);
                        $('<input/>').attr('id','block' + i + '_' + j + '_y').attr('type','hidden').val(j).appendTo(block);
                        $('<input/>').attr('id','block' + i + '_' + j + '_value').attr('type','hidden').val(null).appendTo(block);
                        $('<select/>').attr('id','block' + i + '_' + j + '_select').attr('class','block_select').attr('title','block' + i + '_' + j + '').appendTo(block);
                        addEvent('block' + i + '_' + j + '','click',selectSquare);
                        //addEvent('block' + i + '_' + j + '','touchstart',disableDrag);
                        //addEvent('block' + i + '_' + j + '','touchmove',disableDrag);
                        //addEvent('block' + i + '_' + j + '','touchend',selectSquare);
                        //addEvent('block' + i + '_' + j + '','mousemove',disableDrag);
                        addEvent('block' + i + '_' + j + '','mouseout',enableDrag);
                    }
                }

                area_y.val(value);
                get_columns.val(value);
            }
        }
    } else {
        thi.val(area_y.val());
    }
});
$(document).on("change", "#bottom_rows_value", function (e) {
    let  thi = $(this);
    let  value = parseInt(thi.val());
    let  area_x = $('#area_x');
    let area_y = $('#area_y');
    let get_rows = $('#bottom_rows_value');
    if (value <= 60 && value > 0) {
        if (parseInt(area_x.val()) !== value) {
            let area = $("#area");
            let diff = 0;
            if (area_x.val() > value) {
                diff = Math.abs(area_x.val() - value);
                let you_sure = confirm("" + $('#sure_text_row').html());
                if (you_sure === true) {
                    let elem = $('#block0_0');
                    let endline = $('#block0_endline');
                    let i = 1, j = parseInt(area_y.val()) - 1;
                    debugger;
                    for (i = value+1; i <= parseInt(area_x.val()); i++) {
                        for (j = 1; j <= parseInt(area_y.val()); j++) {
                            elem = $('#block' + i + '_' + j + '');

                            if (elem) {
                                elem.remove();
                            }

                        }
                        endline = $('#block' + i + '_endline');
                        if (endline) {
                            endline.remove();
                        }
                    }

                    area_x.val(value);
                    get_rows.val(value);
                }
            } else {
                diff = Math.abs(value - area_x.val());
                let j = 1;
                for(let i = parseInt(area_x.val()) + 1; i <= value; i++) {
                    for (j = 1; j <= parseInt(area_y.val()); j++) {

                        $('<div/>').attr('id','block' + i + '_' + j + '').attr('class','area_block').attr('title','x: ' + i + ' y: ' + j + '').appendTo('#area');

                        let block = $('#block' + i + '_' + j + '');
                        $('<input/>').attr('id','block' + i + '_' + j + '_x').attr('type','hidden').val(i).appendTo(block);
                        $('<input/>').attr('id','block' + i + '_' + j + '_y').attr('type','hidden').val(j).appendTo(block);
                        $('<input/>').attr('id','block' + i + '_' + j + '_value').attr('type','hidden').val(null).appendTo(block);
                        $('<select/>').attr('id','block' + i + '_' + j + '_select').attr('class','block_select').attr('title','block' + i + '_' + j + '').appendTo(block);
                        addEvent('block' + i + '_' + j + '','click',selectSquare);
                        //addEvent('block' + i + '_' + j + '','touchstart',disableDrag);
                        //addEvent('block' + i + '_' + j + '','touchmove',disableDrag);
                        //addEvent('block' + i + '_' + j + '','touchend',selectSquare);
                        //addEvent('block' + i + '_' + j + '','mousemove',disableDrag);
                        addEvent('block' + i + '_' + j + '','mouseout',enableDrag);
                    }
                    $('<div/>').attr('class','clearfix').attr('id','block' + i + '_endline').appendTo('#area');
                }
                area_x.val(value);
                get_rows.val(value);
            }
        } else {

        }
    } else {
        thi.val(area_x.val());
    }
});
$(document).on("click", "#emptyblock", function (e) {
    let selected_block = $('#selectedblock');
    if (selected_block) {
        let present = $('#' + selected_block.val());
        present.css({'background-color': 'transparent', 'background-image': '', 'border-top': 'none', 'border-left': 'none', 'border-right': 'thin solid black', 'border-bottom': 'thin solid black'});
    }
    $('#emptyblock').hide();
});
$(document).on("click", "#downloadimage", function (e) {
    let area = $("#area");
    if (area) {
        $('#loader_block').show();
        const area_x = $('#area_x');
        const area_y = $('#area_y');
        const mas = [];
        let k = 1;

        for (let i = 1; i <= parseInt(area_y.val()); i++) {
            for (let j = 1; j <= parseInt(area_x.val()); j++) {
                const elem = $('#block' + j + '_' + i + '_value');

                if (elem) {
                    const point = {
                        point_title: '#block' + j + '_' + i,
                        x_coord: i - 1,
                        y_coord: j - 1,
                        value: elem.val()
                    };
                    mas.push(point);
                }
                k++;
            }
        }

        const formData = new FormData();
        formData.append('title', $('input[name=named]').val());
        formData.append('numbering', $('input:radio[name=numbering]:checked').val());
        formData.append('x_point', area_x.val());
        formData.append('y_point', area_y.val());
        formData.append('points', JSON.stringify(mas));

        fetch("../chart/include/save.php",{
            method: "POST",
            body: formData
        })
          .then((result) => {
              if (result.status !== 200) { throw new Error("Bad Server Response"); }
              return result.text();
          })
          .then((response) => {
              console.log(response);
              window.location.href = "../chart/include/picture2.php";
          })
          .catch((error) => { console.log(error); });
        $('#loader_block').hide();
    }
});
$(document).on("click", "#downloadsource", function (e) {
    let area = $("#area");
    if (area) {
        $('#loader_block').show();
        let area_html = area.html();
        const area_x = $('#area_x');
        const area_y = $('#area_y');
        let elem = $('#block0_0');
        let $radios = $('input:radio[name=numbering]');
        let mas = [];
        let j = 1, i = 1, k = 1;
        let point = {};
        for (i = 1; i <= parseInt(area_y.val()); i++) {
            for (j = 1; j <= parseInt(area_x.val()); j++) {
                elem = $('#block' + j + '_' + i + '_value');

                if (elem) {
                    point = {
                        point_title: '#block' + j + '_' + i,
                        x_coord: i - 1,
                        y_coord: j - 1,
                        value: elem.val()
                    };
                    mas.push(point);

                }
                k++;
            }
        }

        let pattern = {
            title: $('input[name=named]').val(),
            x_point: area_x.val(),
            y_point: area_y.val(),
            numbering: $('input:radio[name=numbering]:checked').val(),
            points: mas
        };

        $.post("../chart/include/save.php", {'values': JSON.stringify(pattern, ["title", "x_point", "y_point", "numbering", "points", "point_title", "x_coord", "y_coord", "value"])}, function (file) {
            window.location.href = "../chart/include/export.php";
            //window.open(["include/export.php"]);
        });
        $('#loader_block').hide();
    }
});
$(document).on("click", "#createpdf", function () {
    let area = $("#area");
    if (area) {
        $('#loader_block').show();
        const area_x = $('#area_x');
        const area_y = $('#area_y');
        const mas = [];
        let k = 1;

        for (let i = 1; i <= parseInt(area_y.val()); i++) {
            for (let j = 1; j <= parseInt(area_x.val()); j++) {
                const elem = $('#block' + j + '_' + i + '_value');

                if (elem) {
                    let point = {
                        point_title: '#block' + j + '_' + i,
                        x_coord: i - 1,
                        y_coord: j - 1,
                        value: elem.val()
                    };
                    mas.push(point);

                }
                k++;
            }
        }

        const formData = new FormData();
        formData.append('title', $('input[name=named]').val());
        formData.append('numbering', $('input:radio[name=numbering]:checked').val());
        formData.append('x_point', area_x.val());
        formData.append('y_point', area_y.val());
        formData.append('points', JSON.stringify(mas));

        fetch("../chart/include/save.php",{
            method: "POST",
            body: formData
        })
          .then((result) => {
              if (result.status !== 200) { throw new Error("Bad Server Response"); }
              return result.text();
          })
          .then((response) => {
              console.log(response);
              window.location.href = "../chart/include/export_pdf.php";
          })
          .catch((error) => { console.log(error); });
        $('#loader_block').hide();
    }
});
$(document).on("click", "#resetimage", function (e) {
    let you_sure = confirm("" + $('#sure_text_clear').html());
    if (you_sure === true) {
        let area = $("#area");
        if (area) {
            let area_html = area.html();

            if (area_html.trim() !== '') {
                area.html('');
                fillSquares(11, 11);
                // area.html('' + fillSquares(11, 11));
                $('#area_x').val(10);
                $('#area_y').val(10);
                $('#bottom_rows_value').val(10);
                $('#right_columns_value').val(10);
                $('#selectedblock').empty();
                $('#inputkommand').empty();
                $('#controller').val(null);

                $('input[name=named]').val('Chart_1');
                let $radios = $('input:radio[name=numbering]');
                $radios.filter('[value=all]').prop('checked', true);

                $('#resetimage').hide();
                $('#printimage').hide();
                $('#downloadsource').hide();
                $('#downloadimage').hide();
                $('#createpdf').hide();
            }

        }
    }
});


function selectSquare() {
    //debugger;
    let thi = $(this);
    let title = thi.attr('id');
    if (title) {
        //let select_sample=$('#select_sample');
        let selected_block = $('#selectedblock');

        let x = $('#' + title + '_x');
        let y = $('#' + title + '_y');
        if (selected_block.val() !== title) {
            if (selected_block.val() !== null) {
                let present = $('#' + selected_block.val());
                present.css({'background-color': 'transparent', 'border-top': 'none', 'border-left': 'none', 'border-right': 'thin solid black', 'border-bottom': 'thin solid black'})
                        .attr('alt','x: '+$('#'+title+'_x').val()+' y:'+$('#'+title+'_y').val())
                        .attr('title','x: '+$('#'+title+'_x').val()+' y:'+$('#'+title+'_y').val());
            }
        } else {
            $('#emptyblock').show();
        }
        selected_block.val(title);
        thi.css({'background-color': '#f00', 'border': 'thin solid yellow'});
    }
    enableDrag();
}

function disableDrag() {
    //debugger;
    if (null !== $draggable) {
        $draggable.draggabilly('disable');
    }
}

function enableDrag() {
    if (null !== $draggable) {
        $draggable.draggabilly('enable');
    }
}
$(document).on('change', ".block_select", function () {

    let thi = $(this);
    let title = thi.attr('title');

    if (title) {
        let option = $("select#" + title + "_select");

        let x, y, block, selected;
        x = $('#' + title + '_x');
        y = $('#' + title + '_y');
        block = $('#block' + x.val() + '_' + y.val());
        selected = $('#block' + x.val() + '_' + y.val() + '_value');
        if (option.val() !== -1) {


            selected.val(option.val());
            block.css({'background-image': 'url(\'images/keys_build_' + option.val() + '.png\')', 'background-repeat': 'none'})
                    .attr('alt','x: '+$('#'+title+'_x').val()+' y:'+$('#'+title+'_y').val())
                        .attr('title','x: '+$('#'+title+'_x').val()+' y:'+$('#'+title+'_y').val());

        } else {
            //controller.val(null);
            selected.val('');
            block.css({'background-image': 'none'});
        }
        option.html('');
        option.hide();
    }
});
$(document).on('click', ".right_columns_plus", function () {
    let area = $("#area");
    let area_x = $('#area_x');
    let area_y = $('#area_y');
    let get_columns = $('#right_columns_value');
    if (get_columns && get_columns.val() < 60) {


        let lines = parseInt(area_x.val());
        let i = 1, j = parseInt(area_y.val()) + 1;
        for (i = 1; i <= lines; i++) {

            $('<div/>').attr('id','block' + i + '_' + j + '').attr('class','area_block').attr('title','x: ' + i + ' y: ' + j + '').insertBefore('#block' + i + '_endline');

            let block = $('#block' + i + '_' + j + '');
            $('<input/>').attr('id','block' + i + '_' + j + '_x').attr('type','hidden').val(i).appendTo(block);
            $('<input/>').attr('id','block' + i + '_' + j + '_y').attr('type','hidden').val(j).appendTo(block);
            $('<input/>').attr('id','block' + i + '_' + j + '_value').attr('type','hidden').val(null).appendTo(block);
            $('<select/>').attr('id','block' + i + '_' + j + '_select').attr('class','block_select').attr('title','block' + i + '_' + j + '').appendTo(block);
            addEvent('block' + i + '_' + j + '','click',selectSquare);
            //addEvent('block' + i + '_' + j + '','touchstart',disableDrag);
            //addEvent('block' + i + '_' + j + '','touchmove',disableDrag);
            //addEvent('block' + i + '_' + j + '','touchend',selectSquare);
            //addEvent('block' + i + '_' + j + '','mousemove',disableDrag);
            addEvent('block' + i + '_' + j + '','mouseout',enableDrag);
        }

        //area.html(source_area_new);
        area_y.val(j);
        get_columns.val(j);
    }
});

$(document).on('click', ".right_columns_minus", function () {
    let area_x = $('#area_x');
    let area_y = $('#area_y');
    let get_columns = $('#right_columns_value');

    if (get_columns && get_columns.val() < 60) {
        let you_sure = confirm("" + $('#sure_text_column').html());
        if (you_sure === true) {
            let elem = $('#block0_0');
            let i = 1, j = parseInt(area_y.val()) - 1;
            for (i = 1; i <= parseInt(area_x.val()); i++) {
                elem = $('#block' + i + '_' + parseInt(area_y.val()) + '');
                if (elem) {
                    elem.remove();
                }
            }

            area_y.val(j);
            get_columns.val(j);
        }

    }
});

$(document).on('click', ".bottom_rows_plus", function () {
    let area = $("#area");
    let area_x = $('#area_x');
    let area_y = $('#area_y');
    let get_rows = $('#bottom_rows_value');
    if (get_rows && get_rows.val() < 60) {

        let j = 1, i = parseInt(area_x.val()) + 1;
        for (j = 1; j <= parseInt(area_y.val()); j++) {

            $('<div/>').attr('id','block' + i + '_' + j + '').attr('class','area_block').attr('title','x: ' + i + ' y: ' + j + '').appendTo('#area');

            let block = $('#block' + i + '_' + j + '');
            $('<input/>').attr('id','block' + i + '_' + j + '_x').attr('type','hidden').val(i).appendTo(block);
            $('<input/>').attr('id','block' + i + '_' + j + '_y').attr('type','hidden').val(j).appendTo(block);
            $('<input/>').attr('id','block' + i + '_' + j + '_value').attr('type','hidden').val(null).appendTo(block);
            $('<select/>').attr('id','block' + i + '_' + j + '_select').attr('class','block_select').attr('title','block' + i + '_' + j + '').appendTo(block);
            addEvent('block' + i + '_' + j + '','click',selectSquare);
            //addEvent('block' + i + '_' + j + '','touchstart',disableDrag);
            //addEvent('block' + i + '_' + j + '','touchmove',disableDrag);
            //addEvent('block' + i + '_' + j + '','touchend',selectSquare);
            //addEvent('block' + i + '_' + j + '','mousemove',disableDrag);
            addEvent('block' + i + '_' + j + '','mouseout',enableDrag);
        }
        $('<div/>').attr('class','clearfix').attr('id','block' + i + '_endline').appendTo('#area');

        area_x.val(i);
        get_rows.val(i);
    }

});

$(document).on('click', ".bottom_rows_minus", function () {
    let area = $("#area");
    let area_x = $('#area_x');
    let area_y = $('#area_y');
    let get_rows = $('#bottom_rows_value');
    if (get_rows && get_rows.val() < 60) {

        let you_sure = confirm("" + $('#sure_text_row').html());
        if (you_sure === true) {

            let j = 1, i = parseInt(area_x.val()) ;
            for (j = 1; j <= parseInt(area_y.val()) ; j++) {
                let block = $('#block' + i + '_' + j + '');

                if ('undefined' !== typeof block) {
                    block.remove();
                }

            }
            if ('undefined' !== typeof $('#block' + i + '_endline')) {
                $('#block' + i + '_endline').remove();
            }
            i =i - 1;
            area_x.val(i);
            get_rows.val(i);
        }
    }

});

$(document).on('click', ".language", function () {
    let self = $(this);
    let lang = self.attr('data-language-key');
    //saveSzawlValue('user_select_language',true);
    //saveSzawlValue('lang',lang);
    select_lang(lang);
});

$(document).on("touchstart mousedown", ".area_block", function(event) {
            //если касание, то вычисляем через event.originalEvent.touches[0]:
            if (event.type === "touchstart") {
                let touch = event.originalEvent.touches[0] || event.originalEvent.changedTouches[0];
                let offset = (touch.clientX - $(event.target).offset().left);
            }
            else {
            //если нажата кнопка мышки:
                let offset = (event.offsetX || event.clientX - $(event.target).offset().left);
            }
            //console.log(offset);
            selectSquare();
            //отменяем "всплытие сообщений", чтобы не вызывался клик на тач-устройствах.
            event.stopPropagation();
            event.preventDefault();
});

function fillSquares(x, y) {
    let str = '', src_tmp = '';
    if (x > 0 && y > 0) {
        let i = 1, j = 1;
        src_tmp = '';
        for (i = 1; i < x; i++) {

            for (j = 1; j < y; j++) {
                src_tmp = src_tmp + '<div id="block' + i + '_' + j + '" class="area_block" title="x: ' + i + ' y: ' + j + '"><input type="hidden" id="block' + i + '_' + j + '_x" value="' + i + '"><input type="hidden" id="block' + i + '_' + j + '_y" value="' + j + '"><input type="hidden" id="block' + i + '_' + j + '_value" name="block' + i + '_' + j + '_value" value="null"><select id="block' + i + '_' + j + '_select" name="block' + i + '' + j + '_select" class="block_select" title="block' + i + '_' + j + '"></select></div>';
                //debugger;
                $('<div/>').attr('id','block' + i + '_' + j + '').attr('class','area_block').attr('title','x: ' + i + ' y: ' + j + '').appendTo('#area');

                let block = $('#block' + i + '_' + j + '');
                $('<input/>').attr('id','block' + i + '_' + j + '_x').attr('type','hidden').val(i).appendTo(block);
                $('<input/>').attr('id','block' + i + '_' + j + '_y').attr('type','hidden').val(j).appendTo(block);
                $('<input/>').attr('id','block' + i + '_' + j + '_value').attr('type','hidden').val(null).appendTo(block);
                $('<select/>').attr('id','block' + i + '_' + j + '_select').attr('class','block_select').attr('title','block' + i + '_' + j + '').appendTo(block);
                addEvent('block' + i + '_' + j + '','click',selectSquare);
                //addEvent('block' + i + '_' + j + '','touchstart',disableDrag);
                //addEvent('block' + i + '_' + j + '','touchmove',disableDrag);
                //addEvent('block' + i + '_' + j + '','touchend',selectSquare);
                //addEvent('block' + i + '_' + j + '','mousemove',disableDrag);
                //addEvent('block' + i + '_' + j + '','mouseout',enableDrag);
                addEvent('block' + i + '_' + j + '','mousedown',selectSquare);
            }
            $('<div/>').attr('class','clearfix').attr('id','block' + i + '_endline').appendTo('#area');
            //src_tmp = src_tmp + '<div class="clear"></div>';
        }
        str = str + src_tmp + '';
    }
}
function fillNumbersX(n) {
    let sqr = parseInt(n/24)-2;
    let i=0;
    $('<div/>').attr('id','top-x-axis-0').html('&nbsp;').appendTo($('#top_x_axis'));
    for(i=0;i<sqr;i++) {
        $('<div/>').attr('id','top-x-axis-'+(i+1)).html(''+(i+1)).appendTo($('#top_x_axis'));
    }
}
function fillNumbersY(n) {
    let sqr = parseInt(n/24)-2;
    let i=0;
    $('<div/>').attr('id','top-y-axis-0').html('&nbsp;').appendTo($('#top_y_axis'));
    for(i=0;i<sqr;i++) {
        $('<div/>').attr('id','top-y-axis-'+(i+1)).html(''+(i+1)).appendTo($('#top_y_axis'));
    }

}
function select_lang(let) {
    let form = document.createElement("form"), tmp;
    form.action = self.location;
    form.method = "post";
    form.id = "__id__tmp_form_for_post_submit";
    //for (let param in let) {
    tmp = document.createElement("input");
    tmp.type = "hidden";
    tmp.name = "lang_sel";
    tmp.value = let;
    form.appendChild(tmp);
    //}
    document.body.appendChild(form);
    form.submit();
}
function unhide(divID) {
    let item = document.getElementById(divID);
    if (item) {
        item.className = (item.className === 'hidden') ? 'unhidden' : 'hidden';
    }
}
function cleanArea() {
    let formarea = document.getElementById("inputkommand");
    unhide("subButton");
    unhide("chart_picture");
    unhide("exportLink");
    unhide("saveLink");
    if (formarea.value !== "") {
        formarea.value = "";
    }
    unhide("rezet");
}
function ClickOn(va) {
    let item = document.getElementById("helper");
    let item2 = document.getElementById("showhelp");
    if (va) {
        item.className = 'unhidden';
        item2.className = 'hidden';
    } else {
        item.className = 'hidden';
        item2.className = 'unhidden';
    }
}
function checkArea() {
    let formarea = document.getElementById("inputkommand");
    let alls = new Array(200);
    let sArray = [];
    let n = 0;
    let sk = '', b = 0, e;
    sArray = formarea.value.split("\n");
    let i = 0;
    for (i = 0; i < sArray.length; i++) {
        let mArray = [];
        mArray = sArray[i].split(" ");
        while (b < mArray.length) {
            while (mArray.charAt(b) === sk)
                b++;
            e = b;
            while ((mArray.charAt(e) !== sk) && (e < mArray.length))
                e++;
            alls[n] = parseInt(mArray.substring(b, e));
            n++;
            b = e;
        }
    }
    //unhide("inputkommand");
    //unhide("subButton");
    //let i=0;
}

function addEvent(title, evnt, funct) {
    let answer = null;
    //ger;
    if ('undefined' !== typeof title && 'undefined' !== typeof evnt
        && '' !== evnt && 'undefined' !== typeof funct) {
        let element = document.getElementById('' + title);
        let elements = document.getElementsByClassName(''+title);
        let test = document.getElementById('middle_form');
        let remove_test = false;
        if (!test || null === test || 'undefined' === typeof test) {
            let body = null;
            if ('undefined' !== typeof jQuery) {
                jQuery(document).find('body').each(function () {
                    body = jQuery(this);
                });
                jQuery('<input/>').attr('type', 'hidden').attr('id', 'border_test').appendTo(body);
            } else {
                let btn = document.createElement("input");
                btn.setAttribute('type', 'hidden');
                btn.setAttribute('id', 'border_test');
                body = document.getElementsByTagName('body');
                body[0].appendChild(btn);
            }
            test = document.getElementById('border_test');
            remove_test = true;
        }
        if (null !== test && element && 'undefined' !== typeof element && 'function' === typeof funct ) {
            if ( test.addEventListener ) {
                answer = true;
                element.addEventListener(evnt, funct, false);
            } else if ( test.attachEvent ) {
                answer = element.attachEvent('on' + evnt, funct);
            } else {
                answer = true;
                element.setAttribute('onclick', funct);
            }
        } else if (elements && elements.length > 0 && 'function' === typeof funct) {
            let element2 = elements[0];
            for (let i=0 ; i<elements.length ; i++) {
                element2 = elements[i];

                if (test.attachEvent) {
                    answer = true;
                    element2.attachEvent('on' + evnt, funct);
                } else if (test.addEventListener) {
                    answer = element2.addEventListener(evnt,funct, false);
                } else {
                    answer = true;
                    element2.setAttribute('onclick',funct);
                }
            }
        }
        if (true === remove_test) {
            if ('undefined' !== typeof jQuery) {
                jQuery('#border_test').remove();
            } else {
                body = document.getElementsByTagName('body');
                body[0].removeChild(test);
            }
        }
    }
    return answer;
}

function CR(s, c) {
    s.style.backgroundColor = c;
}

function supports_html5_storage() {
  try {
    return 'localStorage' in window && window['localStorage'] !== null;
} catch (e) {
    return false;
  }
}

function getSzawlState() {
    if (!supports_html5_storage()) { return null; }
    let answer = localStorage.getItem("settings");
    return answer;
}


function saveSzawlValue(key,value) {
    if (!supports_html5_storage()) { return false; }
    let answer = localStorage.getItem("settings");
    answer[key]=value;
    localStorage.setItem("settings", answer);
    return true;
}

function GetHSLang(value) {
    if ('en' === value) {
        return {};
    } else if ('ru' === value){
        return {
	cssDirection: 'ltr',
	loadingText: 'Загружается...',
	loadingTitle: 'Нажмите для отмены',
	focusTitle: 'Нажмите чтобы поместить на передний план',
	fullExpandTitle: 'Развернуть до оригинального размера',
	creditsText: 'Использует <i>Highslide JS</i>',
	creditsTitle: 'Перейти на домашнюю страницу Highslide JS',
	previousText: 'Предыдущее',
	nextText: 'Следующее',
	moveText: 'Переместить',
	closeText: 'Закрыть',
	closeTitle: 'Закрыть (esc)',
	resizeTitle: 'Изменить размер',
	playText: 'Слайдшоу',
	playTitle: 'Начать слайдшоу (пробел)',
	pauseText: 'Пауза',
	pauseTitle: 'Приостановить слайдшоу (пробел)',
	previousTitle: 'Предыдущее (стрелка влево)',
	nextTitle: 'Следующее (стрелка вправо)',
	moveTitle: 'Переместить',
	fullExpandText: 'Оригинальный размер',
	number: 'Изображение %1 из %2',
	restoreTitle: 'Нажмите чтобы закрыть изображение, нажмите и перетащите для изменения местоположения. Для просмотра изображений используйте стрелки.'
};
    } else if ('pl' === value){
        return {
	cssDirection: 'ltr',
	loadingText: 'Ładowanie...',
	loadingTitle: 'Kliknij, aby anulować',
	focusTitle: 'Kliknij, aby przenieść na wierzch',
	fullExpandTitle: 'Rozszerz do pełnego rozmiaru',
	creditsText: 'Korzysta z <i>Highslide JS</i>',
	creditsTitle: 'Przejdź do strony domowej Highslide JS',
	previousText: 'Wstecz',
	nextText: 'Dalej',
	moveText: 'Przesuń',
	closeText: 'Zamknij',
	closeTitle: 'Zamknij (esc)',
	resizeTitle: 'Zmień rozmiar',
	playText: 'Uruchom',
	playTitle: 'Uruchom pokaz slajdów (spacja)',
	pauseText: 'Pauza',
	pauseTitle: 'Wstrzymaj pokaz slajdów (spacja)',
	previousTitle: 'Wstecz (lewa strzałka)',
	nextTitle: 'Dalej (prawa strzałka)',
	moveTitle: 'Przesuń',
	fullExpandText: 'Pełny rozmiar',
	number: 'Image %1 of %2',
	restoreTitle: 'Kliknij, aby zamknąć obrazek; kliknij i przeciąg, aby przesunąć. Użyj klawiszy strzałek, aby przejść dalej lub wstecz.'
};
    } else if ('lt' === value){
        return {
	cssDirection: 'ltr',
	loadingText: 'Kraunama...',
	loadingTitle: 'Atšaukti krovimą',
	focusTitle: 'Rodyti viršuje',
	fullExpandTitle: 'Išplėsti iki pilno dydžio',
	creditsText: 'Pagaminta <i>Highslide JS</i>',
	creditsTitle: 'Eiti į Highslide JS namų puslapį',
	previousText: 'Buvusi',
	nextText: 'Sekanti',
	moveText: 'Patraukti',
	closeText: 'Uždaryti',
	closeTitle: 'Uždaryti (esc klavišas)',
	resizeTitle: 'Pakeisti dydį',
	playText: 'Rodyti',
	playTitle: 'Rodyti slideshow (spacebar klavišas)',
	pauseText: 'Pristabdyti',
	pauseTitle: 'Pristabdyti slideshow (spacebar klavišas)',
	previousTitle: 'Buvusi (klavišas kairėn)',
	nextTitle: 'Sekanti (klavišas dešinėn)',
	moveTitle: 'Patraukti',
	fullExpandText: 'Pilnas dydis',
	number: 'Paveikslėlis %1 iš %2',
	restoreTitle: 'Sputelkite ir uždarysite paveikslėlį. Spustelkite neatleisdami ir galėsite keisti paveikslėlio pozicija svetainėje. Naudokite rodykles, norėdami pažiūrėti buvusią arba sekančią.'
} ;
    } else if ('de' === value){
        return {
	cssDirection: 'ltr',
	loadingText: 'Lade...',
	loadingTitle: 'Klick zum Abbrechen',
	focusTitle: 'Klick um nach vorn zu bringen',
	fullExpandTitle: 'Zur Originalgröße erweitern',
	creditsText: 'Powered by <i>Highslide JS</i>',
	creditsTitle: 'Gehe zur Highslide JS Homepage',
	previousText: 'Voriges',
	nextText: 'Nächstes',
	moveText: 'Verschieben',
	closeText: 'Schließen',
	closeTitle: 'Schließen (Esc)',
	resizeTitle: 'Größe wiederherstellen',
	playText: 'Abspielen',
	playTitle: 'Slideshow abspielen (Leertaste)',
	pauseText: 'Pause',
	pauseTitle: 'Pausiere Slideshow (Leertaste)',
	previousTitle: 'Voriges (Pfeiltaste links)',
	nextTitle: 'Nächstes (Pfeiltaste rechts)',
	moveTitle: 'Verschieben',
	fullExpandText: 'Vollbild',
	number: 'Bild %1 von %2',
	restoreTitle: 'Klick um das Bild zu schließen, klick und ziehe um zu verschieben. Benutze Pfeiltasten für vor und zurück.'
};
    } else if ('fr' === value){
        return {
	cssDirection: 'ltr',
	loadingText: 'Chargement...',
	loadingTitle: 'Cliquer pour annuler',
	focusTitle: 'Cliquer pour amener au premier plan',
	fullExpandTitle: 'Afficher à la taille réelle',
	creditsText: 'Propulsé par <i>Highslide JS</i>',
	creditsTitle: 'Site Web de Highslide JS',
	previousText: 'Précédente',
	nextText: 'Suivante',
	moveText: 'Déplacer',
	closeText: 'Fermer',
	closeTitle: 'Fermer (esc ou Échappement)',
	resizeTitle: 'Redimensionner',
	playText: 'Lancer',
	playTitle: 'Lancer le diaporama (barre d\'espace)',
	pauseText: 'Pause',
	pauseTitle: 'Suspendre le diaporama (barre d\'espace)',
	previousTitle: 'Précédente (flèche gauche)',
	nextTitle: 'Suivante (flèche droite)',
	moveTitle: 'Déplacer',
	fullExpandText: 'Taille réelle',
	number: 'Image %1 sur %2',
	restoreTitle: 'Cliquer pour fermer l\'image, cliquer et faire glisser pour déplacer, utiliser les touches flèches droite et gauche pour suivant et précédent.'
};
    } else if ('es' === value){
        return {
	cssDirection: 'ltr',
	loadingText: 'Cargando...',
	loadingTitle: 'Click para cancelar',
	focusTitle: 'Click para traer al frente',
	fullExpandTitle: 'Expandir al tamaño actual',
	creditsText: 'Potenciado por <i>Highslide JS</i>',
	creditsTitle: 'Ir al home de Highslide JS',
	previousText: 'Anterior',
	nextText: 'Siguiente',
	moveText: 'Mover',
	closeText: 'Cerrar',
	closeTitle: 'Cerrar (esc)',
	resizeTitle: 'Redimensionar',
	playText: 'Iniciar',
	playTitle: 'Iniciar slideshow (barra espacio)',
	pauseText: 'Pausar',
	pauseTitle: 'Pausar slideshow (barra espacio)',
	previousTitle: 'Anterior (flecha izquierda)',
	nextTitle: 'Siguiente (flecha derecha)',
	moveTitle: 'Mover',
	fullExpandText: 'Tamaño real',
	number: 'Imagen %1 de %2',
	restoreTitle: 'Click para cerrar la imagen, click y arrastrar para mover. Usa las flechas del teclado para avanzar o retroceder.'
};
    } else if ('it' === value){
        return {
	cssDirection: 'ltr',
	loadingText: 'Caricamento in corso',
	loadingTitle: 'Fare clic per annullare',
	focusTitle: 'Fare clic per portare in avanti',
	fullExpandTitle: 'Visualizza dimensioni originali',
	creditsText: 'Powered by <i>Highslide JS</i>',
	creditsTitle: 'Vai al sito Web di Highslide JS',
	previousText: 'Precedente',
	nextText: 'Successiva',
	moveText: 'Sposta',
	closeText: 'Chiudi',
	closeTitle: 'Chiudi (Esc)',
	resizeTitle: 'Ridimensiona',
	playText: 'Avvia',
	playTitle: 'Avvia slideshow (barra spaziatrice)',
	pauseText: 'Pausa',
	pauseTitle: 'Pausa slideshow (barra spaziatrice)',
	previousTitle: 'Precedente (freccia sinistra)',
	nextTitle: 'Successiva (freccia destra)',
	moveTitle: 'Sposta',
	fullExpandText: 'Dimensione massima',
	number: 'Immagine %1 di %2',
	restoreTitle: 'Fare clic per chiudere l\'immagine, trascina per spostare. Frecce andare avanti e indietro.'
};
    } else if ('fi' === value){
        return {
	cssDirection: 'ltr',
	loadingText: 'Lataa...',
	loadingTitle: 'Keskeytä',
	focusTitle: 'Tuo esille',
	fullExpandTitle: 'Suurenna täysikokoiseksi',
	creditsText: 'Moottorina <i>Highslide JS</i>',
	creditsTitle: 'Avaa Highslide JS kotisivu',
	previousText: 'Edellinen',
	nextText: 'Seuraava',
	moveText: 'Siirrä',
	closeText: 'Sulje',
	closeTitle: 'Sulje (esc)',
	resizeTitle: 'Muuta kokoa',
	playText: 'Aloita',
	playTitle: 'Aloita esitys (välilyönti)',
	pauseText: 'Tauko',
	pauseTitle: 'Tauko esityksessä (välilyönti)',
	previousTitle: 'Edellinen (nuoli vasemmalle)',
	nextTitle: 'Seuraava (nuoli oikealle)',
	moveTitle: 'Siirrä',
	fullExpandText: 'Täysikokoinen',
	number: 'Kuva %1 / %2',
	restoreTitle: 'Kilauta sulkeaksesi, paina ja raahaa siirtääksesi. Valitse edellinen tai seuraava nuolilla.'
};
    } else if ('uk' === value){
        return {
	cssDirection: 'ltr',
	loadingText: 'Завантаження...',
	loadingTitle: 'Натисніть для скасування',
	focusTitle: 'Натисніть щоб перемістити на передній план',
	fullExpandTitle: 'Розкрити до оригінального розміру',
	creditsText: 'Використовує <i>Highslide JS</i>',
	creditsTitle: 'Відвідати домашню сторінку Highslide JS',
	previousText: 'Попереднє',
	nextText: 'Наступне',
	moveText: 'Перемістити',
	closeText: 'Закрити',
	closeTitle: 'Закрити (esc)',
	resizeTitle: 'Змінити розмір',
	playText: 'Слайдшоу',
	playTitle: 'Почати слайдшоу (пробіл)',
	pauseText: 'Пауза',
	pauseTitle: 'Призупинити слайдшоу (пробіл)',
	previousTitle: 'Попереднє (стрілка ліворуч)',
	nextTitle: 'Наступне (стрілка праворуч)',
	moveTitle: 'Перемістити',
	fullExpandText: 'Оригінальний розмір',
	number: 'Зображення %1 з %2',
	restoreTitle: 'Настисніть кнопку мишеняти щоб закрити зображення, натисніть та перетягніть для зміни розташування. Для просмотру зображень користуйтеся стрілками.'
};
    } else {
        return {};
    }
}
