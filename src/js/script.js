$(document).ready(function () {
  let area = $("#area");
  if (area) {
    let area_html = area.html();


    if (area_html.trim() == '') {
      //alert('empty');
      area.html('' + fillSquares(11, 11));
      $('#area_x').val(10);
      $('#area_y').val(10);
    }
  }
  $('.append').click(function () {
    let thi = $(this);
    let title = thi.attr('id');

    if (title) {
      let selected_block = $('#selectedblock');
      //console.log('selected_block.val() '+selected_block.val());
      if (selected_block.val() === 'null') {
        //console.log('null '+title);
      } else {
        let mas = title.split("y");
        let block = $('#' + selected_block.val());
        let selected = $('#' + selected_block.val() + '_value');
        if (mas[1] != -1 && block) {
          selected.val(parseInt(mas[1]));
          //console.log('title '+title+' #'+selected_block.val()+'='+selected.val());
          block.css({
            'background-image': 'url(\'images/keys_build_' + mas[1] + '.png\')',
            'background-position': 'center center',
            'background-repeat': 'none',
            'background-color': '#fff',
            'border': 'thin solid black'
          });
          $('#printimage').show();
          $('#resetimage').show();
          $('#downloadsource').show();
          $('#downloadimage').show();
          $('#createpdf').show();
        }
        selected_block.val(null);
      }

    }
  });
});
$(document).on("change", "input[name=numbering]", function (e) {
  let thi = $(this);
  let value = thi.val();
  console.log('numbering ' + value);
});
$(document).on("change", "#right_columns_value", function (e) {
  let thi = $(this);
  let value = parseInt(thi.val());
  let area_x = $('#area_x');
  let area_y = $('#area_y');
  if (value <= 30 && value > 0) {
    if (parseInt(area_y.val()) != value) {
      let area = $("#area");
      let diff = 0;
      if (parseInt(area_y.val()) > value) {
        diff = Math.abs(parseInt(area_y.val()) - value);
        let you_sure = confirm("" + $('#sure_text_column').html());
        if (you_sure == true) {
          let source_area = area.html();
          let get_columns = $('#right_columns_value');
          let elem = $('#block0_0');
          let i = 1, k = 1, j = parseInt(area_y.val()) - 1;
          for (i = 1; i <= parseInt(area_x.val()); i++) {
            for (k = value + 1; k <= j + 1; k++) {
              elem = $('#block' + i + '_' + k + '');
              if (elem) {
                elem.remove();
              }
            }
          }
          if (parseInt(area_y.val()) >= 18) {
            let width = parseInt(area.css('width'));
            let width_old = width;
            let width_diff = 24 * Math.abs(parseInt(area_y.val()) - 18);
            width -= width_diff;
            let str_width = width + 'px';
            area.css({'width': str_width});
            //console.log('width_old '+width_old+' width_diff '+width_diff+'  width '+width+' '+area.css('width'));
          }
          area_y.val(value);
          get_columns.val(value);
        }
      } else {
        diff = Math.abs(value - parseInt(area_y.val()));
        let get_columns = $('#right_columns_value');
        let source_area = area.html();
        let source_area_columns = source_area.split('<div class="clear"></div>');
        let source_area_new = '', source_area_new_sub = '';
        let i = 1, k = 1, j = parseInt(area_y.val()) + 1;
        //console.log('j '+j+' ');
        //console.log('diff '+diff+' ');
        for (i = 1; i < source_area_columns.length; i++) {
          for (k = j; k <= value; k++) {
            source_area_new_sub = source_area_new_sub + '<div id="block' + i + '_' + k + '" class="area_block" title="chart.szawl.eu/v2"><input type="hidden" id="block' + i + '_' + k + '_x" value="' + i + '"><input type="hidden" id="block' + i + '_' + k + '_y" value="' + k + '"><input type="hidden" id="block' + i + '_' + k + '_value" name="block' + i + '_' + k + '_value" value="null"><select id="block' + i + '_' + k + '_select" name="block' + i + '_' + k + '_select" class="block_select" title="block' + i + '_' + k + '"></select></div>';
          }
          source_area_new = source_area_new + '' + source_area_columns[i - 1] + '' + source_area_new_sub + '<div class="clear"></div>';
          source_area_new_sub = '';
        }
        //console.log('source_area_new_sub '+source_area_new_sub+' ');
        //console.log('source_area_new '+source_area_new+' ');
        if (value >= 18) {
          var width = parseInt(area.css('width'));
          var width_old = width;
          var width_diff = 24 * diff;
          width += width_diff;
          var str_width = width + 'px';
          area.css({'width': str_width});
          //console.log('width_old '+width_old+' width_diff '+width_diff+'  width '+width+' '+area.css('width'));

        }
        area.html(source_area_new);
        area_y.val(value);
        get_columns.val(value);
      }
    }
  } else {
    thi.val(area_y.val());
  }
});
$(document).on("change", "#bottom_rows_value", function (e) {
  var thi = $(this);
  var value = parseInt(thi.val());
  var area_x = $('#area_x');
  var area_y = $('#area_y');
  console.log('value ' + value + ' area_x ' + area_x.val() + '  area_y ' + area_y.val() + ' ');
  if (value <= 30 && value > 0) {
    if (parseInt(area_x.val()) != value) {
      var area = $("#area");
      var diff = 0;
      if (area_x.val() > value) {
        diff = Math.abs(area_x.val() - value);
        var you_sure = confirm("" + $('#sure_text_row').html());
        if (you_sure == true) {
          var source_area = area.html();
          var get_rows = $('#bottom_rows_value');
          var source_area_columns = source_area.split('<div class="clear"></div>');
          var source_area_old_rows = '';
          var j = 1, i = parseInt(area_x.val()) - 1;
          for (j = 1; j <= value; j++) {
            source_area_old_rows = source_area_old_rows + '' + source_area_columns[j - 1] + '<div class="clear"></div>';
          }
          if (parseInt(area_x.val()) >= 18) {

            var height = parseInt(area.css('height'));
            var height_old = height;
            var height_diff = 24 * Math.abs(parseInt(area_x.val()) - 18);
            height -= height_diff;
            var str_height = height + 'px';
            area.css({'height': str_height});
            //console.log('height_old '+height_old+' height_diff '+height_diff+'  height '+height+' '+area.css('height'));

          }
          area.html(source_area_old_rows);
          area_x.val(value);
          get_rows.val(value);
        }
      } else {
        diff = Math.abs(value - area_x.val());
        var get_rows = $('#bottom_rows_value');
        var source_area = area.html();
        var source_area_new_row = '';
        var j = 1, k = 1, i = parseInt(area_x.val()) + 1;
        for (k = i; k <= value; k++) {
          for (j = 1; j <= parseInt(area_y.val()); j++) {

            source_area_new_row = source_area_new_row + '<div id="block' + k + '_' + j + '" class="area_block" title="chart.szawl.eu/v2"><input type="hidden" id="block' + k + '_' + j + '_x" value="' + k + '"><input type="hidden" id="block' + k + '_' + j + '_y" value="' + j + '"><input type="hidden" id="block' + k + '_' + j + '_value" name="block' + k + '_' + j + '_value" value="null"><select id="block' + k + '_' + j + '_select" name="block' + k + '_' + j + '_select" class="block_select" title="block' + k + '_' + j + '"></select></div>';
          }
          source_area_new_row = source_area_new_row + '<div class="clear"></div>';
        }
        if (value >= 18) {

          var height = parseInt(area.css('height'));
          var height_old = height;
          var height_diff = 24 * diff;
          height += height_diff;
          var str_height = height + 'px';
          area.css({'height': str_height});
          //console.log('height_old '+height_old+' height_diff '+height_diff+'  height '+height+' '+area.css('height'));

        }

        area.html(source_area + '' + source_area_new_row);
        area_x.val(value);
        get_rows.val(value);
      }
    } else {

    }
  } else {
    thi.val(area_x.val());
  }
});
$(document).on("click", "#emptyblock", function (e) {
  var selected_block = $('#selectedblock');
  if (selected_block) {
    var present = $('#' + selected_block.val());
    present.css({
      'background-color': 'transparent',
      'background-image': '',
      'border-top': 'none',
      'border-left': 'none',
      'border-right': 'thin solid black',
      'border-bottom': 'thin solid black'
    });
  }
  $('#emptyblock').hide();
});
$(document).on("click", "#downloadimage", function (e) {
  var area = $("#area");
  if (area) {
    $('#loader_block').show();
    var area_html = area.html();
    var area_x = $('#area_x');
    var area_y = $('#area_y');
    var elem = $('#block0_0');
    var $radios = $('input:radio[name=numbering]');
    var mas = new Array();
    var j = 1, i = 1, k = 1;
    var point = {};
    for (i = 1; i <= parseInt(area_y.val()); i++) {
      for (j = 1; j <= parseInt(area_x.val()); j++) {
        elem = $('#block' + j + '_' + i + '_value');
        //console.log('#block'+j+'_'+i+'_value= '+elem.val() );
        if (elem) {
          point = {
            point_title: '#block' + j + '_' + i,
            x_coord: i - 1,
            y_coord: j - 1,
            value: elem.val()
          };
          mas.push(point);
          //console.log('point '+k+' '+JSON.stringify(point, ["title", "x_coord", "y_coord", "value"]) );
        }
        k++;
      }
    }

    var pattern = {
      title: $('input[name=named]').val(),
      x_point: area_x.val(),
      y_point: area_y.val(),
      numbering: $('input:radio[name=numbering]:checked').val(),
      points: mas
    };
    $.post("include/save.php", {'values': JSON.stringify(pattern, ["title", "x_point", "y_point", "numbering", "points", "point_title", "x_coord", "y_coord", "value"])}, function (file) {
      window.location.href = "../../v1/include/picture2.php";
      //window.open(["include/export.php"]);
    });
    $('#loader_block').hide();
  }
});
$(document).on("click", "#downloadsource", function (e) {
  var area = $("#area");
  if (area) {
    $('#loader_block').show();
    var area_html = area.html();
    var area_x = $('#area_x');
    var area_y = $('#area_y');
    var elem = $('#block0_0');
    var $radios = $('input:radio[name=numbering]');
    var mas = new Array();
    var j = 1, i = 1, k = 1;
    var point = {};
    for (i = 1; i <= parseInt(area_y.val()); i++) {
      for (j = 1; j <= parseInt(area_x.val()); j++) {
        elem = $('#block' + j + '_' + i + '_value');
        //console.log('#block'+j+'_'+i+'_value= '+elem.val() );
        if (elem) {
          point = {
            point_title: '#block' + j + '_' + i,
            x_coord: i - 1,
            y_coord: j - 1,
            value: elem.val()
          };
          mas.push(point);
          //console.log('point '+k+' '+JSON.stringify(point, ["title", "x_coord", "y_coord", "value"]) );
        }
        k++;
      }
    }

    var pattern = {
      title: $('input[name=named]').val(),
      x_point: area_x.val(),
      y_point: area_y.val(),
      numbering: $('input:radio[name=numbering]:checked').val(),
      points: mas
    };
    //console.log('JSON  '+JSON.stringify(pattern, ["title", "x_point", "y_point", "x_coord", "y_coord", "value","numbering"]) );
    $.post("include/save.php", {'values': JSON.stringify(pattern, ["title", "x_point", "y_point", "numbering", "points", "point_title", "x_coord", "y_coord", "value"])}, function (file) {
      window.location.href = "../../v1/include/export.php";
      //window.open(["include/export.php"]);
    });
    $('#loader_block').hide();
  }
});
$(document).on("click", "#createpdf", function (e) {
  var area = $("#area");
  if (area) {
    $('#loader_block').show();
    var area_html = area.html();
    var area_x = $('#area_x');
    var area_y = $('#area_y');
    var elem = $('#block0_0');
    var $radios = $('input:radio[name=numbering]');
    var mas = new Array();
    var j = 1, i = 1, k = 1;
    var point = {};
    for (i = 1; i <= parseInt(area_y.val()); i++) {
      for (j = 1; j <= parseInt(area_x.val()); j++) {
        elem = $('#block' + j + '_' + i + '_value');
        //console.log('#block'+j+'_'+i+'_value= '+elem.val() );
        if (elem) {
          point = {
            point_title: '#block' + j + '_' + i,
            x_coord: i - 1,
            y_coord: j - 1,
            value: elem.val()
          };
          mas.push(point);
          //console.log('point '+k+' '+JSON.stringify(point, ["title", "x_coord", "y_coord", "value"]) );
        }
        k++;
      }
    }

    var pattern = {
      title: $('input[name=named]').val(),
      x_point: area_x.val(),
      y_point: area_y.val(),
      numbering: $('input:radio[name=numbering]:checked').val(),
      points: mas
    };
    //console.log('JSON  '+JSON.stringify(pattern, ["title", "x_point", "y_point", "x_coord", "y_coord", "value","numbering"]) );
    $.post("include/save.php", {'values': JSON.stringify(pattern, ["title", "x_point", "y_point", "numbering", "points", "point_title", "x_coord", "y_coord", "value"])}, function (file) {
      window.location.href = "../include/export_pdf.php";
      //window.open(["include/export.php"]);
    });
    $('#loader_block').hide();
  }
});
$(document).on("click", "#resetimage", function (e) {
  var you_sure = confirm("" + $('#sure_text_clear').html());
  if (you_sure == true) {
    var area = $("#area");
    if (area) {
      var area_html = area.html();


      if (area_html.trim() != '') {
        //alert('empty');
        area.html('' + fillSquares(11, 11));
        $('#area_x').val(10);
        $('#area_y').val(10);
        $('#bottom_rows_value').val(10);
        $('#right_columns_value').val(10);
        $('#selectedblock').empty();
        $('#inputkommand').empty();
        $('#controller').val(null);
        $('input[name=named]').val('Chart_1');
        var $radios = $('input:radio[name=numbering]');
        $radios.filter('[value=all]').prop('checked', true);
        //console.log(' #numbering_all '+$('#numbering_all').attr('checked')+' #numbering_even '+$('#numbering_even').attr('checked')+' #numbering_odd '+$('#numbering_odd').attr('checked'));
        $('#resetimage').hide();
        $('#printimage').hide();
        $('#downloadsource').hide();
        $('#downloadimage').hide();
        $('#createpdf').hide();
      }

    }
  }
});
$(document).on("click", ".area_block", function (e) {

  var thi = $(this);
  var title = thi.attr('id');
  if (title) {
    //alert('title - '+title);
    //var select_sample=$('#select_sample');
    var controller = $('#controller');
    var selected_block = $('#selectedblock');

    var x = $('#' + title + '_x');
    var y = $('#' + title + '_y');
    if (selected_block.val() != title) {
      if (selected_block.val() != null) {
        var present = $('#' + selected_block.val());
        present.css({
          'background-color': 'transparent',
          'border-top': 'none',
          'border-left': 'none',
          'border-right': 'thin solid black',
          'border-bottom': 'thin solid black'
        });
      }
    } else {
      $('#emptyblock').show();
    }
    selected_block.val(title);
    thi.css({'background-color': '#f00', 'border': 'thin solid yellow'});
  }
});


$(document).on('change', ".block_select", function () {

  var thi = $(this);
  var title = thi.attr('title');

  if (title) {
    var option = $("select#" + title + "_select");
    var controller = $('#controller');
    //console.log(' '+option.val());
    var x, y, block, selected;
    x = $('#' + title + '_x');
    y = $('#' + title + '_y');
    block = $('#block' + x.val() + '_' + y.val());
    selected = $('#block' + x.val() + '_' + y.val() + '_value');
    if (option.val() != -1) {


      selected.val(option.val());
      block.css({
        'background-image': 'url(\'images/keys_build_' + option.val() + '.png\')',
        'background-repeat': 'none'
      });

    } else {
      //controller.val(null);
      selected.val('');
      block.css({'background-image': 'none'});
    }
    option.html('');
    option.hide();
  }
});
$(document).on('click', ".right_columns_plus", function () {
  var area = $("#area");
  var area_x = $('#area_x');
  var area_y = $('#area_y');
  var get_columns = $('#right_columns_value');
  if (get_columns && get_columns.val() < 30) {
    var source_area = area.html();
    var source_area_columns = source_area.split('<div class="clear"></div>');
    var source_area_new = '';
    var i = 1, j = parseInt(area_y.val()) + 1;
    for (i = 1; i < source_area_columns.length; i++) {
      source_area_new = source_area_new + '' + source_area_columns[i - 1] + '<div id="block' + i + '_' + j + '" class="area_block" title="chart.szawl.eu/v2"><input type="hidden" id="block' + i + '_' + j + '_x" value="' + i + '"><input type="hidden" id="block' + i + '_' + j + '_y" value="' + j + '"><input type="hidden" id="block' + i + '_' + j + '_value" name="block' + i + '_' + j + '_value" value="null"><select id="block' + i + '_' + j + '_select" name="block' + i + '_' + j + '_select" class="block_select" title="block' + i + '_' + j + '"></select></div><div class="clear"></div>';
    }
    if (j >= 18) {

      var width = parseInt(area.css('width'));
      width += 24;
      var str_width = width + 'px';
      area.css({'width': str_width});
      //console.log('width '+width+' '+area.css('width'));
    }
    area.html(source_area_new);
    area_y.val(j);
    get_columns.val(j);
  }
});

$(document).on('click', ".right_columns_minus", function () {
  var area = $("#area");
  var area_x = $('#area_x');
  var area_y = $('#area_y');
  var get_columns = $('#right_columns_value');
  //console.log('area_y '+area_y.val()+' area_x '+area_x.val()+' '+get_columns.val());
  if (get_columns && get_columns.val() < 30) {
    var you_sure = confirm("" + $('#sure_text_column').html());
    if (you_sure == true) {
      var source_area = area.html();
      var elem = $('#block0_0');
      var i = 1, j = parseInt(area_y.val()) - 1;
      for (i = 1; i <= parseInt(area_x.val()); i++) {
        elem = $('#block' + i + '_' + parseInt(area_y.val()) + '');
        if (elem) {
          elem.remove();
        }
      }
      if (parseInt(area_y.val()) >= 18) {
        var width = parseInt(area.css('width'));
        width -= 24;
        var str_width = width + 'px';
        area.css({'width': str_width});
      }
      //alert('columns '+get_columns.val()+'');
      area_y.val(j);
      get_columns.val(j);
    }

  }
});

$(document).on('click', ".bottom_rows_plus", function () {
  var area = $("#area");
  var area_x = $('#area_x');
  var area_y = $('#area_y');
  var get_rows = $('#bottom_rows_value');
  if (get_rows && get_rows.val() < 30) {
    var source_area = area.html();
    var source_area_new_row = '';
    var j = 1, i = parseInt(area_x.val()) + 1;
    for (j = 1; j <= parseInt(area_y.val()); j++) {

      source_area_new_row = source_area_new_row + '<div id="block' + i + '_' + j + '" class="area_block" title="chart.szawl.eu/v2"><input type="hidden" id="block' + i + '_' + j + '_x" value="' + i + '"><input type="hidden" id="block' + i + '_' + j + '_y" value="' + j + '"><input type="hidden" id="block' + i + '_' + j + '_value" name="block' + i + '_' + j + '_value" value="null"><select id="block' + i + '_' + j + '_select" name="block' + i + '_' + j + '_select" class="block_select" title="block' + i + '_' + j + '"></select></div>';
    }
    source_area_new_row = source_area_new_row + '<div class="clear"></div>';
    if (i >= 18) {

      var height = parseInt(area.css('height'));
      height += 24;
      var str_height = height + 'px';
      area.css({'height': str_height});
      //console.log('height '+height+' '+area.css('height'));
    }

    area.html(source_area + '' + source_area_new_row);
    area_x.val(i);
    get_rows.val(i);
  }

});

$(document).on('click', ".bottom_rows_minus", function () {
  var area = $("#area");
  var area_x = $('#area_x');
  var area_y = $('#area_y');
  var get_rows = $('#bottom_rows_value');
  if (get_rows && get_rows.val() < 30) {

    var you_sure = confirm("" + $('#sure_text_row').html());
    if (you_sure == true) {

      var source_area = area.html();
      var source_area_columns = source_area.split('<div class="clear"></div>');
      var source_area_old_rows = '';
      var j = 1, i = parseInt(area_x.val()) - 1;
      for (j = 1; j < source_area_columns.length - 1; j++) {
        source_area_old_rows = source_area_old_rows + '' + source_area_columns[j - 1] + '<div class="clear"></div>';
      }
      if (i >= 18) {

        var height = parseInt(area.css('height'));
        height -= 24;
        var str_height = height + 'px';
        area.css({'height': str_height});
        //console.log('height '+height+' '+area.css('height'));
      }
      area.html(source_area_old_rows);
      area_x.val(i);
      get_rows.val(i);
    }
  }

});

function fillSquares(x, y) {
  var str = '', src_tmp = '';
  if (x > 0 && y > 0) {
    var i = 1, j = 1;
    src_tmp = '';
    for (i = 1; i < x; i++) {

      for (j = 1; j < y; j++) {
        src_tmp = src_tmp + '<div id="block' + i + '_' + j + '" class="area_block" title="chart.szawl.eu/v2"><input type="hidden" id="block' + i + '_' + j + '_x" value="' + i + '"><input type="hidden" id="block' + i + '_' + j + '_y" value="' + j + '"><input type="hidden" id="block' + i + '_' + j + '_value" name="block' + i + '_' + j + '_value" value="null"><select id="block' + i + '_' + j + '_select" name="block' + i + '' + j + '_select" class="block_select" title="block' + i + '_' + j + '"></select></div>';
      }
      src_tmp = src_tmp + '<div class="clear"></div>';
    }
    str = str + src_tmp + '';
  }
  return str;
}

function select_lang(let) {
  //alert(" "+let+" ");
  var form = document.createElement("form"), tmp;
  form.action = self.location;
  form.method = "post";
  form.id = "__id__tmp_form_for_post_submit";
  //for (var param in let) {
  tmp = document.createElement("input");
  tmp.type = "hidden";
  tmp.name = "lang_sel";
  tmp.value = let;
  form.appendChild(tmp);
  //}
  document.body.appendChild(form);
  form.submit();
}

function unhide(divID) {
  var item = document.getElementById(divID);
  if (item) {
    item.className = (item.className == 'hidden') ? 'unhidden' : 'hidden';
  }
}

function cleanArea() {
  var formarea = document.getElementById("inputkommand");
  unhide("subButton");
  unhide("chart_picture");
  unhide("exportLink");
  unhide("saveLink");
  //alert(" "+formarea.value+" ");
  if (formarea.value !== "") {
    formarea.value = "";
  }
  unhide("rezet");
}

function ClickOn(va) {
  var item = document.getElementById("helper");
  var item2 = document.getElementById("showhelp");
  if (va) {
    item.className = 'unhidden';
    item2.className = 'hidden';
  } else {
    item.className = 'hidden';
    item2.className = 'unhidden';
  }
}

function checkArea() {
  var formarea = document.getElementById("inputkommand");
  var alls = new Array(200);
  var sArray = new Array();
  n = 0;
  var sk = '', b = 0, e;
  sArray = formarea.value.split("\n");
  //alert(""+sArray.length+"");
  //alert(""+sArray[0]);
  //alert(""+sArray[1]);
  //alert(""+sArray[2]);
  var i = 0;
  for (i = 0; i < sArray.length; i++) {
    var mArray = new Array();
    mArray = sArray[i].split(" ");
    //alert(""+mArray.length+"");
    while (b < mArray.length) {
      while (mArray.charAt(b) == sk)
        b++;
      e = b;
      while ((mArray.charAt(e) != sk) && (e < mArray.length))
        e++;
      alls[n] = parseInt(mArray.substring(b, e));
      n++;
      b = e;
    }
  }
  alert("n " + n);
  //unhide("inputkommand");
  //unhide("subButton");
  //var i=0;
  //for(i=0;i<n;i++)alert(""+alls[i]+"");
}

function CR(s, c) {
  s.style.backgroundColor = c;
}
